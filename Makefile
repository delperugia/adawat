#------------------------------
# Command line options:
# 	force Clang++ usage: COMPILER=clang++
# 	force GNU C++ usage: COMPILER=g++
# 	change destination:  DESTDIR=/stage/ PREFIX=/usr/
#
#------------------------------
TEST_PRG     :=	validate
TEST_SRCS    :=	test/adawat_validate.cpp
LIBRARY      :=	libadawat.a
LIBRARY_SRCS :=	src/adawat_common.cpp \
				src/adawat_crashhandler.cpp \
				src/adawat_crypto.cpp \
				src/adawat_encoding.cpp \
                src/adawat_error_control.cpp \
				src/adawat_file.cpp \
				src/adawat_uid.cpp \
				src/adawat_money.cpp \
				src/adawat_stats.cpp \
				src/adawat_string.cpp \
				src/adawat_time.cpp \
				src/adawat_uid.cpp \
				src/adawat_url.cpp
CPPFLAGS    :=	-I include
CXXFLAGS    :=	-g -std=c++17 -O2
CXXWARN     :=	-Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy \
				-Wdisabled-optimization -Wformat=2 -Winit-self -Wmissing-include-dirs \
				-Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow \
				-Wsign-conversion -Wsign-promo -Wstrict-overflow=2 \
				-Wswitch-default -Wundef -Werror -Wno-unused-function -Wno-unused-parameter
LDFLAGS     :=	-g
LDLIBS      :=	-lcrypto -lpthread -lbfd
ARFLAGS     :=	rcs
PREFIX    	:=	/usr/local

#------------------------------
ifneq ($(shell which clang++),)
	ifneq ($(shell which g++),)
		COMPILER := c++
	else
		COMPILER := clang++
	endif
else
	COMPILER := g++
endif
ifeq ($(shell $(COMPILER) -v 2>&1 | grep -c "clang version"), 1)
	CXX     := clang++
	CXXWARN += -ferror-limit=2
else
	CXX     := g++
	CXXWARN += -Wlogical-op -Wstrict-null-sentinel -Wnoexcept
endif

$(info Using $(CXX))

#------------------------------
TEST_OBJS=		$(patsubst  test/%.cpp,objs/test_%.o,$(TEST_SRCS))
LIBRARY_OBJS=	$(patsubst  src/%.cpp,objs/src_%.o,$(LIBRARY_SRCS))

test: $(TEST_PRG)
	@echo Testing $(TEST_PRG) ...
	@"`pwd`/$(TEST_PRG)"

$(TEST_PRG): $(LIBRARY) $(TEST_OBJS)
	@echo Linking $(TEST_PRG) ...
	@$(CXX) $(LDFLAGS) -o $(TEST_PRG) $(TEST_OBJS) $(LIBRARY) $(LDLIBS)

$(LIBRARY): $(LIBRARY_OBJS)
	@echo Building $(LIBRARY) ...
	@$(AR) $(ARFLAGS) -o $(LIBRARY) $(LIBRARY_OBJS)

objs/src_%.o: src/%.cpp
	@mkdir -p objs/
	@echo " " $<
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(CXXWARN) -c -o $@ $<
objs/test_%.o: test/%.cpp
	@mkdir -p objs/
	@echo " " $<
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(CXXWARN) -c -o $@ $<

cppcheck:
	@echo CPPCheck sources...
	@cppcheck -q --enable=warning,style,performance,portability,unusedFunction --inconclusive --inline-suppr -DCPPCHECK -I include src test --error-exitcode=2
	@echo CPPCheck ok

valgrind: $(TEST_PRG)
	@valgrind --error-exitcode=2 --leak-check=yes --show-leak-kinds=all "`pwd`/$(TEST_PRG)"

coverage:	CXXFLAGS += --coverage
coverage:	CXXFLAGS := $(filter-out -O2,$(CXXFLAGS))
coverage:	CXXWARN  := $(filter-out -Wstrict-overflow=5,$(CXXWARN))
coverage:	LDFLAGS  += --coverage
coverage:	clean test
	@gcov -r -o objs/*                                             1>/dev/null    2>/dev/null
	@lcov -q -c -b . -d objs/                                   -o coverage.info  2>/dev/null
	@lcov -q -r coverage.info '/usr/*'                          -o coverage.info  2>/dev/null
	@lcov -q -r coverage.info `pwd`/src/adawat_crashhandler.cpp -o coverage.info  2>/dev/null
	@lcov -q -r coverage.info `pwd`/test/adawat_validate.cpp    -o coverage.info  2>/dev/null
	@lcov -q -l coverage.info
	@genhtml -q -o coverage/ coverage.info
	@echo Notice: compiled binaries are now not optimized and contain coverage info

depend: _depend.inc

_depend.inc: $(LIBRARY_SRCS) $(TEST_SRCS)
	@echo Building dependencies...
	@rm -f                                ./_depend.inc
	@$(CXX) $(CPPFLAGS) -M $^          >> ./_depend.inc
	@sed -ri 's/^(.*\.o: *)/objs\/\1/'    ./_depend.inc

clean:
	@echo Cleaning...
	@rm -f $(TEST_PRG) $(TEST_OBJS) $(LIBRARY_OBJS) _depend.inc
	@rm -f _pvs.log _pvs.err strace_out
	@rm -rf objs coverage coverage.info coverage_filtered.info adawat_common.cpp.gcov

distclean: clean
	@echo Cleaning for distribution...
	@rm -f $(LIBRARY)

install: $(LIBRARY)
	@install -d $(DESTDIR)$(PREFIX)/lib/
	@install -m 644 $(LIBRARY) $(DESTDIR)$(PREFIX)/lib/
	@install -d $(DESTDIR)$(PREFIX)/include/adawat/
	@install -m 644 include/adawat*.h* $(DESTDIR)$(PREFIX)/include/adawat/

uninstall:
	@rm -f $(DESTDIR)$(PREFIX)/lib/$(LIBRARY)
	@rmdir --ignore-fail-on-non-empty $(DESTDIR)$(PREFIX)/lib/
	@rm -f $(DESTDIR)$(PREFIX)/include/adawat/adawat*.h*
	@rmdir --ignore-fail-on-non-empty $(DESTDIR)$(PREFIX)/include/adawat/

include _depend.inc
