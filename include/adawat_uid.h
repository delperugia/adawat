// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Unique identifier functions
//

#pragma once

#include <string>

namespace adawat {

//-------------------------------------------------------------------------
enum class uid_type {
    short_ts,
    medium_ts,
    long_ts
};

enum class uid_alphabet {
    digits,              // 0-9
    upper_alpha,         // A-Z
    lower_alpha,         // a-z
    upper_hexadecimal,   // 0-9A-F
    lower_hexadecimal,   // 0-9a-f
    upper_alphanum,      // A-Z0-9
    lower_alphanum,      // a-z0-9
    base32,              // A-Z2-7 (RFC 4648)
    base32_crockford,    // 0-9ABCDEFGHJKMNPQRSTVWXYZ
    base62               // 0-9A-Za-z
};

enum class uuid_type {
    v1,
    v3,
    v4,
    v5,
    v6,
    v7
};

enum class uuid_namespace {
    dns,
    url,
    oid,
    x500
};  

//-------------------------------------------------------------------------
// Get the alphabet and its size corresponding to the given type
std::tuple< size_t, const char * >
get_alphabet ( uid_alphabet   p_alphabet );

//-------------------------------------------------------------------------
// Returns a time prefixed unique id using one of the following methods:
//
//                                               standard          secured
//              timestamp  period  precision randomness length randomness length
//  - short_ts      16b     4096s     62ms      114b      22      126b      24
//  - medium_ts     24b       48d    250ms      106b      22      124b      25
//  - long_ts       32b      136y      1s        98b      22      122b      26
//
// Having a time prefix has two benefits: smaller size and database insertion
// performances.
//
// Standard version offers a much lower risk of collision than UUID-v4 and uses
// a smaller size, but since the random part is smaller, it is potentially more
// easily guessable. The secured version fixes this by adding extra entropy
// (and is still smaller than UUID-v4).
//
//  Alphabet used is base62.
std::string  uid_ts ( uid_type  p_type, bool  p_secured = false );

//-------------------------------------------------------------------------
// Returns a fixed length uid, without timestamp, using the
// given alphabet.
std::string  uid_no_ts ( size_t        p_length,
                         uid_alphabet  p_alphabet = uid_alphabet::base62 );

//-------------------------------------------------------------------------
// Returns a 6 characters long, unique per day, per process
// using base62 alphabet.
std::string  uid_daily ( void );

//-------------------------------------------------------------------------
// Returns an UUID v4 or v7 unique id
std::string  uid_uuid ( uuid_type           p_type        = uuid_type::v4,
                        uuid_namespace      p_namespace   = uuid_namespace::dns,
                        const std::string & p_name        = "" );

//-------------------------------------------------------------------------
// Convert uid_type to/from string
std::string  uid_type_cvt ( uid_type             p_type );

bool         uid_type_cvt ( const std::string &  p_text,
                            uid_type &           p_type );

//-------------------------------------------------------------------------
// Convert uid_alphabet to/from string
std::string  uid_alphabet_cvt ( uid_alphabet         p_alphabet );

bool         uid_alphabet_cvt ( const std::string &  p_text,
                                uid_alphabet &       p_alphabet );

} // namespace adawat
