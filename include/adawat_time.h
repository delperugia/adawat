// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Time and date related functions
//

#pragma once

#include <ctime>
#include <string>

namespace adawat {

// Returns the number of seconds since midnight (00:00:00)
// to now or to the given absolute time, or -1 on error
int            time_seconds_since_midnight ( std::time_t p_time = 0 );

// Returns the absolute time of the last midnight (00:00:00)
// for now or for the given absolute time, or -1 on error
std::time_t    time_last_midnight          ( std::time_t p_time = 0 );

// Returns the number of seconds to the next midnight (00:00:00)
// from now or from the given absolute time, or -1 on error
int            time_seconds_to_midnight    ( std::time_t p_time = 0 );

// Returns the absolute time of the next midnight (00:00:00)
// from now or from the given absolute time, or -1 on error
std::time_t    time_next_midnight          ( std::time_t p_time = 0 );

// Returns the RFC 3339 UTC string of now or the given absolute time,
// or empty string on error (e.g.: 1985-04-12T23:20:50Z)
std::string    time_format_rfc3339_utc     ( std::time_t p_time = 0 );

// Returns the RFC 3339 local string of now or the given absolute time,
// or empty string on error (e.g.: 1996-12-19T16:39:57-08:00)
std::string    time_format_rfc3339_local   ( std::time_t p_time = 0 );

// Returns the given RFC 3339 string has an absolute time,
// or -1 on error
std::time_t    time_parse_rfc3339          ( const std::string & p_time );

// Returns the number of seconds corresponding to the given duration
//  e.g.: 35s = 35, 1d = 86400, 2d4h = 187200
// or -1 on error. Ignore white spaces, accepted units are:
//      s second    d day   y year
//      m minute    w week
//      h hour      o month
// Month 30.436875 days, Year is 365.2425 days
long long int  time_parse_duration         ( const std::string & p_duration );


// Parse an ISO 8601 duration where format is "P(n)Y(n)M(n)DT(n)H(n)M(n)S"
// When using date greater_than_time elements (YMD), function tries to take
// into account the DST. By default duration are calculated starting now
// but another time reference can be passed.
long long int time_parse_iso8601_duration ( const std::string & p_duration,
                                            std::time_t         p_time = 0 );

} // namespace adawat
