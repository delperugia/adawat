// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// File related functions
//

#pragma once

#include <string>

#include "adawat_common.h"

namespace adawat {

//-------------------------------------------------------------------------

// Append the string into a text file, returns true on success
// Creates the file if needed
bool file_append ( const std::string &  p_file_name,
                   std::string_view     p_content );

// Read the content of a text file, returns true on success
bool file_read ( const std::string &  p_file_name,
                 std::string &        p_content );

// Write the string into a text file, returns true on success
bool file_write ( const std::string &  p_file_name,
                  std::string_view     p_content,
                  bool                 p_create_folder  = false,
                  mode_t               p_file_mode      = 0640,
                  mode_t               p_directory_mode = 0750 );

// Append Data into a binary file, returns true on success
// Creates the file if needed
bool file_append ( const std::string &  p_file_name,
                   const Data &         p_content );

// Read the content of a binary file, returns true on success
bool file_read ( const std::string &  p_file_name,
                 Data &               p_content );

// Write Data into a binary file, returns true on success
bool file_write ( const std::string &  p_file_name,
                  const Data &         p_content,
                  bool                 p_create_folder  = false,
                  mode_t               p_file_mode      = 0640,
                  mode_t               p_directory_mode = 0750 );

} // namespace adawat
