// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Amount and currency manipulation objects
//

#pragma once

#include <string>
#include <unordered_map>

namespace adawat {

//-------------------------------------------------------------------------
// This class represents a currency. Given a 3 letters currency code
// (ISO 4217), it provides the symbol and precision required.
class Currency
{
    public:
        // Mutators
        //
        // Expects a 3 letters currency alphabetic code (ISO 4217)
        bool        set              ( const std::string &  p_code );
        //
        // Add or replace a new currency definition for all instances present or future of Currency
        static
        bool        global_define_currency ( const std::string &  p_code,
                                             const std::string &  p_symbol,
                                             const std::string &  p_symbol_local,
                                             unsigned             p_number,
                                             unsigned             p_precision,
                                             unsigned             p_subunit );
        //
        // Accessors
        //
        bool        is_set           ( void ) const;
        std::string get_code         ( void ) const;   // alphabetic code
        unsigned    get_numeric_code ( void ) const;   // numeric code
        std::string get_symbol       ( void ) const;
        std::string get_symbol_local ( void ) const;
        unsigned    get_precision    ( void ) const;
        unsigned    get_subunit      ( void ) const;
        //
        bool        operator ==      ( Currency const &  p_b ) const;
        bool        operator !=      ( Currency const &  p_b ) const;
        //
    private:
        static
        constexpr
        uint32_t    encode           ( char p_a, char p_b, char p_c );
        //
        uint32_t    m_currency = 0;   // a currency encoded in an integer
        //
        // Static storage of all built-in currencies definitions
        typedef struct {
            std::string     symbol;         //  €
            std::string     symbol_local;   //  €
            unsigned        number;         //  978 (ISO code number)
            unsigned        precision;      //  6 (used when calculating)
            unsigned        subunit;        //  2 (also known as exponent)
        }   CurrencyDefinition;
        //
        typedef std::unordered_map< uint32_t, CurrencyDefinition >
            CurrencyDefinitions;
        //
        static
        CurrencyDefinitions m_currencies;
};

//-------------------------------------------------------------------------
// This class represents an amount, that is a value and a currency.
// Values are represented as 64 bits integer, with a precision depending
// on the currency.
class Amount
{
    public:
        enum class result {
            ok,
            overflow,
            division_by_zero,
            incompatible_currency,
            invalid_value,
            invalid_currency,
            invalid_mode,
            invalid_precision
        };
        //
        static
        std::string to_string   ( result p_result );
        //
        // Comparisons
        // set p_result: <b: -1, =b: 1, >b: 1
        result      comp        ( const Amount & p_b, int & p_result ) const;
        bool        operator == ( const Amount & p_b ) const;
        bool        operator != ( const Amount & p_b ) const;
        bool        is_zero     ( void ) const;
        bool        is_positive ( void ) const;
        bool        is_negative ( void ) const;
        //
        // Set the object amount
        result      set ( const std::string &  p_value,
                          const std::string &  p_currency );
        //
        // Accessors
        bool        is_set       ( void ) const;
        std::string value        ( void ) const;  // with up to precision decimals (e.g.: "12.1")
        std::string currency     ( void ) const;  // the currency sub-units (e.g.: "EUR")
        unsigned    numeric_code ( void ) const;  // the currency numeric code (e.g.: 978)
        std::string symbol       ( void ) const;  // the currency symbol (e.g.: "€")
        std::string symbol_local ( void ) const;  // the local currency symbol (e.g.: "€")
        unsigned    precision    ( void ) const;  // the currency precision (e.g.: 6)
        unsigned    subunit      ( void ) const;  // the currency sub-units (e.g.: 2)
        //
        // Retrieves the object amount as a formatted string
        //   p_precision:     decimal to keep (truncated)
        //   p_group_mode:    0 none, 3 thousands, 4 myriad or 7 Indian 2,2,3
        //   p_currency_mode: 0 none, 1 code before, 2 code after, 3 symbol before, 4 symbol after
        // The amount is truncated to the given precision (round down)
        std::string format ( unsigned             p_precision          = 32 ,
                             const std::string &  p_decimal_separator  = ".",
                             unsigned             p_group_mode         = 3  ,
                             const std::string &  p_group_separator    = " ",
                             unsigned             p_currency_mode      = 2  ,
                             const std::string &  p_currency_separator = " " ) const;
        //
        // Basic operations (modify the current object)
        result      add     ( const Amount &       p_amount );
        result      sub     ( const Amount &       p_amount );
        result      mul     ( const std::string &  p_value  );
        result      div     ( const std::string &  p_value  );
        result      percent ( const std::string &  p_value  );
        result      min     ( const Amount &       p_amount );
        result      max     ( const Amount &       p_amount );
        //
        // Rounds a number according to the given mode
        //   p_mode:      1 up, 2 down, 3 commercial
        //   p_precision: 0 - 5                      0.123456
        // Examples:    round up, precision 2
        //                  2.0864... → 2.09 EUR
        //                  4.1729... → 4.18 EUR
        //              round down, precision 2
        //                  2.0864... → 2.08 EUR
        //                  4.1729... → 4.17 EUR
        //              round commercial, precision 2
        //                  2.0864... → 2.09 EUR
        //                  4.1729... → 4.17 EUR
        result      round   ( unsigned             p_mode,
                              unsigned             p_precision );
        //
    private:
        bool        m_set       = false;    // true if Amount is properly set
        int64_t     m_value     = 0;
        unsigned    m_precision = 0;        // typical range: 0 - 6, maximum 18
        Currency    m_currency;
        //
        static
        result      convert   ( std::string  p_text,
                                unsigned     p_precision,
                                int64_t &    p_value );
        //
        static
        result      fraction  ( std::string  p_text,
                                int64_t &    p_numerator,
                                int64_t &    p_denominator );
};

} // namespace adawat
