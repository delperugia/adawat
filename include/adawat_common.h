// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Common types
//

#pragma once

#include <stdint.h>
#include <string>
#include <vector>

namespace adawat {

//-------------------------------------------------------------------------
constexpr
const char k_adawat_version[] = "1.19-1";

//-------------------------------------------------------------------------
// All data manipulations are done using this class

namespace {
    typedef std::vector< uint8_t > DataBase;
};

class Data : public DataBase
{
    public:
        using DataBase::DataBase;   // import all base constructors
        //
        // Copy a string in Data at construction
        explicit
        Data ( const std::string &  p_string );
        //
        // Copy from string
        Data &      operator=   ( const std::string &  p_string );
        //
        // Retrieve as string
        std::string str         ( void ) const;
};

} // namespace adawat
