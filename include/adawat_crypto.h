// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Cryptology functions: wrappers around openSSL
//

#pragma once

#include <openssl/evp.h>
#include <string>

#include "adawat_common.h"

namespace adawat {

// Use EVP_get_cipherbyname and EVP_get_digestbyname to get an algorithm
// from a name, EVP_CIPHER_* function to get cipher details and
// ENGINE_get_* to list them (or execute 'openssl help').

//-------------------------------------------------------------------------
// Returns and empties the error queue
std::string     crypto_last_errors (void);

//-------------------------------------------------------------------------
// Given a name, returning a pointer to the corresponding algorithm function
// or nullptr if name is not found
const EVP_MD *      crypto_md_from_name     ( const std::string &  p_name );
const EVP_CIPHER *  crypto_cipher_from_name ( const std::string &  p_name );

//-------------------------------------------------------------------------
// Calculate the hash of the message. p_algorithm is one of openSSL digests:
//    EVP_md5, EVP_sha1, EVP_sha256, EVP_sha512...
// Binary digest can be retrieved from p_digest using data() and size()
// Return true on success

bool    crypto_digest ( const std::string &  p_message,   // data to sign
                        Data &               p_digest,    // resulting digest
                        const EVP_MD *       p_algorithm = EVP_sha256() );

bool    crypto_digest ( const Data &         p_message,   // data to sign
                        Data &               p_digest,    // resulting digest
                        const EVP_MD *       p_algorithm = EVP_sha256() );

bool    crypto_digest ( const std::vector< std::reference_wrapper< const std::string > > & p_messages,  // data to sign
                        Data &                                                             p_digest,    // resulting digest
                        const EVP_MD *                                                     p_algorithm = EVP_sha256() );

bool    crypto_digest ( const std::vector< std::reference_wrapper< const Data > > &       p_messages,   // data to sign
                        Data &                                                            p_digest,     // resulting digest
                        const EVP_MD *                                                    p_algorithm = EVP_sha256() );

//-------------------------------------------------------------------------
// Calculate the keyed hash of the message. p_algorithm is one of openSSL digests:
//    EVP_md5, EVP_sha1, EVP_sha256, EVP_sha512...
// Binary digest can be retrieved from p_digest using data() and size()
// Return true on success

bool    crypto_hmac ( const Data &         p_key,       // secret key
                      const std::string &  p_message,   // data to sign
                      Data &               p_digest,    // resulting digest
                      const EVP_MD *       p_algorithm = EVP_sha256() );

bool    crypto_hmac ( const Data &    p_key,       // secret key
                      const Data &    p_message,   // data to sign
                      Data &          p_digest,    // resulting digest
                      const EVP_MD *  p_algorithm = EVP_sha256() );

bool    crypto_hmac ( const Data &                                                       p_key,       // secret key
                      const std::vector< std::reference_wrapper< const std::string > > & p_messages,  // data to sign
                      Data &                                                             p_digest,    // resulting digest
                      const EVP_MD *                                                     p_algorithm = EVP_sha256() );

bool    crypto_hmac ( const Data &                                                p_key,       // secret key
                      const std::vector< std::reference_wrapper< const Data > > & p_messages,  // data to sign
                      Data &                                                      p_digest,    // resulting digest
                      const EVP_MD *                                              p_algorithm = EVP_sha256() );

//-------------------------------------------------------------------------
// Derives a hash of a password with a salt, applying the given iteration
// count. Algorithm is one of:
//    EVP_md5, EVP_sha1, EVP_sha256, EVP_sha512...
// Output size requested must not be larger than the algorithm used:
//    SHA-1   20 bytes
//    SHA-224 28 bytes
//    SHA-256 32 bytes
//    SHA-384 48 bytes
//    SHA-512 64 bytes
bool    crypto_pbkdf2 ( const Data &    p_password,                 // password
                        const Data &    p_salt,                     // random data       64 bits at least
                        Data &          p_digest,                   // resulting digest
                        unsigned        p_iterations     = 100000,  // number of loops
                        size_t          p_requested_size = 16,      // data_out size, in bytes
                        const EVP_MD *  p_algorithm      = EVP_sha256() );

//-------------------------------------------------------------------------
// Encrypts/decrypts data. p_algorithm is one of openSSL EVP ciphers:
//    EVP_aes_128_cbc, EVP_aes_256_cbc, EVP_idea_cbc...
// Encoded data can be retrieved from p_data_out using data() and size()
// Return true on success
bool    crypto_cipher ( const Data &        p_key,                  // secret key
                        const Data &        p_iv,                   // initialization vector
                        const Data &        p_data_in,              // data to encrypt or decrypt
                        Data &              p_data_out,             // encrypted or decrypted data
                        bool                p_encrypt,              // encrypt or decrypt operation
                        const EVP_CIPHER *  p_algorithm = EVP_aes_128_cbc(),
                        bool                p_padding   = true );   // pad using standard block padding

//-------------------------------------------------------------------------
// Generates random data
// If p_requested_size is greater than 256, data are not CSPRNG
// Always succeeds if p_must_be_csprng is false
bool    crypto_random ( size_t  p_requested_size,           // number of byte to generate
                        Data &  p_data_out,                 // resulting data
                        bool    p_must_be_csprng = true );  // returns false if not

// A simple, fail safe, PRNG; should not be used
void crypto_random_prng ( size_t  p_requested_size,         // number of byte to generate
                          Data &  p_data_out );             // resulting data

//-------------------------------------------------------------------------
// Time-constant compare
bool    crypto_equal  ( const Data &      p_data_1,
                        const Data &      p_data_2 );

bool    crypto_equal  ( std::string_view  p_string_1,
                        std::string_view  p_string_2 );

//-------------------------------------------------------------------------
// One-Time Password: HOTP (RFC4226) et TOTP (RFC6238)
// Return true on success

bool    crypto_hotp  ( const Data &   p_key,
                       uint64_t       p_counter,
                       unsigned       p_digits,   // HOTP size, maximum 9
                       std::string &  p_hotp );

// Since the user may enter a code just after it changed, you may want to also check with
// the TOTP valid at that time by allowing a, for example, 5s lag by setting p_t0 to 5
bool    crypto_totp  ( const Data &   p_key,
                       unsigned       p_digits,   // TOTP size, maximum 9
                       std::string &  p_totp,
                       unsigned       p_step = 30,
                       time_t         p_t0   = 0 );

//-----------------------------------------
// Generate a private and public pair of keys for the given algorithm
// Accepts "rsa", "rsa_pss", "ed25519" or "ed448", or one of the curve name returned by:
//      openssl ecparam -list_curves
// For rsa, the number of bits can be specified in p_parameters (default is 2048 since OpenSSL 1.1.0):
//      "bits=1024"
// Return true on success, keys use X.509 format
bool    crypto_pkey_generate ( const std::string &  p_algorithm,
                               const std::string &  p_parameters,
                               adawat::Data &       p_key_private,
                               adawat::Data &       p_key_public );

//-----------------------------------------
// Converts the given key, private or public, to PEM
// Return true on success
bool    crypto_pkey_to_PEM ( const adawat::Data &  p_key,
                             std::string &         p_PEM );

//-----------------------------------------
// Read the given PEM containing a private or public key
// On success returns true and the type of key (private / public)
std::tuple< bool, bool >  // success, is_private
        crypto_pkey_from_PEM ( adawat::Data &       p_key,
                               const std::string &  p_PEM );

//-----------------------------------------
// Return the signature of the given message using a private key
// Similar to:
//  openssl dgst -sha256 -sign private.pem < text.txt > signature.bin
// Return true on success
bool    crypto_pkey_sign ( const adawat::Data &  p_key_private,
                           const adawat::Data &  p_message,
                           adawat::Data &        p_signature );

//-----------------------------------------
// Verify the signature of the given message using a public key
// Similar to:
//  openssl dgst -sha256 -verify public.pem -signature signature.bin < text.txt
// Return true on success
bool    crypto_pkey_verify ( const adawat::Data &  p_key_public,
                             const adawat::Data &  p_message,
                             const adawat::Data &  p_signature );

} // namespace adawat
