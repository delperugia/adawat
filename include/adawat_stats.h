// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Statistic related functions
//

#pragma once

#include <cstdint>
#include <cstddef>
#include <deque>

namespace adawat {

constexpr
size_t k_default_window_size = 64;

//-------------------------------------------------------------------------
// Update an average counter with a new value
void stats_update_average ( double &  p_average,
                            double    p_new_value,
                            size_t    p_window_size = k_default_window_size );

//-------------------------------------------------------------------------
// A simple object use to calculate the mean and variance of a variable
// using a variable length window.
class StatsValue
{
    public:
        //------------------------------------------
        // Constructor. Defines the sliding window size
        explicit
        StatsValue  ( size_t  p_window_size = k_default_window_size );
        //
        //------------------------------------------
        // Feed a new value
        void      add_value   ( double p_value );
        //
        //------------------------------------------
        // Accessors
        uint64_t  count       ( void ) const;
        double    max         ( void ) const;
        double    mean        ( void ) const;
        double    min         ( void ) const;
        double    sd          ( void ) const;
        double    variance    ( void ) const;
        //
        std::tuple< uint64_t, double, double, double, double, double > // count, max, mean, min, sd, variance
                  details     ( void ) const;
        //
        //------------------------------------------
        // Retrieving the max/mean/min values of the window
        double    window_max     ( void ) const;
        double    window_mean    ( void ) const;
        double    window_min     ( void ) const;
        //
        std::tuple< double, double, double > // max/mean/min
                  window_details ( void ) const;
        //
        //------------------------------------------
        // Set the new window size, existing data are kept
        void      resize      ( size_t  p_new_size );
        //
        //------------------------------------------
        // Clear all statistics (window max size remains)
        void      clear       ( void );
        //
    private:
        size_t                  m_window_size = k_default_window_size;
        std::deque< double >    m_window;
        //
        double      m_mean;
        double      m_M2;
        double      m_min;
        double      m_max;
        uint64_t    m_count;
};

} // namespace adawat
