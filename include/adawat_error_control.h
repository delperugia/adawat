// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Error control functions
//

#pragma once

#include <string>

namespace adawat {

//-------------------------------------------------------------------------
enum class error_control_type {
    luhn,    //  Luhn formula ("modulus 10")
    mod97,   //  MOD-97-10 as per ISO/IEC 7064:2003
    damm,    //  Damm algorithm
    iban,    //  IBAN with country and signature at the beginning
    rib      //  RIB with signature at the end (BBAN signature for: France, Monaco...)
};

//-------------------------------------------------------------------------
// Add a signature to the given string
// IBAN: must start with two letters country code, followed by 00 and the BBAN
// Returns false on error
bool error_control_sign ( std::string &        p_text,
                          error_control_type   p_type );

//-------------------------------------------------------------------------
// Return true if the given string has a valid signature
bool error_control_check ( const std::string &  p_text,
                           error_control_type   p_type );

//-------------------------------------------------------------------------
// Convert error_control_type to/from string
std::string  error_control_type_cvt ( error_control_type    p_type );

bool         error_control_type_cvt ( const std::string &   p_text,
                                      error_control_type &  p_type );

//-------------------------------------------------------------------------
// Returns the signature length of the given type
size_t error_control_signature_length ( error_control_type    p_type );

} // namespace adawat
