// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Thread safe template queue
//

#pragma once

#include <chrono>
#include <condition_variable>
#include <deque>
#include <map>
#include <mutex>
#include <optional>
#include <thread>

using namespace std::literals::chrono_literals;

namespace adawat {

//-------------------------------------------------------------------------
// A thread protected queue, with delayed pushed items.
// Consumers retrieve elements specifying a timeout.
//
//  ThreadQueue< int >    q;
//  int                   x = 2;
//  std::optional< int >  y;
//
//  q.push( 1 );
//  q.push( std::move(x) );
//  y = q.pop ( 5s );   std::cout << "pop: " << y.value() << endl;
//  y = q.pop ( 5s );   std::cout << "pop: " << y.value() << endl;
//
// An item can delayed when pushed:
//  q.push( 2, 10s );
//
// These events are stored in a separated 'waiting" container
// and move when the delay expired to the main 'queue'.
//
// browse_queue and browse_waiting expect a lambda like:
//
//    auto [ was_stopped, deleted_items, remaining_items ] = q.browse_queue(
//    []( auto & item ) -> std::tuple<bool, bool> {
//        return {false, true}; // stop, remove
//    });
//
//    auto [ was_stopped, deleted_items, remaining_items ] = q.browse_waiting(
//    []( auto & timepoint, auto & item ) -> std::tuple<bool, bool> {
//        return {false, true}; // stop, remove
//    });

template< typename Item >
class ThreadQueue {
    public:
        //
        ThreadQueue()
        {
            // cppcheck-suppress useInitializationList
            m_receptionist =
                std::thread ( [this] {
                    for ( ; ! m_stopping ; )
                    {
                        std::chrono::duration to_wait = 10000ms; // as a security, exit every 10s
                        //
                        std::unique_lock lock( m_waiting_mutex );
                        //
                        for ( ; ! m_waiting.empty(); ) // move all past events from m_waiting to m_queue
                        {
                            auto next_event            = m_waiting.begin(); // since m_waiting is ordered, begin() is next
                            auto next_event_time_point = next_event->first;
                            auto delta                 = std::chrono::duration_cast<std::chrono::milliseconds>(
                                                            next_event_time_point - std::chrono::steady_clock::now() );
                            //
                            if ( delta <= 0ms ) { // event is in the past
                                push( std::move( next_event->second ) ); // move Item from m_waiting to m_queue
                                m_waiting.erase( next_event );
                            } else { // event is in the future
                                to_wait = delta;
                                break;
                            }
                        }
                        //
                        // Wait notify on a push to m_waiting
                        // Do not ignore spurious awakenings because to_wait must be recalculated
                        m_waiting_monitor.wait_for( lock, to_wait );
                    }
                });
        }
        //
        virtual
        ~ThreadQueue()
        {
            m_stopping = true;
            m_waiting_monitor.notify_one(); // awake m_receptionist
            m_receptionist.join();          // wait his death
        }
        //
        //------------------------------------------
        // Accessors
        //
        // Number of consumers currently waiting in pop
        size_t count_consumers ( void ) const
        {
            std::lock_guard lock( m_queue_mutex );
            //
            return m_count_consumers;
        }
        //
        // Number of jobs pending in the queue
        size_t queue_size ( void ) const
        {
            std::lock_guard lock( m_queue_mutex );
            //
            return m_queue.size();
        }
        //
        // Number of delayed jobs waiting to be move to the queue
        size_t waiting_size ( void ) const
        {
            std::lock_guard lock( m_waiting_mutex );
            //
            return m_waiting.size();
        }
        //
        //------------------------------------------
        // Mutators
        //
        // To be called before pushing an item: estimate if the new item
        // will linger in queue or will immediately be popped by a consumer
        bool will_linger ( void ) const
        {
            std::lock_guard lock( m_queue_mutex );
            //
            return m_queue.size() >= m_count_consumers;
        }
        //
        // Removes all elements from the queue and waiting
        void clear ( void )
        {
            {
                std::lock_guard lock( m_waiting_mutex );
                //
                m_waiting.clear();
            }
            {
                std::lock_guard lock( m_queue_mutex );
                //
                m_queue.clear();
                m_queue.shrink_to_fit();
            }
        }
        //
        // Push a new item in the queue or waiting container, with copy
        // https://stackoverflow.com/questions/21439359/signal-on-condition-variable-without-holding-lock
        void push ( const Item &              p_item,
                    std::chrono::milliseconds p_delay = 0ms )
        {
            if ( p_delay == 0ms ) {
                {
                    std::lock_guard lock( m_queue_mutex );
                    m_queue.push_back( p_item );
                }
                //
                m_queue_monitor.notify_one();
            } else {
                {
                    std::lock_guard lock( m_waiting_mutex );
                    m_waiting.insert( { std::chrono::steady_clock::now() + p_delay, p_item } );
                }
                //
                m_waiting_monitor.notify_one();
            }
        }
        //
        // Push a new item in the queue or waiting container, to be called with std::move
        // https://stackoverflow.com/questions/21439359/signal-on-condition-variable-without-holding-lock
        void push ( Item &&                   p_item,
                    std::chrono::milliseconds p_delay = 0ms )
        {
            if ( p_delay == 0ms ) {
                {
                    std::lock_guard lock( m_queue_mutex );
                    m_queue.push_back( std::move( p_item ) );
                }
                //
                m_queue_monitor.notify_one();
            } else {
                {
                    std::lock_guard lock( m_waiting_mutex );
                    m_waiting.insert( { std::chrono::steady_clock::now() + p_delay, std::move( p_item ) } );
                }
                //
                m_waiting_monitor.notify_one();
            }
        }
        //
        // Try to pop an item from the queue.
        // Returns nothing on timeout
        std::optional<Item> pop ( const std::chrono::milliseconds p_timeout )
        {
            std::unique_lock lock( m_queue_mutex );
            //
            m_count_consumers++;
            //
            bool signaled =
                m_queue_monitor.wait_for( lock,
                                          p_timeout,
                                          [this]{ return ! m_queue.empty(); } );  // loop on spurious awakenings
            //
            m_count_consumers--;  // the mutex is relocked when exiting wait_for
            //
            if ( signaled )
            {
                Item item = std::move( m_queue.front() );
                m_queue.pop_front();
                return item;
            }
            else
            {
                return std::nullopt;
            }
        }
        //
        // Filter elements from the queue
        // Expects a lambda taking a ITEM reference and returning std::tuple<bool,bool>:
        //   first bool indicates if browsing must stop, second if current item must be deleted from queue
        // Returns a tuple with: was_stopped, deleted_items, remaining_items
        template <typename Lambda>
        std::tuple<bool, size_t, size_t> browse_queue ( Lambda && p_lambda )
        {
            std::lock_guard lock( m_queue_mutex );
            //
            size_t original_size = m_queue.size();
            bool   stopped       = false, remove;
            //
            for ( auto iter = m_queue.begin(); iter != m_queue.end() && ! stopped; )
            {
                std::tie( stopped, remove ) = p_lambda( * iter );
                //
                if ( remove )
                    iter = m_queue.erase( iter );
                else
                    iter++;
            }
            //
            return { stopped, original_size - m_queue.size(), m_queue.size() };
        }
        //
        // Filter elements from the waiting list
        // Expects a lambda taking a time_point and an ITEM reference and, returning std::tuple<bool,bool>:
        //   first bool indicates if browsing must stop, second if current item must be deleted from waiting list
        // Returns a tuple with: was_stopped, deleted_items, remaining_items
        template <typename Lambda>
        std::tuple<bool, size_t, size_t> browse_waiting ( Lambda && p_lambda )
        {
            std::lock_guard lock( m_waiting_mutex );
            //
            size_t original_size = m_waiting.size();
            bool   stopped       = false, remove;
            //
            for ( auto iter = m_waiting.begin(); iter != m_waiting.end() && ! stopped; )
            {
                std::tie( stopped, remove ) = p_lambda( iter->first, iter->second );
                //
                if ( remove )
                    iter = m_waiting.erase( iter );
                else
                    iter++;
            }
            //
            return { stopped, original_size - m_waiting.size(), m_waiting.size() };
        }
        //
    private:
        // Items waiting to be pushed in queue
        mutable
        std::mutex               m_waiting_mutex;           // to protect the data
        std::condition_variable  m_waiting_monitor;         // to wait for data
        std::map< std::chrono::steady_clock::time_point, Item >
                                 m_waiting;                 // the ordered waiting list itself
        //
        // Items in queue waiting to be popped
        mutable
        std::mutex               m_queue_mutex;             // to protect the data
        std::condition_variable  m_queue_monitor;           // to wait for data
        std::deque< Item >       m_queue;                   // the queue itself
        size_t                   m_count_consumers = 0;     // number of consumers currently waiting in pop
        //
        std::thread              m_receptionist;            // thread moving items from waiting to queue
        bool                     m_stopping        = false; // stop the waiting thread
};

} // namespace adawat
