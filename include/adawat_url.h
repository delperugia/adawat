// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// URL functions
//

#pragma once

#include <string>

#include "adawat_string.h"

namespace adawat {

//-------------------------------------------------------------------------
// Same as the curl curl_easy_escape
std::string url_encode ( std::string_view  p_encoded );

//-------------------------------------------------------------------------
// Same as the curl curl_easy_unescape
bool url_decode ( std::string_view  p_encoded,
                  std::string &     p_decoded );

//-------------------------------------------------------------------------
// Build the URL encoded string of the given parameters
// p_parameters is an associative container with pair element
// like std::map or std::multimap.
template< typename T >
std::string url_encode_parameters ( const T &  p_parameters )
{
    std::string  url;
    bool         first = true;
    //
    url.reserve( p_parameters.size() * 64 );  // arbitrary size per parameter
    //
    for ( const auto & parameter: p_parameters ) {
        if ( first )  // no delimiter before first element
            first = false;
        else
            url += '&';
        //
        url += url_encode( parameter.first ) +
               '=' +
               url_encode( parameter.second );
    }
    //
    return url;
}

//-------------------------------------------------------------------------
// Extract an URL encoded string into an associative container
// with pair element like std::map or std::multimap
template< typename T >
bool url_decode_parameters ( std::string_view  p_url_encoded,
                             T &               p_parameters )
{
    p_parameters.clear();
    //
    auto elements = str_split_view( p_url_encoded, "&" );
    //
    for ( auto & element: elements )      // element are like "a=1"
    {
        std::string_view key;
        std::string      key_decoded, element_decoded;
        //
        if ( element.empty() )            // a=1&&b=2
            return false;
        //
        key = str_token_view( element, "=" );  // key="a" element="1" (modify elements)
        //
        if ( ! url_decode( key,     key_decoded     ) ||
             ! url_decode( element, element_decoded ) )
            return false;
        //
        p_parameters.insert( { key_decoded, element_decoded } );
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Add parameters (and URL encode them) to an existing URL
// with or without existing parameters.
// Parameters can be added or replace existing ones.
// Parameters can be set as fragment parameters or not
template< typename T >
std::string url_add_parameters ( const std::string &  p_url,
                                 const T &            p_parameters,
                                 bool                 p_as_fragment = false,
                                 bool                 p_replace     = false )
{
    std::string new_url         = p_url,
                parameter_part,
                fragment_part;
    //
    // Extract the existing fragment part (host + parameter parts remain in new_url)
    if ( size_t pos = new_url.find_last_of( '#' ); pos != std::string::npos )
    {
        fragment_part = new_url.substr( pos );
        new_url.erase( pos );
    }
    //
    // Extract the existing parameter part (host part remain in new_url)
    if ( size_t pos = new_url.find_last_of( '?' ); pos != std::string::npos )
    {
        parameter_part = new_url.substr( pos );
        new_url.erase( pos );
    }
    //
    if ( p_as_fragment ) {
        // If there is already some parameters, start with separator '&'
        if ( fragment_part.empty() || p_replace )
            fragment_part  = '#' + url_encode_parameters( p_parameters );
        else
            fragment_part += '&' + url_encode_parameters( p_parameters );
    } else {
        // If there is already some parameters, start with separator '&'
        if ( parameter_part.empty() || p_replace )
            parameter_part  = '?' + url_encode_parameters( p_parameters );
        else
            parameter_part += '&' + url_encode_parameters( p_parameters );
    }
    //
    // Rebuild full URL
    return new_url + parameter_part + fragment_part;
}

} // namespace adawat
