// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Dynamic thread pool template
//

#pragma once

#include <chrono>
#include <mutex>
#include <thread>
#include <unordered_set>

#include "adawat_threading_queue.hpp"

using namespace std::literals::chrono_literals;

namespace adawat {

//-------------------------------------------------------------------------
// A call handling a queue of jobs to process with worker threads
// Threads are started / stopped dynamically.
// Inherit from this template and implement the execute method to process jobs
//
//  class Workers : public ThreadPool< int >
//  {
//  protected:
//     void execute ( int && u )
//     {
//        std::cout << "work on " << u << endl;
//        std::this_thread::sleep_for( 500ms );
//     }
//  };
//  ...
//  Workers  w;
//  int x = 2;
//
//  w.push( 1 );
//  w.push( std::move(x) ); // move rvalue reference
//
template< typename Job >
class ThreadPool {
    //
    public:
        //
        //------------------------------------------
        ThreadPool                 ()                     = default;
        //
        //------------------------------------------
        // These operations are deleted by the mutex
        ThreadPool                 ( ThreadPool const & ) = delete;
        ThreadPool &  operator =   ( ThreadPool const & ) = delete;
        ThreadPool                 ( ThreadPool && )      = delete;
        ThreadPool &  operator =   ( ThreadPool && )      = delete;
        //
        //------------------------------------------
        virtual
        ~ThreadPool ()
        {
            stop( true );
        }
        //
        //------------------------------------------
        // Accessors
        //
        // Current number of jobs waiting
        size_t count_jobs_waiting ( void ) const
        {
            return m_jobs.waiting_size();
        }
        //
        // Current number of enqueued jobs
        size_t count_jobs_pending ( void ) const
        {
            return m_jobs.queue_size();
        }
        //
        // Current number of jobs running
        size_t count_jobs_running ( void ) const
        {
            return count_threads() - m_jobs.count_consumers();
        }
        //
        // Current number of threads in the pool
        size_t count_threads ( void ) const
        {
            std::lock_guard lock( m_mutex );
            //
            return m_threads.size();
        }
        //
        // Delay an idle thread waits before exiting
        std::chrono::seconds linger_delay ( void ) const
        {
            std::lock_guard lock( m_mutex );
            //
            return m_linger_delay;
        }
        //
        // Maximum number of threads in the pool
        size_t max_threads ( void ) const
        {
            std::lock_guard lock( m_mutex );
            //
            return m_max_threads;
        }
        //
        // Minimum number of threads in the pool
        size_t min_threads ( void ) const
        {
            std::lock_guard lock( m_mutex );
            //
            return m_min_threads;
        }
        //
        //------------------------------------------
        // Mutators
        //
        // Delay an idle thread waits before exiting, returns previous value
        std::chrono::seconds set_linger_delay ( std::chrono::seconds  p_delay )
        {
            std::lock_guard lock( m_mutex );
            //
            std::swap( m_linger_delay, p_delay );
            return p_delay;
        }
        //
        // Maximum number of threads in the pool
        size_t set_max_threads  ( size_t p_max )
        {
            std::lock_guard lock( m_mutex );
            //
            std::swap( m_max_threads, p_max );
            return p_max;
        }
        //
        // Minimum number of threads in the pool
        size_t set_min_threads  ( size_t  p_min )
        {
            std::lock_guard lock( m_mutex );
            //
            std::swap( m_min_threads, p_min );
            return p_min;
        }
        //
        //------------------------------------------
        // Enqueue a job, with copy
        // Returns false if queue is stopping
        bool push ( const Job &               p_job,
                    std::chrono::milliseconds p_delay = 0ms )
        {
            if ( m_stopping )
                return false;
            //
            check_add_worker();  // if needed and possible, create a new worker thread
            //
            m_jobs.push( p_job, p_delay );
            return true;
        }
        //
        //------------------------------------------
        // Enqueue a job, to be called with std::move
        // Returns false if queue is stopping
        bool push ( Job &&                    p_job,
                    std::chrono::milliseconds p_delay = 0ms )
        {
            if ( m_stopping )
                return false;
            //
            check_add_worker();  // if needed and possible, create a new worker thread
            //
            m_jobs.push( std::forward< Job >( p_job ), p_delay );
            return true;
        }
        //
        //------------------------------------------
        // Reactivate the object, if it had ever been stopped
        void start ( void )
        {
            m_stopping = false;
        }
        //
        //------------------------------------------
        // Stop worker threads and wait for them to exit
        // New jobs will be rejected
        // Eventually purge pending jobs from the queue
        // Note: even if not purging, does not wait for
        // delayed jobs. You have to check idle() before
        // if you need all waiting and queued jobs to be
        // completed
        void stop ( bool p_purge_jobs )
        {
            m_stopping = true;
            //
            if ( p_purge_jobs )
                m_jobs.clear();
            //
            // Wait for all threads to finish
            while ( count_threads() > 0 )
                std::this_thread::sleep_for( 10ms );
        }
        //
        //------------------------------------------
        // Returns true if all pushed jobs have been processed:
        // if the queue is empty and all threads are waiting
        bool idle ( void ) const
        {
            return count_jobs_waiting() == 0 &&
                   count_jobs_pending() == 0 &&
                   count_threads()      == m_jobs.count_consumers();
        }
        //
        //------------------------------------------
        // Access to the queue browsing functions
        template <typename Lambda>
        std::tuple<bool, size_t, size_t> browse_queue ( Lambda && p_lambda )
        {
            return m_jobs.browse_queue(p_lambda);
        }
        //
        template <typename Lambda>
        std::tuple<bool, size_t, size_t> browse_waiting ( Lambda && p_lambda )
        {
            return m_jobs.browse_waiting( p_lambda );
        }
        //
    protected:
        //------------------------------------------
        // Overload this function with your code
        virtual
        void execute ( Job && p_job ) = 0;  // a rvalue if the derived class wants to forward it
        //
        //------------------------------------------
        // Ensure there is the minimum configured workers and
        // if the queue is not empty, try to create a new one
        // The worker thread code is defined as a lambda here
        void check_add_worker ( void )
        {
            std::lock_guard lock( m_mutex );
            //
            if ( ( m_threads.size() < m_min_threads ) ||                          // minimum configured
                 ( m_jobs.will_linger() && m_threads.size() < m_max_threads ) )   // useful and possible
            {
                // Create a new worker thread...
                std::thread worker ( [this] {                                     // thread code
                    for ( auto waiting = 0s; ; ) {                                // count lingering time
                        auto job = m_jobs.pop( m_stopping ? 100ms : 1s );         // job is an std::optional
                        //                                                        //
                        if ( job.has_value() ) {                                  // poll job (use small delays to reduce stop() time)
                            waiting = 0s;                                         //   reset the timeout on success
                            execute( std::move( job.value() ) );                  //   execute it
                        } else {                                                  //
                            waiting += 1s;                                        // if pop timed out, it must be 1s
                            //                                                    //
                            std::lock_guard thread_lock( m_mutex ); // timeout means there is no more job:
                            //                                                    //
                            if ( m_stopping ||                                    // if stopping or
                                ( waiting >= m_linger_delay &&                    //    linger too much
                                m_threads.size() > m_min_threads ) )              //      and enough remaining threads
                            {                                                     //
                                m_threads.erase( std::this_thread::get_id() );    // unregister this worker
                                break;                                            // and stop
                            }                                                     //
                        }                                                         //
                    }                                                             //
                });
                //
                m_threads.insert( worker.get_id() );  // keep the thread id then
                worker.detach();                      // let the thread live his life
            }
        }
        //
    private:
        mutable
        std::mutex              m_mutex;                 // to protect data
        //
        std::unordered_set< std::thread::id >
                                m_threads;               // map of running worker threads' id
        //
        ThreadQueue< Job >      m_jobs;                  // the queue of pending jobs
        //
        size_t                  m_min_threads  = 1;      // minimum number of threads in the pool
        size_t                  m_max_threads  = 2;      // maximum number of threads in the pool
        std::chrono::seconds    m_linger_delay = 60s;    // delay an idle thread waits before exiting
        bool                    m_stopping     = false;  // will refuse new job and wait thread's termination
};

} // namespace adawat
