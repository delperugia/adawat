// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// String manipulation functions
//

#pragma once

#include <limits>
#include <sstream>
#include <string>
#include <vector>

namespace adawat {

//-------------------------------------------------------------------------
// List of characters used as delimiters by default
constexpr
char k_whitespaces[] = " \t\n\v\f\r\0";

//-------------------------------------------------------------------------
// Make a string upper / lower case
// Return reference on the string
std::string &   str_lower ( std::string &  p_text );
std::string &   str_upper ( std::string &  p_text );

//-------------------------------------------------------------------------
// Strip whitespaces (or other characters) from the beginning and/or end of a string
// Return reference on the string
std::string &   str_ltrim ( std::string &     p_text,
                            std::string_view  p_white_chars = k_whitespaces );
std::string &   str_rtrim ( std::string &     p_text,
                            std::string_view  p_white_chars = k_whitespaces );
std::string &   str_trim  ( std::string &     p_text,
                            std::string_view  p_white_chars = k_whitespaces );

//-------------------------------------------------------------------------
// Replace all occurrences of the search string with the replacement string
// Return reference on the string
std::string &   str_replace ( std::string &     p_text,
                              std::string_view  p_search,
                              std::string_view  p_replace_by,
                              size_t            p_count = std::numeric_limits< size_t >::max() );  // limit the number of replacements

//-------------------------------------------------------------------------
// Split / rebuild elements of a string separated by delimiter
// str_split:
//    - set rest of the string as the last element if p_uLimit is reached
//      (resulting set size is then p_uLimit + 1)
// str_join is a generic version that accept containers of any type,
// using p_glue as the separator:
//    str_join ( std::vector<int>   ({1, 2, 3, 4, 5}) )
//    str_join ( std::list<int>     ({1, 2, 3, 4, 5}) )
//    str_join ( std::vector<string>({"a","b","c"}  ) )

// p_count limits the number of items, the rest of the string is
// put in a last element. So the total size can be p_count + 1.
std::vector< std::string >      str_split      ( const std::string &  p_text,
                                                 std::string_view     p_separator,
                                                 size_t               p_count = std::numeric_limits< size_t >::max() );

std::vector< std::string_view>  str_split_view ( std::string_view     p_text,
                                                 std::string_view     p_separator,
                                                 size_t               p_count = std::numeric_limits< size_t >::max() );

template< typename T >
std::string str_join ( const T &        p_elements,
                       std::string_view p_glue  = "," )
{
    std::ostringstream stream;
    bool               first = true;
    //
    for ( const auto & element: p_elements ) {
        if ( first )  // no delimiter before first element
            first = false;
        else
            stream << p_glue;
        //
        stream << element;
    }
    //
    return stream.str();
}

//-------------------------------------------------------------------------
// Extract and return the first word of a string
// Return empty if no word is found
std::string       str_token      ( std::string &       p_text,
                                   std::string_view    p_white_chars = k_whitespaces );

std::string_view  str_token_view ( std::string_view &  p_text,
                                   std::string_view    p_white_chars = k_whitespaces );

//-------------------------------------------------------------------------
// Returns true if a key is in a CSV string
bool            str_csv_has_value ( std::string_view  p_key,
                                    std::string_view  p_csv,
                                    char              p_separator = ',' );

//-------------------------------------------------------------------------
// Return a formatted string using the standard printf format
// Optimized for resulting strings smaller than 1KB
std::string     str_sprintf ( const char *  p_format,
                              ... );

//-------------------------------------------------------------------------
// Prefix and suffix comparison
// Return true is found
bool    str_starts_with ( std::string_view  p_text,
                          std::string_view  p_prefix );

bool    str_ends_with   ( std::string_view  p_text,
                          std::string_view  p_suffix );

} // namespace adawat
