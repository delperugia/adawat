// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Various base encoding and decoding functions
//

#pragma once

#include <string>

#include "adawat_common.h"

namespace adawat {

//-------------------------------------------------------------------------
// Base16, Base32, and Base64 Data Encodings (RFC 4648)

enum class base64_type {
    standard,   // regular base64, RFC 4648
    url,        // URL and Filename safe alphabet, RFC 4648
    xxencoding  // Xxencoding alphabet
};

// Returns the encoded data as a string
std::string     encoding_base16_encode ( const Data &  p_data_in,
                                         bool          p_lower_case = false );

std::string     encoding_base32_encode ( const Data &  p_data_in,
                                         bool          p_base32hex  = false,   // use triacontakaidecimal
                                         bool          p_padding    = true );  // with = padding

std::string     encoding_base64_encode ( const Data &  p_data_in,
                                         base64_type   p_type       = base64_type::standard,
                                         bool          p_padding    = true );  // with = padding

// Returns false if the input text is invalid; set p_data_out
bool    encoding_base16_decode ( std::string_view  p_text_in,
                                 Data &            p_data_out );

bool    encoding_base32_decode ( std::string_view  p_text_in,
                                 Data &            p_data_out,
                                 bool              p_base32hex  = false );  // use triacontakaidecimal

bool    encoding_base64_decode ( std::string_view  p_text_in,
                                 Data &            p_data_out,
                                 base64_type       p_type       = base64_type::standard );

} // namespace adawat
