# Adawat

## Table of Contents

- [About](#about)
- [Installation](#installation)
  - [Building](#building)
  - [Usage example](#usage-example)
- [API Reference](#api-reference)
  - [adawat_common_h](#adawat_common_h)
  - [adawat_crashhandler_h](#adawat_crashhandler_h)
  - [adawat_crypto_h](#adawat_crypto_h)
  - [adawat_encoding_h](#adawat_encoding_h)
  - [adawat_error_control_h](#adawat_error_control_h)
  - [adawat_file_h](#adawat_file_h)
  - [adawat_money_h](#adawat_money_h)
  - [adawat_stats_h](#adawat_stats_h)
  - [adawat_string_h](#adawat_string_h)
  - [adawat_time_h](#adawat_time_h)
  - [adawat_threading_pool_hpp](#adawat_threading_pool_hpp)
  - [adawat_threading_queue_hpp](#adawat_threading_queue_hpp)
  - [adawat_uid_h](#adawat_uid_h)
  - [adawat_url_h](#adawat_url_h)
- [License](#license)

## About

A general purpose C++17 library providing the following features
(non-exhaustive list):

- crash handling
- cryptology functions
- encoding functions
- file functions
- money calculation
- statistics
- string functions
- threading pool
- threading queue
- time functions

## Installation

Adawat was tested on:

- Ubuntu 24.04
- Ubuntu 22.04
- Ubuntu 20.04
- Ubuntu 18.04
- Fedora 29
- Debian 10

using:
- GCC 13.2
- GCC 11.2
- GCC 9.3
- GCC 6.3
- GCC 7.3
- GCC 8.3
- Clang 6.0

The following packages must be installed first:

- libbfd-dev (at least 2.28)
- libssl-dev (at least 1.1)

Licenses of these modules are available here:

- BFD [license](https://www.gnu.org/licenses/gpl-3.0.en.html)
- OpenSSL [license](https://www.apache.org/licenses/LICENSE-2.0.txt)

Since BFD (used by the crashhandler in Adawat) is GPL v3 licensed, it can only
be used if the resulting software is also licensed under GPL v3.

To disable it, unset the define macro `GNU_GPL_COMPATIBLE` in the source
file **adawat_crashhandler.cpp**, or define when compiling the
macro `ADAWAT_NOT_GNU_GPL_COMPATIBLE`.

### Building

```shell
  sudo apt-get install make g++ libbfd-dev libssl-dev
  make
  sudo make install
```

### Usage example

```cpp
  #include <iostream>
  #include <adawat/adawat_common.h>
  #include <adawat/adawat_encoding.h>

  int main( int argc, char* argv [] )
  {
      adawat::Data  data = { 0x01, 0xFF };
      //
      std::cout << adawat::encoding_base16_encode( data ) << std::endl;
  }
```

and:

```shell
  g++ test.cpp -ladawat
```

## API Reference

Below is the list of functions and classes available, per file.

### adawat_common_h

| Classes    | Description                                      |
| :--------- | :----------------------------------------------- |
| Data       | type used in Adawat for binary data manipulation |
| set_data   | copy from std::string to Data                    |
| get_data   | copy from Data to std::string                    |
| operator<< | insert operators into/from Data and std::string  |

### adawat_crashhandler_h

| Functions          | Description                      |
| :----------------- | :------------------------------- |
| crashhandler_setup | generate report on program crash |

### adawat_crypto_h

Hash and digest functions provided by OpenSSL are listed
using the command line:

```shell
  openssl help
```

This includes the common hash functions:

- sha256, sha512, sha3-224, sha3-256, sha3-384, sha3-512
- blake2s256, blake2b512

and cipher functions:

- aes-128-cbc, aes-256-cbc

Public key algorithms supported are:

- Edwards curves `ed25519` and `ed448`
- elliptic curves listed by `openssl ecparam -list_curves` (e.g.: `secp224r1`, `prime256v1`...)
- `rsa` and `rsa_pss`

| Functions            | Description                                   |
| :------------------- | :-------------------------------------------- |
| crypto_cipher        | various (de)crypting functions                |
| crypto_digest        | various hash functions                        |
| crypto_hmac          | hash message authentication code              |
| crypto_last_errors   | returns and empties the error queue           |
| crypto_pbkdf2        | password-based key derivation function 2      |
| crypto_random        | generates random data                         |
| crypto_equal         | time-constant compare                         |
| crypto_hotp          | HOTP generation function                      |
| crypto_totp          | TOTP generation function                      |
| crypto_pkey_generate | generate a private and public pair of keys    |
| crypto_pkey_to_PEM   | returns the private or public key as a PEM    |
| crypto_pkey_from_PEM | read a PEM containing a private or public key |
| crypto_pkey_sign     | signature a message using a private key       |
| crypto_pkey_verify   | verify a signature using a public key         |

### adawat_encoding_h

Encode and decode data stored in a Data object using various
bases and symbols.

| Functions              | Description                        |
| :--------------------- | :--------------------------------- |
| encoding_base16_decode | in lower or upper case             |
| encoding_base16_encode | in lower or upper case             |
| encoding_base32_decode | in base32, base32hex               |
| encoding_base32_encode | in base32, base32hex               |
| encoding_base64_decode | in base64, base64url or Xxencoding |
| encoding_base64_encode | in base64, base64url or Xxencoding |

### adawat_error_control_h

Error control functions (Luhn, Mod97, Damm, IBAN, RIB)

| Functions                     | Description                                          |
| :-----------------------------| :--------------------------------------------------- |
| error_control_sign            | add a check digit to the given string                |
| error_control_check           | check that the given string has a valid check digit  |
| error_control_type_cvt        | error_control_type type-to-text conversion functions |
| error_control_signature_length| returns the signature length of the given type       |

### adawat_file_h

| Functions   | Description                               |
| :---------- | :---------------------------------------- |
| file_append | append the string or Data into a file     |
| file_read   | read the content of a text or binary file |
| file_write  | write the string or Data into a file      |

### adawat_money_h

| Classes  | Description                            |
| :------- | :------------------------------------- |
| Amount   | basic operations on value and currency |
| Currency | class representing a currency          |

### adawat_stats_h

| Classes    | Description                                                         |
| :--------- | :------------------------------------------------------------------ |
| StatsValue | simple object used to calculate the mean and variance of a variable |

| Functions            | Description                                |
| :------------------- | :----------------------------------------- |
| stats_update_average | update an average counter with a new value |

### adawat_string_h

| Functions         | Description                                       |
| :---------------- | :------------------------------------------------ |
| str_csv_has_value | find key in csv                                   |
| str_ends_with     | suffix comparison                                 |
| str_join          | concat elements of a container as a string        |
| str_lower         | make a string lower case                          |
| str_ltrim         | strip whitespaces from the beginning              |
| str_replace       | substitute a text by another in a string          |
| str_rtrim         | strip whitespaces from the end                    |
| str_split         | split elements of a string by a delimiter         |
| str_split_view    | split elements of a string by a delimiter         |
| str_sprintf       | formats a string using the standard printf format |
| str_starts_with   | prefix comparison                                 |
| str_token         | extract and return next word of a string          |
| str_token_view    | extract and return next word of a string          |
| str_trim          | strip whitespaces from the beginning and end      |
| str_upper         | make a string upper case                          |

### adawat_time_h

| Functions                   | Description                                       |
| :-------------------------- | :------------------------------------------------ |
| time_format_rfc3339_local   | format like: 1996-12-19T16:39:57-08:00            |
| time_format_rfc3339_utc     | format like: 1985-04-12T23:20:50.52Z              |
| time_last_midnight          | absolute time of the last midnight                |
| time_next_midnight          | absolute time of the next midnight                |
| time_parse_duration         | convert string like "2d4h" into number of seconds |
| time_parse_rfc3339          | RFC 3339 string has an absolute time              |
| time_seconds_since_midnight | number of seconds since midnight                  |
| time_seconds_to_midnight    | number of seconds to the next midnight            |
| time_parse_iso8601_duration | parse an ISO 8601 duration as a number of seconds |

### adawat_threading_pool_hpp

| Classes    | Description            |
| :--------- | :--------------------- |
| ThreadPool | pool of worker threads |

### adawat_threading_queue_hpp

| Classes     | Description            |
| :---------- | :--------------------- |
| ThreadQueue | thread protected queue |

### adawat_uid_h

| Functions        | Description                                    |
| :--------------- | :--------------------------------------------- |
| uid_ts           | unique identifiers, with type prefix           |
| uid_no_ts        | unique identifiers, without type prefix        |
| uid_daily        | unique per day identifier                      |
| uid_type_cvt     | uid_type type-to-text conversion functions     |
| uid_alphabet_cvt | uid_alphabet type-to-text conversion functions |
| uid_uuid         | UUID v4                                        |

### adawat_url_h

| Functions             | Description                       |
| :-------------------- | :-------------------------------- |
| url_encode            | URL-encode a string               |
| url_decode            | URL-decode a string               |
| url_encode_parameters | URL-encode a query string         |
| url_decode_parameters | URL-decode a query string         |
| url_add_parameters    | add parameters to an existing URL |

## License

Copyright 2018-2024 Pierre del Perugia

[Apache license v2](https://www.apache.org/licenses/LICENSE-2.0.txt)
