// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <cassert>
#include <stdexcept>

#include "adawat_string.h"
#include "adawat_error_control.h"

namespace adawat {

//-------------------------------------------------------------------------
// Luhn
namespace {

    // Luhn code calculation
    bool error_control_luhn_calc ( const std::string &  p_text,
                                   bool                 p_second,  // start from last digit
                                   int &                p_sum )
    {
        // Doubled digit value modulo 10
        static const int doubled [ 10 ]  = { 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 };
        //
        p_sum = 0;
        //
        for ( auto i = p_text.rbegin(); i != p_text.rend(); i++ )
        {
            int digit = static_cast< int >( *i - '0' );
            if ( digit < 0 || digit > 9 )
                return false;  // not a digit
            //
            p_sum += ( p_second = ! p_second ) ?
                        digit :
                        doubled[ digit ];
        }
        //
        return true;
    }

    //-------------------------------------------------------------------------
    // Returns the given string with its Luhn code suffixed
    // p_text is returned unchanged if it doesn't contains only digits
    bool error_control_luhn_sign ( std::string &  p_text )
    {
        int sum;
        //
        // The first digit doubled is immediately to the left of the check digit
        // Since the check digit is not yet added, start from last
        if ( ! error_control_luhn_calc( p_text, true, sum ) )
            return false;
        //
        p_text += std::to_string( ( sum * 9 ) % 10 );
        //
        return true;
    }

    //-------------------------------------------------------------------------
    // Return true if the given string has a valid Luhn signature
    bool error_control_luhn_check ( const std::string &  p_text )
    {
        int sum;
        //
        // The first digit doubled is immediately to the left of the check digit
        // (the last one), so last one is not doubled
        if ( ! error_control_luhn_calc( p_text, false, sum ) )
            return false;
        //
        return ( sum % 10 ) == 0;
    }

}; // namespace Luhn

//-------------------------------------------------------------------------
// Damm
namespace {

    // Damm code calculation as digit between '0' and '9'
    bool error_control_damm_calc ( const std::string &  p_text,
                                   char &               p_code )
    {
        static const char matrix[ 10 ][ 10 ] = {
            { 0, 3, 1, 7, 5, 9, 8, 6, 4, 2 },
            { 7, 0, 9, 2, 1, 5, 4, 8, 6, 3 },
            { 4, 2, 0, 6, 8, 7, 1, 3, 5, 9 },
            { 1, 7, 5, 0, 9, 8, 3, 4, 2, 6 },
            { 6, 1, 2, 3, 0, 4, 5, 9, 7, 8 },
            { 3, 6, 7, 4, 2, 0, 9, 5, 8, 1 },
            { 5, 8, 6, 9, 7, 2, 0, 1, 3, 4 },
            { 8, 9, 4, 5, 3, 6, 2, 0, 1, 7 },
            { 9, 4, 3, 8, 6, 1, 7, 2, 0, 5 },
            { 2, 5, 8, 1, 4, 3, 6, 7, 9, 0 }
        };
        //
        int interim = 0;
        //
        for ( auto c: p_text )
        {
            int digit = static_cast< int >( c - '0' );
            if ( digit < 0 || digit > 9 ) return false;  // not a digit
            //
            interim = matrix[ interim ][ digit ];
        }
        //
        p_code = '0' + interim;
        //
        return true;
    }

    //-------------------------------------------------------------------------
    // Returns the given string with its Damm code suffixed
    bool error_control_damm_sign ( std::string &  p_text )
    {
        char code;
        //
        if ( ! error_control_damm_calc( p_text, code ) )
            return false;
        //
        p_text += code;
        //
        return true;
    }

    //-------------------------------------------------------------------------
    // Return true if the given string has a valid Damm signature
    bool error_control_damm_check ( const std::string &  p_text )
    {
        char code;
        //
        if ( ! error_control_damm_calc( p_text, code ) )
            return false;
        //
        return code == '0';
    }

}; // namespace Damm

//-------------------------------------------------------------------------
// MOD-97
namespace {

    // Used piecewise calculation of the modulo, with chunk of 16 digits:
    // when adding the 2 remainder digits, it is still below the unsigned
    // long long limit
    bool error_control_mod97_calc ( const std::string &   p_text,
                                    unsigned long long &  p_code )
    {
        size_t              pos = 0;
        unsigned long long  mod = 0, chunk;
        std::string         rem;
        char                c;
        //
        do {
            auto next = p_text.substr( pos, 16 );                                // take next chunk
            //
            if ( sscanf( ( rem + next ).c_str(), "%llu%c", &chunk, &c ) != 1 )   // previous mod + this chunk...
                return false;
            //
            mod  = chunk % 97;                                                   // mod 97
            rem  = std::to_string( mod );                                        // remainder as string
            pos += next.length();                                                // stop on last chunk
        } while ( pos < p_text.length() );
        //
        p_code = mod;
        return true;
    }

    //-------------------------------------------------------------------------
    // Add the MOD-97 signature to the given string
    // p_complement is either 97 or 98
    // Returns false on error
    bool error_control_mod97_sign ( std::string &  p_text )
    {
        // Start by adding the placeholder of the signature
        p_text += "00";
        //
        // Calculate the modulo
        unsigned long long  mod;
        if ( ! error_control_mod97_calc( p_text, mod ) )
            return false;
        //
        // Replace the placeholder with 97 complement to have 1 during check
        p_text.replace( p_text.length() - 2,
                        2,
                        adawat::str_sprintf( "%02llu", 98 - mod ) );
        //
        return true;
    }

    //-------------------------------------------------------------------------
    // Return true if the given string has a valid MOD-97 signature
    bool error_control_mod97_check ( const std::string &  p_text )
    {
        // Calculate the modulo
        unsigned long long  mod;
        if ( ! error_control_mod97_calc( p_text, mod ) )
            return false;
        //
        // The remainder of the division by 97 must be 1.
        return mod == 1;
    }

}; // namespace mod97

//-------------------------------------------------------------------------
// IBAN
namespace {

    //-------------------------------------------------------------------------
    // Replace letters present in an IBAN by digits
    std::string error_control_iban_letter_subst ( std::string  p_iban )
    {
        for (;;)
        {
            auto pos = p_iban.find_first_of( "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            if ( pos == std::string::npos )
                break;
            //
            p_iban.replace( pos,
                            1,
                            std::to_string( p_iban[ pos ] - 'A' + 10 ) ); // A -> 10, B -> 11...
        }
        //
        return p_iban;
    }

    //-------------------------------------------------------------------------
    // Set the IBAN signature to the given string (in 3rd and 4th position)
    // Must start with two letters country code, followed by 00 and the BBAN: "FR0030..."
    // Returns false on error
    bool error_control_iban_sign ( std::string &  p_iban )
    {
        if ( p_iban.length() < 5 )
            return false;
        //
        std::string text = error_control_iban_letter_subst( p_iban.substr( 4 ) + p_iban.substr( 0, 4 ) ); // move 4 first chars to the end
        //
        // Calculate the modulo
        unsigned long long  mod;
        if ( ! error_control_mod97_calc( text, mod ) )
            return false;
        //
        // Replace the placeholder with 97 complement to have 1 during check
        p_iban.replace( 2,
                        2,
                        adawat::str_sprintf( "%02llu", 98 - mod ) );
        //
        return true;
    }

    //-------------------------------------------------------------------------
    // Return true if the given string has a valid IBAN signature
    bool error_control_iban_check ( const std::string &  p_iban )
    {
        if ( p_iban.length() < 5 )
            return false;
        //
        std::string text = error_control_iban_letter_subst( p_iban.substr( 4 ) + p_iban.substr( 0, 4 ) ); // move 4 first chars to the end
        //
        // Calculate the modulo
        unsigned long long  mod;
        if ( ! error_control_mod97_calc( text, mod ) )
            return false;
        //
        // The remainder of the division by 97 must be 1.
        return mod == 1;
    }

}; // namespace IBAN

//-------------------------------------------------------------------------
// RIB
namespace {

    //-------------------------------------------------------------------------
    // Replace letters present in a RIB by digits
    std::string error_control_rib_letter_subst ( std::string  p_rib )
    {
        for (;;)
        {
            auto pos = p_rib.find_first_of( "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            if ( pos == std::string::npos )
                break;
            //
            char letter = p_rib[ pos ],
                 substitute;
            //
                 if ( letter >= 'S' ) substitute = '2' + ( letter - 'S' ); // S-Z -> 2-9
            else if ( letter >= 'J' ) substitute = '1' + ( letter - 'J' ); // J-R -> 1-9
            else                      substitute = '1' + ( letter - 'A' ); // A-I -> 1-9
            //
            p_rib[ pos ] = substitute;
        }
        //
        return p_rib;
    }

    //-------------------------------------------------------------------------
    // Add the RIB signature to the given string
    // Returns false on error
    bool error_control_rib_sign ( std::string &  p_rib )
    {
        if ( p_rib.length() < 1 )
            return false;
        //
        std::string text = error_control_rib_letter_subst( p_rib );
        //
        // Calculate the modulo
        unsigned long long  mod;
        if ( ! error_control_mod97_calc( text + "00", mod ) )
            return false;
        //
        // Add the 98 complement as signature
        p_rib += adawat::str_sprintf( "%02llu", 97 - mod );
        //
        return true;
    }


    //-------------------------------------------------------------------------
    // Return true if the given string has a valid RIB signature
    bool error_control_rib_check ( const std::string &  p_rib )
    {
        if ( p_rib.length() < 3 )
            return false;
        //
        std::string key  = p_rib.substr( p_rib.length() - 2 );
        std::string text = error_control_rib_letter_subst( p_rib.substr( 0, p_rib.length() - 2 ) ); // text without the key
        //
        // Calculate the modulo
        unsigned long long  mod;
        if ( ! error_control_mod97_calc( text + "00", mod ) )
            return false;
        //
        // Checks the signature
        return adawat::str_sprintf( "%02llu", 97 - mod ) == key;
    }

}; // namespace RIB

//-------------------------------------------------------------------------
// Add a signature to the given string
// Returns false on error
bool error_control_sign ( std::string &       p_text,
                          error_control_type  p_type )
{
    switch ( p_type )
    {
    case error_control_type::luhn:   return error_control_luhn_sign ( p_text );
    case error_control_type::mod97:  return error_control_mod97_sign( p_text );
    case error_control_type::damm:   return error_control_damm_sign ( p_text );
    case error_control_type::iban:   return error_control_iban_sign ( p_text );
    case error_control_type::rib:    return error_control_rib_sign  ( p_text );
    default:    assert( false );
                throw std::invalid_argument( "error_control_sign" );
    }
}

//-------------------------------------------------------------------------
// Return true if the given string has a valid signature
bool error_control_check ( const std::string &  p_text,
                           error_control_type   p_type )
{
    switch ( p_type )
    {
    case error_control_type::luhn:   return error_control_luhn_check ( p_text );
    case error_control_type::mod97:  return error_control_mod97_check( p_text );
    case error_control_type::damm:   return error_control_damm_check ( p_text );
    case error_control_type::iban:   return error_control_iban_check ( p_text );
    case error_control_type::rib:    return error_control_rib_check  ( p_text );
    default:    assert( false );
                throw std::invalid_argument( "error_control_check" );
    }
}

//-------------------------------------------------------------------------
// Convert error_control to string
std::string error_control_type_cvt ( error_control_type  p_type )
{
    switch ( p_type )
    {
    case error_control_type::luhn:   return "luhn";
    case error_control_type::mod97:  return "mod97";
    case error_control_type::damm:   return "damm";
    case error_control_type::iban:   return "iban";
    case error_control_type::rib:    return "rib";
    default:    assert( false );
                throw std::invalid_argument( "error_control_type_cvt" );
    }
}

//-------------------------------------------------------------------------
// Convert error_control from string
bool error_control_type_cvt ( const std::string &   p_text,
                              error_control_type &  p_type )
{
    if ( p_text == "luhn"  ) { p_type = error_control_type::luhn;  return true; }
    if ( p_text == "mod97" ) { p_type = error_control_type::mod97; return true; }
    if ( p_text == "damm"  ) { p_type = error_control_type::damm;  return true; }
    if ( p_text == "iban"  ) { p_type = error_control_type::iban;  return true; }
    if ( p_text == "rib"   ) { p_type = error_control_type::rib;   return true; }
    //
    return false;
}

//-------------------------------------------------------------------------
// Returns the signature length of the given type
size_t error_control_signature_length ( error_control_type  p_type )
{
    switch ( p_type )
    {
    case error_control_type::luhn:   return 1;
    case error_control_type::mod97:  return 2;
    case error_control_type::damm:   return 1;
    case error_control_type::iban:   return 2;
    case error_control_type::rib:    return 2;
    default:    assert( false );
                throw std::invalid_argument( "error_control_signature_length" );
    }
}

} // namespace adawat
