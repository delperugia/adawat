// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "adawat_common.h"

namespace adawat {

//-------------------------------------------------------------------------
// Copy a string in Data at construction
Data::Data ( const std::string &  p_string )
{
    assign( p_string.begin(), p_string.end() );
}

//-------------------------------------------------------------------------
// Copy a string in Data
Data & Data::operator= ( const std::string &  p_string )
{
    assign( p_string.begin(), p_string.end() );
    //
    return * this;
}

//-------------------------------------------------------------------------
// Copy a Data in a string
std::string Data::str ( void ) const
{
    return std::string( begin(), end() );
}

} // namespace adawat
