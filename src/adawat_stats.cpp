// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <algorithm>
#include <limits>
#include <numeric>
#include <math.h>

#include "adawat_stats.h"

namespace adawat {

//-------------------------------------------------------------------------
// Update an average counter with a new value
// Not precise, slow to converge but fast and simple average calculation
void stats_update_average ( double &  p_average,
                            double    p_new_value,
                            size_t    p_window_size /* = k_default_window_size */ )
{
    p_average += ( p_new_value - p_average ) / p_window_size;
}

//-------------------------------------------------------------------------
// StatsValue
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
StatsValue::StatsValue ( size_t  p_window_size /* = k_default_window_size */ )
{
    m_window_size = p_window_size;
    //
    clear();
}

//-------------------------------------------------------------------------
// Using a static window array instead of a deque saves 10% CPU time,
// but deque is cleaner and offers dynamic window size.
// Just pushing values in the window and calculating mean and variance
// on demand is only twice faster, and running variance/mean could be
// used to trigger event. Some figures:
// Pushing 10M values: 0.800s (vs 0.350s if only pushing)
void StatsValue::add_value ( double p_value )
{
    m_count++;
    //
    if ( p_value < m_min ) m_min = p_value;
    if ( p_value > m_max ) m_max = p_value;
    //
    double delta    = p_value - m_mean;
    double new_mean = m_mean + delta / m_count;
    //
    m_M2   += delta * ( p_value - new_mean );
    m_mean  = new_mean;
    //
    if ( m_window.size() >= m_window_size )
        m_window.pop_front();
    //
    m_window.push_back( p_value );
}

//-------------------------------------------------------------------------
uint64_t StatsValue::count ( void ) const
{
    return m_count;
}

//-------------------------------------------------------------------------
double StatsValue::max ( void ) const
{
    return m_count == 0 ?  0.0 : m_max;
}

//-------------------------------------------------------------------------
double StatsValue::mean ( void ) const
{
    return m_mean;
}

//-------------------------------------------------------------------------
double StatsValue::min ( void ) const
{
    return m_count == 0 ? 0.0 : m_min;
}

//-------------------------------------------------------------------------
double StatsValue::sd ( void ) const
{
    return sqrt( variance() );
}

//-------------------------------------------------------------------------
double StatsValue::variance ( void ) const
{
    // M2 cannot theoretically be negative. However, because of
    // floating point rounding, it can sometime be just under 0.
    return m_count > 1 && m_M2 > 0 ?
            m_M2 / (m_count - 1) :
            0.0;
}

//-------------------------------------------------------------------------
std::tuple< uint64_t, double, double, double, double, double > // count, max, mean, min, sd, variance
    StatsValue::details ( void ) const
{
    if ( m_count == 0 )
        return { 0, 0, 0, 0, 0, 0 };
    //
    double v = variance();
    //
    return { m_count, m_max, m_mean, m_min, sqrt( v ), v };
}

//------------------------------------------
// Retrieving the mean/min/max values of the window
double StatsValue::window_max ( void ) const
{
    return m_window.empty() ?
            0 :
            (* std::max_element( m_window.begin(), m_window.end() ));
}

double StatsValue::window_mean ( void ) const
{
    return m_window.empty() ?
            0 :
            std::reduce( m_window.begin(), m_window.end() ) / m_window.size();
}

double StatsValue::window_min ( void ) const
{
    return m_window.empty() ?
            0 :
            (* std::min_element( m_window.begin(), m_window.end() ));
}

std::tuple< double, double, double > // max/mean/min
    StatsValue::window_details ( void ) const
{
    if ( m_window.empty() )
        return { 0, 0, 0 };
    //
    double v_max = std::numeric_limits< double >::min(),
           v_min = std::numeric_limits< double >::max(),
           v_sum = 0;
    //
    for ( const auto & value: m_window )
    {
        v_max  = std::max( v_max, value );
        v_min  = std::min( v_min, value );
        v_sum += value;
    }
    //
    return { v_max, v_sum / m_window.size(), v_min };
}

//-------------------------------------------------------------------------
// Set the new window size, existing data are kept
void StatsValue::resize ( size_t  p_new_size )
{
    while ( m_window.size() > p_new_size ) {  // when reducing size, properly remove old values
        double old_value = m_window.front();
        m_window.pop_front();
        //
        double new_mean = m_mean - ( old_value - m_mean ) / m_window.size();  // remove old value
        //
        m_M2   = m_M2 - ( old_value - m_mean ) * ( old_value - new_mean );    // remove old value
        m_mean = new_mean;
    }
    //
    m_window_size = p_new_size;
}

//-------------------------------------------------------------------------
// Clear all statistics (window size remains)
void StatsValue::clear ( void )
{
    m_mean  = 0;
    m_M2    = 0;
    m_min   = std::numeric_limits< double >::max();
    m_max   = std::numeric_limits< double >::min();
    m_count = 0;
    //
    m_window.clear();
}

} // namespace adawat
