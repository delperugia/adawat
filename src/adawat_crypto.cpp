// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <byteswap.h>
#include <functional>
#include <inttypes.h>
#include <mutex>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <random>
#include <string.h>
#include <unistd.h>             // syscall getentropy

#if !__GLIBC_PREREQ(2,25)
    #include <sys/syscall.h>    // SYS_getrandom
#endif

#include "adawat_crypto.h"
#include "adawat_encoding.h"
#include "adawat_string.h"

namespace adawat {

//-------------------------------------------------------------------------
// Wrappers around openSSL functions. EVP function are used because
// they use hardware acceleration (compare to HMAC_ AES_...).
//
// Link with: -lcrypto
// See:
//    https://www.openssl.org/docs/man1.0.2/crypto/EVP_DigestInit_ex.html
//    https://www.openssl.org/docs/man1.0.2/crypto/EVP_aes_128_cbc_hmac_sha1.html

//-------------------------------------------------------------------------
// Returns and empties the error queue

namespace
{
    // Append the next error to the buffer
    int cb_crypto_last_errors ( const char *str, size_t len, void *u )
    {
        std::string * message = reinterpret_cast< std::string * >( u );
        //
        if ( ! message->empty() )
            message->append( ";" );
        //
        message->append( std::string( str, len ) );
        //
        return 0;
    }
}

std::string     crypto_last_errors ( void )
{
    std::string message;
    //
    ERR_print_errors_cb( cb_crypto_last_errors, & message );
    //
    return message;
}

//-------------------------------------------------------------------------
// Given a name, returning a pointer to the corresponding algorithm function
// or nullptr on error
const EVP_MD *      crypto_md_from_name     ( const std::string &  p_name )
{
    return EVP_get_digestbyname( p_name.c_str() );
}

const EVP_CIPHER *  crypto_cipher_from_name ( const std::string &  p_name )
{
    return EVP_get_cipherbyname( p_name.c_str() );
}

//-------------------------------------------------------------------------
// Calculate the hash of a message

template< typename M>
bool crypto_digest ( const std::vector< std::reference_wrapper< const M > > & p_parts,     // data to sign
                     Data &                                                   p_digest,    // resulting digest
                     const EVP_MD *                                           p_algorithm  /* = EVP_sha256() */ )
{
    EVP_MD_CTX * context = EVP_MD_CTX_new();
    if ( context == nullptr )
        return false;
    //
    p_digest.resize( EVP_MAX_MD_SIZE );  // largest possible digest size
    //
    bool     done   = true;
    unsigned md_len = 0; // maximum will be EVP_MAX_MD_SIZE;
    //
    done = done && EVP_DigestInit_ex( context, p_algorithm, nullptr );
    //
    for ( const auto & part: p_parts )
        done = done && EVP_DigestUpdate( context, part.get().data(), part.get().size() ); // cppcheck-suppress useStlAlgorithm,unmatchedSuppression
    //
    done = done && EVP_DigestFinal_ex( context, p_digest.data(), & md_len );
    //
    if ( done )
        p_digest.resize( md_len );  // trim digest to actual size
    //
    EVP_MD_CTX_free( context );
    //
    return done;
}

// A single string
bool crypto_digest( const std::string & p_message,   // data to sign
                    Data &              p_digest,    // resulting digest
                    const EVP_MD *      p_algorithm  /* = EVP_sha256() */)
{
    return crypto_digest< std::string >( { std::cref( p_message ) }, p_digest, p_algorithm );
}

// A single Data
bool crypto_digest ( const Data &   p_message,   // data to sign
                     Data &         p_digest,    // resulting digest
                     const EVP_MD * p_algorithm  /* = EVP_sha256() */ )
{
    return crypto_digest< Data >( { std::cref( p_message ) }, p_digest, p_algorithm );
}

// An array of string cref
bool crypto_digest( const std::vector< std::reference_wrapper< const std::string > > & p_messages,  // data to sign
                    Data &                                                             p_digest,    // resulting digest
                    const EVP_MD *                                                     p_algorithm  /* = EVP_sha256() */)
{
    return crypto_digest< std::string >( p_messages, p_digest, p_algorithm );
}

// An array of Data cref
bool crypto_digest ( const std::vector< std::reference_wrapper< const Data > > & p_messages,  // data to sign
                     Data &                                                      p_digest,    // resulting digest
                     const EVP_MD *                                              p_algorithm  /* = EVP_sha256() */ )
{
    return crypto_digest< Data >( p_messages, p_digest, p_algorithm );
}

//-------------------------------------------------------------------------
// Calculate the keyed hash of the message.

template< typename M>
bool crypto_hmac ( const Data &                                             p_key,       // secret key
                   const std::vector< std::reference_wrapper< const M > > & p_parts,     // data to sign
                   Data &                                                   p_digest,    // resulting digest
                   const EVP_MD *                                           p_algorithm  /* = EVP_sha256() */ )
{
    EVP_MD_CTX * context = EVP_MD_CTX_new();
    if ( context == nullptr )
        return false;
    //
    EVP_PKEY * key  = EVP_PKEY_new_mac_key( EVP_PKEY_HMAC, nullptr,
                                            p_key.data(),
                                            static_cast< int >( p_key.size() ) );
    if ( key == nullptr )
        return false;
    //
    p_digest.resize( EVP_MAX_MD_SIZE ); // largest possible digest size
    //
    bool   done   = true;
    size_t md_len = EVP_MAX_MD_SIZE;    // size of buffer passed to EVP_DigestSignFinal
    //
    done = done && EVP_DigestSignInit( context, nullptr, p_algorithm, nullptr, key );
    //
    for ( const auto &part : p_parts )
        done = done && EVP_DigestSignUpdate( context, part.get().data(), part.get().size() ); // cppcheck-suppress useStlAlgorithm,unmatchedSuppression
    //
    done = done && EVP_DigestSignFinal( context, p_digest.data(), &md_len );
    //
    if ( done )
        p_digest.resize( md_len ); // trim digest to actual size
    //
    EVP_PKEY_free  ( key     );
    EVP_MD_CTX_free( context );
    //
    return done;
}

bool crypto_hmac ( const Data &         p_key,       // secret key
                   const std::string &  p_message,   // data to sign
                   Data &               p_digest,    // resulting digest
                   const EVP_MD *       p_algorithm  /* = EVP_sha256() */ )
{
    return crypto_hmac< std::string >( p_key, { std::cref( p_message) }, p_digest, p_algorithm );
}

bool crypto_hmac ( const Data &    p_key,       // secret key
                   const Data &    p_message,   // data to sign
                   Data &          p_digest,    // resulting digest
                   const EVP_MD *  p_algorithm  /* = EVP_sha256() */ )
{
    return crypto_hmac< Data >( p_key, { std::cref( p_message) }, p_digest, p_algorithm );
}

bool crypto_hmac ( const Data &                                                       p_key,       // secret key
                   const std::vector< std::reference_wrapper< const std::string > > & p_messages,  // data to sign
                   Data &                                                             p_digest,    // resulting digest
                   const EVP_MD *                                                     p_algorithm  /* = EVP_sha256() */ )
{
    return crypto_hmac< std::string >( p_key, p_messages, p_digest, p_algorithm );
}

bool crypto_hmac ( const Data &                                                p_key,       // secret key
                   const std::vector< std::reference_wrapper< const Data > > & p_messages,  // data to sign
                   Data &                                                      p_digest,    // resulting digest
                   const EVP_MD *                                              p_algorithm  /* = EVP_sha256() */ )
{
    return crypto_hmac< Data >( p_key, p_messages, p_digest, p_algorithm );
}

//-------------------------------------------------------------------------
// Derives a hash of a password with a salt, applying the given iteration
// count. Algorithm is one of:
//    EVP_md5, EVP_sha1, EVP_sha256, EVP_sha512
// Output size requested should not be larger than the algorithm used:
//    SHA-1   20 bytes
//    SHA-224 28 bytes
//    SHA-256 32 bytes
//    SHA-384 48 bytes
//    SHA-512 64 bytes
// Takes 188ms with default parameters to complete
bool    crypto_pbkdf2 ( const Data &    p_password,        // password
                        const Data &    p_salt,            // random data       64 bits
                        Data &          p_digest,          // resulting digest
                        unsigned        p_iterations,      // = 100000 number of loops
                        size_t          p_requested_size,  // = 16     data_out size, in bytes
                        const EVP_MD *  p_algorithm        /* = EVP_sha256() */ )
{
    p_digest.resize( p_requested_size );
    //
    return
        PKCS5_PBKDF2_HMAC( reinterpret_cast< const char * >( p_password.data() ),
                           static_cast< int >( p_password.size() ),
                           p_salt.data(),
                           static_cast< int >( p_salt.size() ),
                           static_cast< int >( p_iterations ),
                           p_algorithm,
                           static_cast< int >( p_requested_size ),
                           p_digest.data() ) == 1;
}

//-------------------------------------------------------------------------
// Encrypts/decrypts data.
namespace
{

    // Check the size of the key or IV
    bool check_size_of( const char *p_name, size_t p_size, int p_expected_size )
    {
        if ( static_cast< int >( p_size ) == p_expected_size )
            return true;
        //
        ERR_raise        ( ERR_LIB_NONE, ERR_R_PASSED_INVALID_ARGUMENT );
        ERR_add_error_txt( nullptr,
                           str_sprintf( "crypto_cipher: %s size is %zu, %d expected",
                                        p_name, p_size, p_expected_size ).c_str() );
        return false;
    }
}

bool crypto_cipher ( const Data &        p_key,       // secret key
                     const Data &        p_iv,        // initialization vector
                     const Data &        p_data_in,   // data to encrypt or decrypt
                     Data &              p_data_out,  // encrypted or decrypted data
                     bool                p_encrypt,   // encrypt or decrypt operation
                     const EVP_CIPHER *  p_algorithm  /* = EVP_aes_128_cbc() */,
                     bool                p_padding    /* = true */ )
{
    EVP_CIPHER_CTX * context = EVP_CIPHER_CTX_new();
    if ( context == nullptr )
        return false;
    //
    bool done = false;
    int  data_len, padding_len;
    //
    // Start by doing an init without key/iv to retrieved and check expected length
    if ( EVP_CipherInit_ex( context, p_algorithm, nullptr, nullptr, nullptr, p_encrypt ? 1 : 0 )  &&
         check_size_of( "key", p_key.size(), EVP_CIPHER_CTX_key_length( context ) )      &&
         check_size_of( "IV" , p_iv .size(), EVP_CIPHER_CTX_iv_length ( context ) )  )
    {
        EVP_CIPHER_CTX_set_padding( context, p_padding ? 1 : 0 );  // to be called after each init
        //
        // And now the real init is done here
        if ( EVP_CipherInit_ex( context, p_algorithm, nullptr, p_key.data(), p_iv.data(), p_encrypt ? 1 : 0 ) )
        {
            EVP_CIPHER_CTX_set_padding( context, p_padding ? 1 : 0 );  // to be called after each init
            //
            p_data_out.resize( p_data_in.size() + EVP_MAX_BLOCK_LENGTH );  // encoded size can be larger than message due to padding
            //
            if ( EVP_CipherUpdate( context,
                                   p_data_out.data(),
                                   & data_len,  // will contain the size of data written in p_data_out
                                   p_data_in.data(),
                                   static_cast< int >( p_data_in.size() ) ) )
            {
                if ( EVP_CipherFinal_ex( context,
                                         p_data_out.data() + data_len,  // start at the end of the encoded data
                                         & padding_len ) )              // will contain padding length
                {
                    // Trim any extra data
                    p_data_out.resize( static_cast<size_t>( data_len + padding_len ) );  //-V1028  PVS thinks there might be an overflow
                    done = true;
                }
            }
        }
    }
    //
    EVP_CIPHER_CTX_free( context );
    //
    return done;
}

//-------------------------------------------------------------------------
// Generates random data
// If p_requested_size is greater than 256, data are not CSPRNG.
// If the generation of a CSPRNG fails, and if it is allowed,
// generates a simpler PRNG for which the function cannot fail
bool crypto_random ( size_t  p_requested_size,               // number of byte to generate
                     Data &  p_data_out,                     // resulting data
                     bool    p_must_be_csprng /* = true */ ) // returns false if not
{
    p_data_out.resize( p_requested_size );
    //
    // Fills everything with random data
#if __GLIBC_PREREQ(2,25)
    if ( getentropy( p_data_out.data(), p_data_out.size() ) == 0  )
        return true;
#else
    if ( syscall( SYS_getrandom, p_data_out.data(), p_data_out.size(), 0 ) == static_cast< long >( p_data_out.size() ) )
        return true;
#endif
    //
    if ( p_must_be_csprng ) {
        ERR_raise        ( ERR_LIB_SYS, errno );
        ERR_add_error_txt( nullptr, "crypto_random: csprng failed and was mandatory" );
        return false;
    }
    //
    crypto_random_prng( p_requested_size, p_data_out );
    //
    return true;
}

//-------------------------------------------------------------------------
// Generates random data using a simple, fail safe, PRNG
// We cannot really use std::random_device because if this function is called it will probably fail
// Should not be used
void crypto_random_prng ( size_t  p_requested_size, // number of byte to generate
                          Data &  p_data_out )      // resulting data
{
    p_data_out.resize( p_requested_size );
    //
    static std::mutex    s_lock;
    static bool          s_need_initialization = true;
    static std::mt19937  s_mt;  // use a Mersenne twister generator
    //
    std::lock_guard lock( s_lock );
    //
    // First time use time to seed the generator
    if ( s_need_initialization )
    {
        struct timespec ts;
        //
        timespec_get( &ts, TIME_UTC );
        //
        std::seed_seq seed = { static_cast< unsigned long >( getpid()   ),
                               static_cast< unsigned long >( ts.tv_sec  ),
                               static_cast< unsigned long >( ts.tv_nsec ) };
        //
        s_mt.seed( seed );
        s_need_initialization = false;
    }
    //
    while ( p_requested_size-- )
        p_data_out[ p_requested_size ] = s_mt();
}

//-------------------------------------------------------------------------
// Time-constant comparisons
bool crypto_equal ( const Data &  p_data_1,
                    const Data &  p_data_2 )
{
    if ( p_data_1.size() != p_data_2.size() )
        return false;  // leaks the size
    //
    return CRYPTO_memcmp( p_data_1.data(),
                          p_data_2.data(),
                          p_data_1.size() ) == 0;
}

bool crypto_equal ( std::string_view  p_string_1,
                    std::string_view  p_string_2 )
{
    if ( p_string_1.length() != p_string_2.length() )
        return false;  // leaks the size
    //
    return CRYPTO_memcmp( p_string_1.data(),
                          p_string_2.data(),
                          p_string_1.length() ) == 0;
}

//-------------------------------------------------------------------------
// One-Time Password: HOTP (RFC4226) et TOTP (RFC6238)
// Return true on success

bool crypto_hotp ( const Data &   p_key,
                   uint64_t       p_counter,
                   unsigned       p_digits,     // HOTP size, maximum 9
                   std::string &  p_hotp )
{
    if ( p_digits > 9 ) {                       // limit of our pow10 table
        ERR_raise        ( ERR_LIB_NONE, ERR_R_PASSED_INVALID_ARGUMENT );
        ERR_add_error_txt( nullptr, "crypto_hotp: maximum number of digits is 9" );
        return false;
    }
    //
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    p_counter = bswap_64( p_counter );          // "high-order byte first"
#endif
    //
    adawat::Data       C, HS;
    constexpr unsigned pow10[] =
        { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000 };
    //
    C.resize( sizeof( p_counter ) );            // "8-byte counter"
    memcpy( C.data(), & p_counter, sizeof( p_counter ) );
    //
    if ( ! adawat::crypto_hmac( p_key, C, HS, EVP_sha1() ) )
        return false;
    //
    // Truncation
    size_t   offset = HS[ 19 ] & 0xF;
    //
    uint32_t DT     = static_cast< uint32_t >(
        ( HS[ offset      ] & 0x7F ) << 24   |
        ( HS[ offset + 1u ] & 0xFF ) << 16   |
        ( HS[ offset + 2u ] & 0xFF ) <<  8   |
        ( HS[ offset + 3u ] & 0xFF ) <<  0
    );
    //
    // HOTP Computation
    p_hotp = str_sprintf( "%0*" PRIu32, p_digits, DT % pow10[ p_digits ] );
    return true;
}

// Since the user may enter a code just after it changed, you may want to also check with
// the TOTP valid at that time by allowing a, for example, 5s lag by setting p_t0 to 5
bool crypto_totp ( const Data &   p_key,
                   unsigned       p_digits,             // TOTP size, maximum 9
                   std::string &  p_totp,
                   unsigned       p_step /* = 30 */,
                   time_t         p_t0   /* = 0  */ )
{
    if ( p_step == 0 ) {
        ERR_raise        ( ERR_LIB_NONE, ERR_R_PASSED_INVALID_ARGUMENT );
        ERR_add_error_txt( nullptr, "crypto_totp: minimum number of steps is 1" );
        return false;
    }
    //
    time_t t = time( nullptr );
    if ( t < 0 ) {
        ERR_raise        ( ERR_LIB_SYS, errno );
        ERR_add_error_txt( nullptr, "crypto_totp: failed to get current time" );
        return false;
    }
    //
    t -= p_t0;
    if ( t < 0 ) {
        ERR_raise        ( ERR_LIB_NONE, ERR_R_PASSED_INVALID_ARGUMENT );
        ERR_add_error_txt( nullptr, "crypto_totp: t0 must be in the past" );
        return false;
    }
    //
    uint64_t counter = static_cast< uint64_t >( t / p_step );
    //
    return crypto_hotp( p_key, counter, p_digits, p_totp );
}

//-------------------------------------------------------------------------
// Public-key functions: generation and signature

namespace
{

    //-----------------------------------------
    // Reads a bio to a Data object
    bool bio_to_data ( BIO *          p_bio,
                       adawat::Data & p_binary )
    {
        constexpr size_t chunk      = 128;
        size_t           total_read = 0;
        int              read;
        //
        do
        {   // Read bio using small chunks
            p_binary.resize( total_read + chunk );                         // extend buffer
            read = BIO_read( p_bio, p_binary.data() + total_read, chunk ); // read a the end
            //
            if ( read > 0 )
                total_read += static_cast< unsigned >( read );
            else if ( ! BIO_should_retry( p_bio ) )
                return false;
        } while ( read > 0 );
        //
        p_binary.resize( total_read );
        return true;
    }

    //-----------------------------------------
    // Generate a private and public pair of keys
    // For ED, similar to:
    //   openssl genpkey -algorithm ed25519 -outform PEM -out private.pem
    //   openssl pkey -in private.pem -pubout -out public.pem
    // For EC, similar to:
    //   openssl ecparam -genkey -name secp160r2 -noout -out private.pem
    //   openssl ec -in private.pem -pubout -out public.pem
    bool pkey_generate ( int             p_key,        // one of the EVP_PKEY_...
                         int             p_ec_nid,     // if p_key is EVP_PKEY_EC, one of EC NID_...
                         int             p_rsa_bits,
                         adawat::Data &  p_key_private,
                         adawat::Data &  p_key_public )
    {
        EVP_PKEY * pkey = nullptr;
        bool       done = true;
        //
        {  // Prepare and generate the keys
            EVP_PKEY_CTX * context = EVP_PKEY_CTX_new_id( p_key, nullptr );
            //
            done = done && EVP_PKEY_keygen_init( context ) == 1;
            done = done && ( p_rsa_bits < 0 ||
                             EVP_PKEY_CTX_set_rsa_keygen_bits( context, p_rsa_bits ) == 1 );     // RSA only
            done = done && ( p_key != EVP_PKEY_EC ||
                             EVP_PKEY_CTX_set_ec_paramgen_curve_nid( context, p_ec_nid ) == 1 ); // EC only
            done = done && EVP_PKEY_keygen( context, &pkey ) == 1;
            //
            EVP_PKEY_CTX_free( context );
        }
        //
        {  // Extract public key, convert it to DER
            BIO * bio = BIO_new( BIO_s_mem() );
            //
            done = done && i2d_PUBKEY_bio( bio, pkey ) == 1;
            done = done && bio_to_data( bio, p_key_public );
            //
            BIO_free_all( bio );
        }
        //
        {  // Extract private key, convert it to DER
            BIO * bio = BIO_new( BIO_s_mem() );
            //
            done = done && i2d_PrivateKey_bio( bio, pkey ) == 1;
            done = done && bio_to_data( bio, p_key_private );
            //
            BIO_free_all( bio );
        }
        //
        EVP_PKEY_free( pkey );
        return done;
    }

}

//-----------------------------------------
// Generate a private and public pair of keys for the given algorithm
// Accepts "rsa", "rsa_pss", "ed25519" or "ed448", or one of the curve name returned by:
//      openssl ecparam -list_curves
// For rsa, the number of bits can be specified in p_parameters (default is 2024 since OpenSSL 1.1.0):
//      "bits=1024"
// Return true on success
bool crypto_pkey_generate ( const std::string &  p_algorithm,
                            const std::string &  p_parameters,
                            adawat::Data &       p_key_private,
                            adawat::Data &       p_key_public )
{
    // Retrieve algorithm and parameters
    int nid  = NID_undef;
    int bits = -1;
    //
    if ( p_algorithm == "ed25519" ) {
        nid = EVP_PKEY_ED25519;
    }
    else if ( p_algorithm == "ed448" ) {
        nid = EVP_PKEY_ED448;
    }
    else if ( p_algorithm == "rsa" ) {
        nid = EVP_PKEY_RSA;
        //
        if ( ! p_parameters.empty() )
            if ( sscanf( p_parameters.c_str(), "bits=%d", &bits ) != 1 )
                return false;
    }
    else if ( p_algorithm == "rsa_pss" ) {
        nid = EVP_PKEY_RSA_PSS;
        //
        if ( ! p_parameters.empty() )
            if ( sscanf( p_parameters.c_str(), "bits=%d", &bits ) != 1 )
                return false;
    }
    else {
        nid = OBJ_txt2nid( p_algorithm.c_str() );
    }
    //
    if ( nid == NID_undef )
        return false;
    //
    // Generate keys
    switch ( nid )
    {
        case EVP_PKEY_ED25519:
        case EVP_PKEY_ED448:
            return pkey_generate( nid,         -1 ,   -1, p_key_private, p_key_public );
        case EVP_PKEY_RSA:
        case EVP_PKEY_RSA_PSS:
            return pkey_generate( nid,         -1 , bits, p_key_private, p_key_public );
        default:
            return pkey_generate( EVP_PKEY_EC, nid,   -1, p_key_private, p_key_public );
    }
}

//-----------------------------------------
// Converts the given key, private or public, to PEM
bool    crypto_pkey_to_PEM ( const adawat::Data &  p_key,
                             std::string &         p_PEM )
{
    adawat::Data PEM;
    EVP_PKEY *   pkey       = nullptr;
    bool         is_private = true;
    bool         done       = true;
    //
    {  // Try to decode it as public, then as private
        const uint8_t * key = p_key.data();
        //
        pkey = d2i_PUBKEY( nullptr, &key, static_cast<long>( p_key.size() ) );
        if ( pkey == nullptr )
            pkey = d2i_AutoPrivateKey( nullptr, &key, static_cast<long>( p_key.size() ) );
        else
            is_private = false;
        //
        if ( pkey == nullptr ) // unrecognized key
            return false;
    }
    //
    {  // Export key to PEM
        BIO * bio = BIO_new( BIO_s_mem() );
        //
        if ( is_private )
            done = done && PEM_write_bio_PrivateKey( bio, pkey, nullptr, nullptr, 0, nullptr, nullptr ) == 1;
        else
            done = done && PEM_write_bio_PUBKEY( bio, pkey ) == 1;
        //
        done = done && bio_to_data( bio, PEM );
        BIO_free_all( bio );
    }
    //
    EVP_PKEY_free( pkey );
    p_PEM = PEM.str();
    return done;
}

//-----------------------------------------
// Read the given PEM containing a private or public key
std::tuple< bool, bool >  // success, is_private
    crypto_pkey_from_PEM ( adawat::Data &       p_key,
                           const std::string &  p_PEM )
{
    EVP_PKEY *   pkey       = nullptr;
    bool         is_private = true;
    bool         done       = true;
    //
    {  // Try to read it as public, then as private
        BIO * bio = BIO_new_mem_buf( p_PEM.data(), p_PEM.length() );
        //
        pkey = PEM_read_bio_PUBKEY( bio, nullptr, nullptr, nullptr );
        if ( pkey == nullptr ) {
            BIO_ctrl( bio, BIO_CTRL_RESET, 0, nullptr ); // rewind BIO
            pkey = PEM_read_bio_PrivateKey( bio, nullptr, nullptr, nullptr );
        }
        else
            is_private = false;
        //
        BIO_free_all( bio );
    }
    //
    if ( pkey == nullptr )
        return { false, false };
    //
    if ( is_private )
    {  // Convert private key to DER
        BIO * bio = BIO_new( BIO_s_mem()  );
        done      = done && i2d_PrivateKey_bio( bio, pkey ) == 1;
        done      = done && bio_to_data( bio, p_key );
        BIO_free_all( bio );
    }
    else
    {  // Convert public key to DER
        BIO * bio = BIO_new( BIO_s_mem() );
        done      = done && i2d_PUBKEY_bio( bio, pkey ) == 1;
        done      = done && bio_to_data( bio, p_key );
        BIO_free_all( bio );
    }
    //
    EVP_PKEY_free( pkey );
    return { done, is_private };
}

//-----------------------------------------
// Return the signature of the given message using a private key
// Similar to:
//  openssl dgst -sha256 -sign private.pem < text.txt > signature.bin
// Return true on success
bool crypto_pkey_sign ( const adawat::Data &  p_key_private,
                        const adawat::Data &  p_message,
                        adawat::Data &        p_signature )
{
    EVP_PKEY * pkey = nullptr;
    bool       done = false;
    //
    {  // Decode private key
        const uint8_t * key = p_key_private.data();
        pkey                = d2i_AutoPrivateKey( nullptr, &key, static_cast< long >( p_key_private.size() ) );
    }
    //
    if ( pkey == nullptr )
        return false;
    //
    {  // Sign the data
        EVP_MD_CTX * ctx    = EVP_MD_CTX_new();
        size_t       siglen = static_cast< size_t >( EVP_PKEY_size( pkey ) );
        //
        p_signature.resize( siglen );
        //
        // Default type is EVP_sha256 for EC, SHA512/SHA3 for ED
        done =
            EVP_DigestSignInit( ctx, nullptr, nullptr, nullptr, pkey )                             == 1 &&
            EVP_DigestSign( ctx, p_signature.data(), &siglen, p_message.data(), p_message.size() ) == 1;
        //
        p_signature.resize( done ? siglen : 0 );
        //
        EVP_MD_CTX_free( ctx );
    }
    //
    EVP_PKEY_free( pkey );
    return done;
}

//-----------------------------------------
// Verify the signature of the given message using a public key
// Similar to:
//  openssl dgst -sha256 -verify public.pem -signature signature.bin < text.txt
// Return true on success
bool crypto_pkey_verify ( const adawat::Data & p_key_public,
                          const adawat::Data & p_message,
                          const adawat::Data & p_signature )
{
    EVP_PKEY * pkey     = nullptr;
    bool       verified = false;
    //
    {  // Decode public key
        const uint8_t * key = p_key_public.data();
        pkey                = d2i_PUBKEY( nullptr, &key, static_cast< long >( p_key_public.size() ) );
    }
    //
    if ( pkey == nullptr )
        return false;
    //
    {  // Verify signature
        EVP_MD_CTX * ctx = EVP_MD_CTX_new();
        //
        // Default type is EVP_sha256 for EC, SHA512/SHA3 for ED
        verified =
            EVP_DigestVerifyInit( ctx, nullptr, nullptr, nullptr, pkey )                                        == 1 &&
            EVP_DigestVerify( ctx, p_signature.data(), p_signature.size(), p_message.data(), p_message.size() ) == 1;
        //
        EVP_MD_CTX_free( ctx );
    }
    //
    EVP_PKEY_free( pkey );
    return verified;
}

} // namespace adawat
