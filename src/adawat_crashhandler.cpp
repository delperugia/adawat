// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <cxxabi.h>
#include <dlfcn.h>
#include <execinfo.h>
#include <link.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#define GNU_GPL_COMPATIBLE          // if program does not use GPL license, unset this

#if defined ADAWAT_NOT_GNU_GPL_COMPATIBLE
    #undef GNU_GPL_COMPATIBLE
#endif

#if defined GNU_GPL_COMPATIBLE
    #include <bfd.h>
#endif

namespace adawat {

constexpr
int k_max_stack_frames = 32;

//-------------------------------------------------------------------------
typedef struct {
    FILE *          handle_report;
    void *          searched_address;
    bfd_symbol **   symbol_table;
    bool            found;
} browse_sections_data;

//-------------------------------------------------------------------------
// Nicely log the function name and location
static
void dump_function ( FILE *        p_handle_report,
                     const char *  p_file_name,
                     unsigned int  p_file_line,
                     const char *  p_function_name )
{
    fprintf( p_handle_report, "%s at ", p_function_name );
    //
    if ( p_file_name == nullptr ) {  // unknown file name
        fprintf( p_handle_report, "?" );
    } else {
        const char * name_only = strrchr( p_file_name, '/' );  // remove full path
        //
        if ( name_only == nullptr )
            fprintf( p_handle_report, "%s", p_file_name );
        else
            fprintf( p_handle_report, "%s", name_only + 1 );  // +1 to skip remaining /
    }
    //
    if ( p_file_line != 0 )  // no line number
        fprintf( p_handle_report, ":%u", p_file_line );
    //
    fprintf( p_handle_report, "\n" );
}

#if defined GNU_GPL_COMPATIBLE

//-------------------------------------------------------------------------
// Within one section, search the piece of code matching the address
static
void browse_sections_cb ( bfd * p_handle, asection * p_section, void * p_data )
{
    auto data = reinterpret_cast< browse_sections_data * >( p_data );
    //
    const char * file_name = nullptr, * function_name = nullptr;
    unsigned int file_line;
    //
#ifdef bfd_get_section_flags
    if ( bfd_get_section_flags( p_handle, p_section ) & (SEC_ALLOC|SEC_CODE) )
#else
    if ( bfd_section_flags( p_section ) & (SEC_ALLOC|SEC_CODE) )
#endif
    {
#ifdef bfd_get_section_vma
        char *       begin = reinterpret_cast< char* >( bfd_get_section_vma( p_handle, p_section ) );
        const char * end   = begin + bfd_section_size( p_handle, p_section );
#else
        char *       begin = reinterpret_cast< char* >( bfd_section_vma( p_section ) );
        const char * end   = begin + bfd_section_size( p_section );
#endif
        //
        // If the searched address is in this section
        if ( data->searched_address >= begin && data->searched_address < end )
        {
            // Offset of the searched address in the section
            long offset = reinterpret_cast< char* >( data->searched_address ) -
                          begin;
            //
            if ( bfd_find_nearest_line( p_handle,
                                        p_section,
                                        data->symbol_table,
                                        static_cast< bfd_vma >( offset ),
                                        & file_name, & function_name, & file_line ) )
            {
                data->found = true;
                //
                int          status;
                const char * demangled = abi::__cxa_demangle( function_name, nullptr, nullptr, & status );
                //
                dump_function( data->handle_report,
                               file_name,
                               file_line,
                               ( status == 0 ) ? demangled : function_name );
            }
        }
    }
}

//-------------------------------------------------------------------------
// Within this object, find the address
static
bool find_in_object ( FILE * p_handle_report, const char * p_file_name, void * p_address )
{
    bool            found   = false;
    //
    // Open Binary File Descriptor for this object
    bfd * handle = bfd_openr( p_file_name, nullptr );  // use auto target detection
    if ( handle != nullptr )
    {
        if ( bfd_check_format( handle, bfd_object ) &&      // with auto-target, the target is actually found here
             ( bfd_get_file_flags( handle ) & HAS_SYMS ) )  // this object has debug symbols
        {
            // Read all static symbols (bfd_map_over_sections crashes on dynamic ones)
            unsigned int  symbol_size;
            void *        symbols    = nullptr;
            long          nb_symbols = bfd_read_minisymbols( handle, false, & symbols, & symbol_size );
            //
            if ( nb_symbols > 0 )
            {
                browse_sections_data data;
                    data.handle_report    = p_handle_report;
                    data.searched_address = p_address;
                    data.symbol_table     = reinterpret_cast< bfd_symbol** >( symbols );
                    data.found            = false;
                //
                // Browse all sections, searching the IP
                bfd_map_over_sections( handle, browse_sections_cb, & data );
                //
                found = data.found;
            }
            //
            if ( symbols != nullptr )
                free( symbols );
        }
        //
        bfd_close( handle );
    }
    //
    return found;
}

#else  // not using GPL

static
bool find_in_object ( FILE * p_handle_report, const char * p_file_name, void * p_address )
{
    dump_function( p_handle_report, p_file_name, 0, "?" );
    //
    return true;
}

#endif

//-------------------------------------------------------------------------
typedef struct {
    FILE *  handle_report;
    void *  searched_address;
    bool    found;
} browse_objects_data;

//-------------------------------------------------------------------------
// Browse all loaded ELF shared object, find the one where the address is located
static
int browse_objects_cb ( struct dl_phdr_info * p_info, size_t /* p_size */, void * p_data )
{
    auto data = reinterpret_cast< browse_objects_data *>( p_data );
    //
    // For each item in the object
    for (int x = 0; x < p_info->dlpi_phnum; x++)
    {
        // Calculate the begin and end of code for this item
        char *       begin = reinterpret_cast< char* >( p_info->dlpi_addr + p_info->dlpi_phdr[x].p_vaddr );
        const char * end   = begin + p_info->dlpi_phdr[x].p_memsz;
        //
        // If the searched address is in this item
        if ( data->searched_address >= begin && data->searched_address < end )
        {
            //
            // Relative address of the looked up address in the object
            void * in_object_address = reinterpret_cast< char* >( data->searched_address ) - 
                                       p_info->dlpi_addr;
            //
            // Now search function in item,
            if ( p_info->dlpi_name[ 0 ] == 0 )  // empty means current file
                data->found |= find_in_object( data->handle_report, "/proc/self/exe",  in_object_address );
            else
                data->found |= find_in_object( data->handle_report, p_info->dlpi_name, in_object_address );
        }
    }
    //
    return data->found ? 1 : 0;  // stop browsing if found
}

//-------------------------------------------------------------------------
// For each IP, find the loaded object where this IP is located
static
void backtrace_dump ( FILE * p_handle_report, void * p_addresses[], int p_nb_addresses )
{
    bfd_init();
    //
    // Skip the two first addresses: they are the system signal handler and our handler
    for ( int n = 2; n < p_nb_addresses; n++ )
    {
        fprintf( p_handle_report, "#%-2d %p in ", n, p_addresses[ n ] );
        //
        browse_objects_data data;
            data.handle_report      = p_handle_report;
            data.searched_address   = p_addresses[ n ];
            data.found              = false;
        //
        dl_iterate_phdr( browse_objects_cb, & data );  // browse loaded objects
        //
        if ( ! data.found )
            fprintf( p_handle_report, "?\n" );
    }
}

//-------------------------------------------------------------------------
// Catch a signal, create a report file with details on the crash
static
void handler ( int p_sig, const siginfo_t * p_info, void * /* p_context */ )
{
    const char *  signal_name;
    FILE *        handle_report;
    //
    //-------
    switch ( p_sig ) {
        case SIGSEGV:   signal_name = "SIGSEGV";  break;
        case SIGFPE:    signal_name = "SIGFPE";   break;
        case SIGILL:    signal_name = "SIGILL";   break;
        case SIGBUS:    signal_name = "SIGBUS";   break;
        case SIGPOLL:   signal_name = "SIGPOLL";  break;
        case SIGXCPU:   signal_name = "SIGXCPU";  break;
        case SIGXFSZ:   signal_name = "SIGXFSZ";  break;
        case SIGPIPE:   signal_name = "SIGPIPE";  break;
        case SIGABRT:   signal_name = "SIGABRT";  break;
        case SIGTERM:   signal_name = "SIGTERM";  break;
        default:        signal_name = "unknown";  break;
    }
    //
    //-------
    // This part of code in bracket to release stack asap
    {
        char report_name[ 24 ];
        snprintf( report_name, sizeof( report_name ), "/tmp/watson.%ld", static_cast<long>( getpid() ) );
        //
        handle_report = fopen( report_name, "wt" );
        if ( handle_report == nullptr )
            _exit(EXIT_FAILURE);
    }
    //
    //-------
    // Use procfs to get current process binary file
    char    exe_name[ 256 ];  // PATH_MAX is too big, 256 should be enough (note: lstat fails on /proc/)
    ssize_t ret = readlink( "/proc/self/exe", exe_name, sizeof( exe_name ) - 1 );
    if ( ret == -1 )
        strcpy( exe_name, "unknown" );
    else
        exe_name[ ret ] = 0;  // readlink doesn't put one
    //
    //-------
    // Write report header
    {
        time_t            raw_time;
        const struct tm * time_info;
        char              when[ 32 ];
        //
        time ( & raw_time );
        time_info = localtime ( & raw_time );
        strftime ( when, sizeof( when ), "%FT%TL", time_info );  // 2001-08-23T14:55:02L
        //
        fprintf( handle_report,
                 "\n"
                 "date:   %s\n"
                 "thread: 0x%ld\n"
                 "signal: %s\n"
                 "exe:    %s\n"
                 "info:   si_addr=%p si_errno=%d si_fd=%x\n",
                 when,
                 static_cast<long>( syscall(SYS_gettid) ),
                 signal_name,
                 exe_name,
                 p_info->si_addr, p_info->si_errno, p_info->si_fd
        );
    }
    //
    //-------
    // Now dump stack
    {
        void *   stack_frames[ k_max_stack_frames ];
        int      trace_size;
        //
        // Retrieve the IP list on the stack
        trace_size = backtrace( stack_frames, k_max_stack_frames );
        //
        fprintf( handle_report, "stack (%d):\n", trace_size );
        //
        // Dump and decode them
        backtrace_dump( handle_report, stack_frames, trace_size );
    }
    //
    //-------
    fprintf( handle_report, "\n" );
    fclose(  handle_report );
    //
    _exit(EXIT_FAILURE);
}

//-------------------------------------------------------------------------
// Setup the crash handler. Report files are stored in /tmp/ folder.
// Report names are like:
//      watson.PID
bool crashhandler_setup( void )
{
    //-------
    // Allocate the stack used by the signal handler in another space: mmap
    // Enclosed the new stack in two write protected pages: if somebody
    // corrupted then, signal will be trigger before the stack is reached
    // Start by allocating a continuous readable / writable area
    size_t page_size  = static_cast< size_t >( sysconf(_SC_PAGESIZE) );
    size_t stack_size = static_cast< size_t >( SIGSTKSZ              );
    char * new_stack  = static_cast< char * > (
        mmap( nullptr,
              page_size + stack_size + page_size,
              PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON | MAP_STACK,
              -1,
              0 )
    );
    if ( new_stack == MAP_FAILED )
        return false;
    //
    // Then protected the two pages
    mprotect( new_stack,                          page_size, PROT_NONE );
    mprotect( new_stack + page_size + stack_size, page_size, PROT_NONE );
    //
    //-------
    // Setup new stack
    stack_t sigstk = { };
        sigstk.ss_sp    = new_stack + page_size;
        sigstk.ss_size  = stack_size;
        sigstk.ss_flags = 0;
    //
    if ( sigaltstack( & sigstk, nullptr ) != 0 )
        return false;
    //
    //-------
    // Change signal handlers
    struct sigaction sig_action = { };
        sig_action.sa_sigaction = reinterpret_cast< void (*)(int, siginfo_t*, void*) >( handler );
        sig_action.sa_flags     = SA_SIGINFO | SA_ONSTACK;
        sigemptyset( & sig_action.sa_mask );
    //
    if ( sigaction( SIGABRT, & sig_action, nullptr ) != 0  ||
         sigaction( SIGBUS,  & sig_action, nullptr ) != 0  ||
         sigaction( SIGFPE,  & sig_action, nullptr ) != 0  ||
         sigaction( SIGILL,  & sig_action, nullptr ) != 0  ||
         sigaction( SIGPIPE, & sig_action, nullptr ) != 0  ||
         sigaction( SIGPOLL, & sig_action, nullptr ) != 0  ||
         sigaction( SIGSEGV, & sig_action, nullptr ) != 0  ||
         sigaction( SIGTERM, & sig_action, nullptr ) != 0  ||
         sigaction( SIGXCPU, & sig_action, nullptr ) != 0  ||
         sigaction( SIGXFSZ, & sig_action, nullptr ) != 0  )
    {
        return false;
    }
    //
    return true;
}

} // namespace adawat
