// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <cstdint>
#include <cstdio>
#include <string>

namespace adawat {

//-------------------------------------------------------------------------
// URL encoding
std::string url_encode ( std::string_view  p_decoded )
{
    std::string  encoded;
    char         s[ 4 ];    // placeholder for % and 2 hexadecimal digits in a c-string
    //
    encoded.reserve( p_decoded.length() + 16 );  // arbitrary extra space
    //
    // Fill the static char in the string
    s[ 0 ] = '%'; // encoded characters are prefixed by %
    s[ 3 ] = 0;   // c-string terminator
    //
    for ( const auto & c: p_decoded )
        if ( isalnum( c ) || c == '-' || c == '_' || c == '.' || c == '~' ) {  // unreserved characters
            encoded += c;
        } else {
            s[ 1 ] = "0123456789ABCDEF"[ ( c >> 4 ) & 0xF ];
            s[ 2 ] = "0123456789ABCDEF"[ ( c      ) & 0xF ];
            encoded += s;
        }
    //
    return encoded;
}

//-------------------------------------------------------------------------
// URL decoding
bool url_decode ( std::string_view  p_encoded,
                  std::string &     p_decoded  )
{
    // 0xFF is a marker for invalid characters
    //
    static const uint8_t base16set[] =
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x0.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x1.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x2.
        "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x3.  0 -> 9
        "\xFF\x0A\x0B\x0C\x0D\x0E\x0F\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x4.  A -> F
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x5.
        "\xFF\x0A\x0B\x0C\x0D\x0E\x0F\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x6.  a -> f
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x7.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF";
    //
    p_decoded.clear();
    p_decoded.reserve( p_encoded.length() ); // at least
    //
    for ( auto i = p_encoded.begin(); i != p_encoded.end(); i++ ) {
        if ( *i == '%' ) {
            // Read the two characters following %
            if ( ++i == p_encoded.end() ) return false;
            uint8_t high_nibble = base16set [ static_cast< size_t >( * i ) ];
            if ( ++i == p_encoded.end() ) return false;
            uint8_t low_nibble  = base16set [ static_cast< size_t >( * i ) ];
            //
            // Check their validity
            if ( (high_nibble | low_nibble) == 0xFF )  // if one of them is the invalid char marker
                return false;
            //
            // Add the corresponding character to the string
            p_decoded += static_cast< char >( high_nibble << 4 | low_nibble );
        } else {
            p_decoded += *i;
        }
    }
    //
    return true;
}

} // namespace adawat
