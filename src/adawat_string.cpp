// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <algorithm>
#include <cstdarg>

#include "adawat_string.h"

//-------------------------------------------------------------------------
//  - Most of these functions act on the parameter (instead of returning a
//    new object value) because it is faster:
//          - call by const value and return    4.715s   (const std::string & p_text,...
//          - call by value and return          4.665s   (std::string p_text,...
//          - call by reference                 1.530s   (std::string & p_text,...
//  - std::string::find of std::string is faster than const char *

namespace adawat {

//-------------------------------------------------------------------------
// Make a string upper / lower case
// Return reference on the string
std::string & str_lower ( std::string &  p_text )
{
    std::transform( p_text.begin(), p_text.end(), p_text.begin(), ::tolower );
    //
    return p_text;
}

std::string & str_upper ( std::string &  p_text )
{
    std::transform( p_text.begin(), p_text.end(), p_text.begin(), ::toupper );
    //
    return p_text;
}

//-------------------------------------------------------------------------
// Strip whitespace (or other characters) from the beginning and/or end of a string
// Return reference on the string
std::string & str_ltrim ( std::string &     p_text,
                          std::string_view  p_white_chars /* = k_whitespaces */ )
{
    size_t begin = p_text.find_first_not_of( p_white_chars );
    //
    if ( begin == std::string::npos ) // p_text contains nothing or only spaces
        p_text.clear();
    else
        p_text.erase( 0, begin );
    //
    return p_text;
}

std::string & str_rtrim ( std::string &     p_text,
                          std::string_view  p_white_chars /* = k_whitespaces */ )
{
    size_t last = p_text.find_last_not_of( p_white_chars );
    //
    if ( last == std::string::npos ) // p_text contains nothing or only spaces
        p_text.clear();
    else
        p_text.erase( last + 1 );
    //
    return p_text;
}

std::string & str_trim ( std::string &     p_text,
                         std::string_view  p_white_chars /* = k_whitespaces */ )
{
    str_ltrim( p_text, p_white_chars );
    str_rtrim( p_text, p_white_chars );
    //
    return p_text;
}

//-------------------------------------------------------------------------
// Replace all occurrences of the search string with the replacement string
// Return reference on the string
std::string &  str_replace ( std::string &        p_text,
                             std::string_view     p_search,
                             std::string_view     p_replace_by,
                             size_t               p_count /* = std::numeric_limits< size_t >::max() */ )
{
    if ( p_text.empty() || p_search.empty() )
        return p_text;
    //
    size_t position = 0;
    //
    while ( p_count-- &&
            ( position = p_text.find( p_search, position )) != std::string::npos )
    {
        p_text.replace( position, p_search.length(), p_replace_by );
        position += p_replace_by.length();
    }
    //
    return p_text;
}

//-------------------------------------------------------------------------
// Split elements of a string separated by delimiter

std::vector< std::string > str_split ( const std::string &  p_text,
                                       std::string_view     p_separator,
                                       size_t               p_count /* = std::numeric_limits< size_t >::max() */ )
{
    std::vector< std::string > result;
    size_t                     begin = 0;
    //
    if ( p_text.empty() )
        return result;
    //
    for ( ; p_count--; )
    {
        size_t position = p_text.find( p_separator, begin );  // getline only accepts a single char separator
        //
        if ( position == std::string::npos )  // no more occurrence
            break;                            // last element will be pushed in result after the loop
        //
        result.emplace_back( p_text.substr( begin, position-begin ) );
        //
        begin = position + p_separator.length();
    }
    //
    result.emplace_back( p_text.substr( begin ) );  // push the rest of the string as the last element
    //
    return result;
}

std::vector< std::string_view> str_split_view ( std::string_view     p_text,
                                                std::string_view     p_separator,
                                                size_t               p_count /* = std::numeric_limits< size_t >::max() */ )
{
    std::vector< std::string_view > result;
    size_t                          begin = 0;
    //
    if ( p_text.empty() )
        return result;
    //
    for ( ; p_count--; )
    {
        size_t position = p_text.find( p_separator, begin );  // getline only accepts a single char separator
        //
        if ( position == std::string::npos )  // no more occurrence
            break;                            // last element will be pushed in result after the loop
        //
        result.emplace_back( p_text.substr( begin, position-begin ) );
        //
        begin = position + p_separator.length();
    }
    //
    result.emplace_back( p_text.substr( begin ) );  // push the rest of the string as the last element
    //
    return result;
}

//-------------------------------------------------------------------------
// Extract and return the first word of a string
std::string str_token ( std::string &     p_text,
                        std::string_view  p_white_chars /* = k_whitespaces */ )
{
    if ( p_text.empty() )  // empty string
        return p_text;
    //
    size_t begin = p_text.find_first_not_of( p_white_chars );
    if ( begin == std::string::npos )  // string full of spaces
        return p_text.erase();
    //
    size_t end   = p_text.find_first_of    ( p_white_chars, begin );
    size_t next  = p_text.find_first_not_of( p_white_chars, end   );
    //
    auto   token = p_text.substr ( begin,
                                   end==std::string::npos ?
                                       end :           // no space after the 1st token
                                       end - begin );  // some spaces after 1st token
    p_text.erase( 0, next );
    //
    return token;
}

std::string_view  str_token_view ( std::string_view &  p_text,
                                   std::string_view    p_white_chars /* = k_whitespaces */ )
{
    if ( p_text.empty() )  // empty string
        return p_text;
    //
    size_t begin = p_text.find_first_not_of( p_white_chars );
    if ( begin == std::string::npos )  // string full of spaces
    {
        p_text.remove_prefix( p_text.length() );
        return p_text;
    }
    //
    size_t end   = p_text.find_first_of    ( p_white_chars, begin );
    size_t next  = p_text.find_first_not_of( p_white_chars, end );
    //
    auto   token = p_text.substr ( begin,
                                   end==std::string::npos ?
                                       end :           // no space after the 1st token
                                       end - begin );  // some spaces after 1st token
    p_text.remove_prefix( next==std::string::npos ?
                            p_text.length() :   // nothing left after this token: clear all
                            next );             // remove up to the next token
    //
    return token;
}

//-------------------------------------------------------------------------
// Returns true if a key is in a CSV string
bool str_csv_has_value ( std::string_view  p_key,
                         std::string_view  p_csv,
                         char              p_separator /* = ',' */ )
{
    size_t offset = 0;
    //
    for (;;) {
        size_t pos = p_csv.find( p_key, offset );
        //
        if ( pos == std::string::npos )
            return false;
        //
        if ( pos == 0 || p_csv.at( pos - 1 ) == p_separator )       // good on left side
            if ( ( pos + p_key.length() ) == p_csv.length()         // key at the end
                 ||
                 p_csv.at( pos + p_key.length() ) == p_separator )  // key followed by separator
                return true;
        //
        offset++;
    }
}

//-------------------------------------------------------------------------
// Return a formatted string
// Done in a single step for small strings (<1K), in two steps with
// dynamic allocation in a vector for bigger strings
std::string str_sprintf ( const char *  p_format,
                          ... )
{
    char small_buffer [1024];  // assume most calls will need less than this
    //
    va_list ap1;
    va_start( ap1, p_format );
    //
    va_list ap2;
    va_copy( ap2, ap1 );  // make a copy of the va_list because the 1st call to vsnprintf may change it
    //
    //-------
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
    int result = vsnprintf( small_buffer, sizeof( small_buffer ), p_format, ap1 );
#pragma GCC diagnostic pop
    //
    if ( result < 0 ) { // an error occurred in vsnprintf
        va_end( ap1 );
        va_end( ap2 );
        //
        return "";
    }
    //
    // Number of characters that would have been written if n had been
    // sufficiently large, not counting the terminating null character
    size_t needed = static_cast<size_t>( result );
    //
    //-------
    if ( needed >= sizeof(small_buffer) )  // if it was not enough, allocate a larger buffer
    {
        va_end( ap1 );
        //
        std::vector<char> dynamic_buffer ( needed + 1 );  // +1 for the terminating null-byte
        //
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
        vsnprintf( dynamic_buffer.data(), dynamic_buffer.size(), p_format, ap2 );
#pragma GCC diagnostic pop
        //
        va_end( ap2 );
        //
        return std::string( dynamic_buffer.begin(), std::prev( dynamic_buffer.end() ) );  // std::prev to skip the null-byte
    }
    else
    {
        va_end( ap1 );
        va_end( ap2 );
        //
        return std::string( small_buffer, needed );
    }
}

//-------------------------------------------------------------------------
// Prefix and suffix comparison
// Return true is found
//
bool str_starts_with ( std::string_view  p_text,
                       std::string_view  p_prefix )
{
    return p_text.compare( 0, p_prefix.length(), p_prefix ) == 0;
}
//
bool str_ends_with ( std::string_view  p_text,
                     std::string_view  p_suffix )
{
    if ( p_text.length() < p_suffix.length() )
        return false;
    //
    return p_text.compare( p_text.length() - p_suffix.length(),
                           p_suffix.length(),
                           p_suffix ) == 0;
}

} // namespace adawat
