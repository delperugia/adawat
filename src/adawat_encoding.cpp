// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <cassert>
#include <stdexcept>

#include "adawat_encoding.h"

namespace adawat {

//-------------------------------------------------------------------------
// Encodes and returns the data using base 16 (see RFC 4648)
// Timing:  0.810s @1M
std::string encoding_base16_encode ( const Data &  p_data_in,
                                     bool          p_lower_case /* = false */ )
{
    static const char base16set_upper[] =
        "000102030405060708090A0B0C0D0E0F"
        "101112131415161718191A1B1C1D1E1F"
        "202122232425262728292A2B2C2D2E2F"
        "303132333435363738393A3B3C3D3E3F"
        "404142434445464748494A4B4C4D4E4F"
        "505152535455565758595A5B5C5D5E5F"
        "606162636465666768696A6B6C6D6E6F"
        "707172737475767778797A7B7C7D7E7F"
        "808182838485868788898A8B8C8D8E8F"
        "909192939495969798999A9B9C9D9E9F"
        "A0A1A2A3A4A5A6A7A8A9AAABACADAEAF"
        "B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF"
        "C0C1C2C3C4C5C6C7C8C9CACBCCCDCECF"
        "D0D1D2D3D4D5D6D7D8D9DADBDCDDDEDF"
        "E0E1E2E3E4E5E6E7E8E9EAEBECEDEEEF"
        "F0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF";
    //
    static const char base16set_lower[] =
        "000102030405060708090a0b0c0d0e0f"
        "101112131415161718191a1b1c1d1e1f"
        "202122232425262728292a2b2c2d2e2f"
        "303132333435363738393a3b3c3d3e3f"
        "404142434445464748494a4b4c4d4e4f"
        "505152535455565758595a5b5c5d5e5f"
        "606162636465666768696a6b6c6d6e6f"
        "707172737475767778797a7b7c7d7e7f"
        "808182838485868788898a8b8c8d8e8f"
        "909192939495969798999a9b9c9d9e9f"
        "a0a1a2a3a4a5a6a7a8a9aaabacadaeaf"
        "b0b1b2b3b4b5b6b7b8b9babbbcbdbebf"
        "c0c1c2c3c4c5c6c7c8c9cacbcccdcecf"
        "d0d1d2d3d4d5d6d7d8d9dadbdcdddedf"
        "e0e1e2e3e4e5e6e7e8e9eaebecedeeef"
        "f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff";
    //
    std::string  result;
    const char * set_to_use = p_lower_case ? base16set_lower : base16set_upper;
    //
    result.reserve( p_data_in.size() * 2 );
    //
    for ( auto x : p_data_in)
        result.append( & set_to_use[ x * 2], 2 );
    //
    return result;
}

//-------------------------------------------------------------------------
// Encodes and returns the data using base 32 (see RFC 4648)
// Timing:  0.900s @1M
std::string encoding_base32_encode ( const Data &  p_data_in,
                                     bool          p_base32hex  /* = false */,
                                     bool          p_padding    /* = true  */ )
{
    // See encoding_base64_encode
    static const char base32set[] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567=";
    //
    static const char base32hexset[] =
        "0123456789ABCDEFGHIJKLMNOPQRSTUV"
        "0123456789ABCDEFGHIJKLMNOPQRSTUV"
        "0123456789ABCDEFGHIJKLMNOPQRSTUV"
        "0123456789ABCDEFGHIJKLMNOPQRSTUV"
        "0123456789ABCDEFGHIJKLMNOPQRSTUV"
        "0123456789ABCDEFGHIJKLMNOPQRSTUV"
        "0123456789ABCDEFGHIJKLMNOPQRSTUV"
        "0123456789ABCDEFGHIJKLMNOPQRSTUV=";
    //
    const char * set = p_base32hex ? base32hexset : base32set;
    //
    std::string      result;
    const uint8_t *  ra  = p_data_in.data();                // reading pointer
    size_t           len = p_data_in.size();
    uint8_t          last_group [ 5 ] = { 0, 0, 0, 0, 0 };  // rather than extending p_data_in to have the right size, use a different buffer
    size_t           to_add = 8;                            // until last group, add 4 char
    //
    while ( len > 0 )
    {
        if ( len < 5 ) { // if the last block is not complete, switch to the another buffer
            last_group [ 0 ] =             *ra++;      // there is at least one byte
            last_group [ 1 ] = (len > 1) ? *ra++ : 0;  // possibly two
            last_group [ 2 ] = (len > 2) ? *ra++ : 0;  // possibly three
            last_group [ 3 ] = (len > 3) ? *ra++ : 0;  // possibly four
            ra     = last_group;
            to_add = len * 8 / 5 + 1;                  // only write meaning full bytes
        }
        //
        char ar[8] =
            { set[                         ra[0] >> 3                 ],
              set[ static_cast< uint8_t >( ra[0] << 2 ) | ra[1] >> 6  ],
              set[                       ( ra[1] >> 1 ) /*& 0x1F*/    ],
              set[ static_cast< uint8_t >( ra[1] << 4 ) | ra[2] >> 4  ],
              set[ static_cast< uint8_t >( ra[2] << 1 ) | ra[3] >> 7  ],
              set[                       ( ra[3] >> 2 ) /*& 0x1F*/    ],
              set[ static_cast< uint8_t >( ra[3] << 3 ) | ra[4] >> 5  ],
              set[                         ra[4]                      ] };
        //
        result.append( ar, to_add );
        //
        if ( len <= 5 )  // should be <, but exit also on last block (no reason to inc/dec below)
            break;
        //
        len -= 5;
        ra  += 5;
    }
    //
    // Add padding
    if ( p_padding )
        result.append( 8 - to_add, '=' );
    //
    return result;
}

//-------------------------------------------------------------------------
// Encodes and returns the data using base 64, base 64 url (see RFC 4648)
// or base 64 Xxencoding
// Timing:  1.462s @1M
// See https://en.wikipedia.org/wiki/Base64#C_encoder_efficiency_considerations
std::string encoding_base64_encode ( const Data &  p_data_in,
                                     base64_type   p_type       /* = base64_type::standard */,
                                     bool          p_padding    /* = true  */ )
{
    static const char base64set[] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    //
    static const char base64urlset[] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_=";
    //
    static const char xxencodeset[] =
        "+-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        "+-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        "+-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        "+-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz=";
    //
    const char * set;
    //
    switch ( p_type )
    {
        case base64_type::standard:   set = base64set;    break;
        case base64_type::url:        set = base64urlset; break;
        case base64_type::xxencoding: set = xxencodeset;  break;
        default:    assert( false );
                    throw std::invalid_argument( "encoding_base64_encode" );
    }
    //
    std::string      result;
    const uint8_t *  ra  = p_data_in.data();          // reading pointer
    size_t           len = p_data_in.size();
    uint8_t          last_group [ 3 ] = { 0, 0, 0 };  // rather than extending p_data_in to have the right size, use a different buffer
    size_t           to_add = 4;                      // until last group, add 4 char
    //
    while ( len > 0 )
    {
        if ( len < 3 ) {  // if the last block is not complete, switch to another buffer: last_group
                           last_group [ 0 ] = *ra++;  // there is at least one byte
            if ( len > 1 ) last_group [ 1 ] = *ra++;  // possibly two
            ra     = last_group;
            to_add = len * 4 / 3 + 1;                 // only write meaningful bytes
        }
        //
        char ar[ 4 ] =
            { set[                         ra[0] >> 2                               ] ,
              set[ static_cast< uint8_t >( ra[0] << 4 )|  ra[1] >> 4                ] ,
              set[ static_cast< uint8_t >               ( ra[1] << 2 ) | ra[2] >> 6 ] ,
              set[                                                       ra[2]      ] };
        //
        result.append( ar, to_add );
        //
        if ( len <= 3 )  // should be <, but exit also on last block (no reason to inc/dec below)
            break;
        //
        len -= 3;
        ra  += 3;
    }
    //
    // Add padding
    if ( p_padding )
        result.append( (3 - len) % 3, '=' );  // hence: len=1 -> '==', len=2 -> '=', len=0 -> ''
    //
    return result;
}

//-------------------------------------------------------------------------
// Encodes and returns the data using base 16 (see RFC 4648)
bool encoding_base16_decode ( std::string_view  p_text_in,
                              Data &            p_data_out )
{
    // 0xFF is a marker for invalid characters
    //
    static const uint8_t base16set[] =
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x0.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x1.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x2.
        "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x3.  0 -> 9
        "\xFF\x0A\x0B\x0C\x0D\x0E\x0F\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x4.  A -> F
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x5.
        "\xFF\x0A\x0B\x0C\x0D\x0E\x0F\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x6.  a -> f
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x7.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF";
    //
    size_t        len = p_text_in.size();
    const char *  ra  = p_text_in.data();
    //
    if ( len & 1 ) // input must have an even length
        return false;
    //
    p_data_out.resize( len / 2 );
    //
    for (size_t r = 0, w = 0; r < len; r += 2, w++) {
        uint8_t high_nibble = base16set [ static_cast< size_t >( ra[r]     ) ];  // cast because signed values in ra cannot be used as index
        uint8_t low_nibble  = base16set [ static_cast< size_t >( ra[r + 1] ) ];
        //
        if ( (high_nibble | low_nibble) == 0xFF )  // if one of them is the invalid char marker
            return false;
        //
        p_data_out[ w ] = high_nibble << 4 | low_nibble;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Encodes and returns the data using base 32 or base 32 Extended Hex Alphabet (see RFC 4648)
bool encoding_base32_decode ( std::string_view  p_text_in,
                              Data &            p_data_out,
                              bool              p_base32hex  /* = false */ )
{
    // 0xFF is a marker for invalid characters
    //
    static const uint8_t base32set[] =
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x0.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x1.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x2.
        "\xFF\xFF\x1A\x1B\x1C\x1D\x1E\x1F\xFF\xFF\xFF\xFF\xFF\x00\xFF\xFF"   // 0x3.  2 -> 7 =
        "\xFF\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E"   // 0x4.  A -> O
        "\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\xFF\xFF\xFF\xFF\xFF"   // 0x5.  P -> Z
        "\xFF\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E"   // 0x6.  a -> o
        "\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\xFF\xFF\xFF\xFF\xFF"   // 0x7.  p -> z
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF";
    //
    static const uint8_t base32sethex[] =
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x0.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x1.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x2.
        "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\xFF\xFF\xFF\x00\xFF\xFF"   // 0x3.  0 -> 9 =
        "\xFF\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18"   // 0x4.  A -> O
        "\x19\x1A\x1B\x1C\x1D\x1E\x1F\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x5.  P -> V
        "\xFF\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18"   // 0x6.  a -> o
        "\x19\x1A\x1B\x1C\x1D\x1E\x1F\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x7.  p -> v
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF";
    //
    const uint8_t * set = p_base32hex  ? base32sethex : base32set;
    //
    size_t        len = p_text_in.size();
    const char *  ra  = p_text_in.data();
    char          last_group [ 8 ] = { '=','=','=','=','=','=','=','=' };  // rather than extending p_text_in to have the right size, use a different buffer (full of =)
    size_t        to_add           = 5;                                    // until last group, add 5 bytes
    //
    p_data_out.clear  ();
    p_data_out.reserve( 5 + len * 5 / 8 );
    //
    // Ignore padding char
    while ( len > 0 && ra[ len - 1 ] == '=' ) len--;
    //
    while ( len > 0 )
    {
        if ( len < 8 ) {  // if the last block is not complete, switch to another buffer: last_group
            if ( len==1 || len==3 || len==6 )  // 2/4/7 chars are needed to encode 1/2/4 bytes: data is corrupted
                return false;
            //
                           last_group[ 0 ] = *ra++;
                           last_group[ 1 ] = *ra++;
            if ( len > 2 ) last_group[ 2 ] = *ra++;  // 2 perhaps 3 chars are present
            if ( len > 3 ) last_group[ 3 ] = *ra++;  // 2 perhaps 3 chars are present
            if ( len > 4 ) last_group[ 4 ] = *ra++;  // 2 perhaps 3 chars are present
            if ( len > 5 ) last_group[ 5 ] = *ra++;  // 2 perhaps 3 chars are present
            if ( len > 6 ) last_group[ 6 ] = *ra++;  // 2 perhaps 3 chars are present
            ra     = last_group;
            to_add = len * 5 / 8;                    // only write meaningful bytes
        }
        //
        uint8_t b1 = set [ static_cast< size_t >( *ra++ ) ];  // cast because signed values in ra cannot be used as index
        uint8_t b2 = set [ static_cast< size_t >( *ra++ ) ];
        uint8_t b3 = set [ static_cast< size_t >( *ra++ ) ];
        uint8_t b4 = set [ static_cast< size_t >( *ra++ ) ];
        uint8_t b5 = set [ static_cast< size_t >( *ra++ ) ];
        uint8_t b6 = set [ static_cast< size_t >( *ra++ ) ];
        uint8_t b7 = set [ static_cast< size_t >( *ra++ ) ];
        uint8_t b8 = set [ static_cast< size_t >( *ra++ ) ];
        //
        if ( (b1 | b2 | b3 | b4 | b5 | b6 | b7 | b8) == 0xFF )  // if one of them is the invalid char marker
            return false;
        //
                          p_data_out.push_back( (b1 << 3) | (b2 >> 2 & 0b0111)                        );
        if ( to_add > 1 ) p_data_out.push_back( (b2 << 6) | (b3 << 1           ) | (b4 >> 4 & 0b0001) );
        if ( to_add > 2 ) p_data_out.push_back( (b4 << 4) | (b5 >> 1 & 0b1111  )                      );
        if ( to_add > 3 ) p_data_out.push_back( (b5 << 7) | (b6 << 2           ) | (b7 >> 3 & 0b0011) );
        if ( to_add > 4 ) p_data_out.push_back( (b7 << 5) | (b8                )                      );
        //
        if ( len <= 8 ) // should be < but no reason to do the dec below
            return true;
        //
        len -= 8;
    }
    //
    return true;
}

//-------------------------------------------------------------------------
// Encodes and returns the data using base 64 or base 64 url (see RFC 4648)
// See: https://en.wikipedia.org/wiki/Base64#C_encoder_efficiency_considerations
bool encoding_base64_decode ( std::string_view  p_text_in,
                              Data &            p_data_out,
                              base64_type       p_type  /* = base64_type::standard */ )
{
    // 0xFF is a marker for invalid characters
    //
    static const uint8_t base64set[] =
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x0.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x1.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x3E\xFF\xFF\xFF\x3F"   // 0x2.  + /
        "\x34\x35\x36\x37\x38\x39\x3A\x3B\x3C\x3D\xFF\xFF\xFF\x00\xFF\xFF"   // 0x3.  0 -> 9 =
        "\xFF\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E"   // 0x4.  A -> O
        "\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\xFF\xFF\xFF\xFF\xFF"   // 0x5.  P -> Z
        "\xFF\x1A\x1B\x1C\x1D\x1E\x1F\x20\x21\x22\x23\x24\x25\x26\x27\x28"   // 0x6.  a -> o
        "\x29\x2A\x2B\x2C\x2D\x2E\x2F\x30\x31\x32\x33\xFF\xFF\xFF\xFF\xFF"   // 0x7.  p -> z
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF";
    //
    static const uint8_t base64urlset[] =
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x0.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x1.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x3E\xFF\xFF"   // 0x2.  -
        "\x34\x35\x36\x37\x38\x39\x3A\x3B\x3C\x3D\xFF\xFF\xFF\x00\xFF\xFF"   // 0x3.  0 -> 9 =
        "\xFF\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E"   // 0x4.  A -> O
        "\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\xFF\xFF\xFF\xFF\x3F"   // 0x5.  P -> Z _
        "\xFF\x1A\x1B\x1C\x1D\x1E\x1F\x20\x21\x22\x23\x24\x25\x26\x27\x28"   // 0x6.  a -> o
        "\x29\x2A\x2B\x2C\x2D\x2E\x2F\x30\x31\x32\x33\xFF\xFF\xFF\xFF\xFF"   // 0x7.  p -> z
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF";
    //
    static const uint8_t xxencodeset[] =
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x0.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"   // 0x1.
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x00\xFF\x01\xFF\xFF"   // 0x2.  + -
        "\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\xFF\xFF\xFF\x00\xFF\xFF"   // 0x3.  0 -> 9 =
        "\xFF\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A"   // 0x4.  A -> O
        "\x1B\x1C\x1D\x1E\x1F\x20\x21\x22\x23\x24\x25\xFF\xFF\xFF\xFF\xFF"   // 0x5.  P -> Z
        "\xFF\x26\x27\x28\x29\x2A\x2B\x2C\x2D\x2E\x2F\x30\x31\x32\x33\x34"   // 0x6.  a -> o
        "\x35\x36\x37\x38\x39\x3A\x3B\x3C\x3D\x3E\x3F\xFF\xFF\xFF\xFF\xFF"   // 0x7.  p -> z
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF";
    //
    const uint8_t * set;
    //
    switch ( p_type )
    {
        case base64_type::standard:   set = base64set;    break;
        case base64_type::url:        set = base64urlset; break;
        case base64_type::xxencoding: set = xxencodeset;  break;
        default:    assert( false );
                    throw std::invalid_argument( "encoding_base64_decode" );
    }
    //
    size_t        len = p_text_in.size();
    const char *  ra  = p_text_in.data();
    char          last_group [4] = { '=','=','=','=' };  // rather than extending p_text_in to have the right size, use a different buffer (full of =)
    size_t        to_add = 3;                            // until last group, add 3 bytes
    //
    p_data_out.clear  ();
    p_data_out.reserve( 3 + len * 6 / 8 );
    //
    // Ignore padding char
    while ( len > 0 && ra[ len - 1 ] == '=' )
        len--;
    //
    while ( len > 0 )
    {
        if ( len < 4 ) {  // if the last block is not complete, switch to another buffer: last_group
            if ( len == 1 )  // two chars are needed to encode one byte: data is corrupted
                return false;
            //
                           last_group[ 0 ] = *ra++;
                           last_group[ 1 ] = *ra++;
            if ( len > 2 ) last_group[ 2 ] = *ra++;  // 2 perhaps 3 chars are present
            ra     = last_group;
            to_add = len * 6 / 8;                    // only write meaningful bytes
        }
        //
        uint8_t b1 = set [ static_cast< size_t >( *ra++ ) ];  // cast because signed values in ra cannot be used as index
        uint8_t b2 = set [ static_cast< size_t >( *ra++ ) ];
        uint8_t b3 = set [ static_cast< size_t >( *ra++ ) ];
        uint8_t b4 = set [ static_cast< size_t >( *ra++ ) ];
        //
        if ( (b1 | b2 | b3 | b4) == 0xFF )  // if one of them is the invalid char marker
            return false;
        //
                          p_data_out.push_back( (b1 << 2) | (b2 >> 4 & 0b0011) );
        if ( to_add > 1 ) p_data_out.push_back( (b2 << 4) | (b3 >> 2 & 0b1111) );
        if ( to_add > 2 ) p_data_out.push_back( (b3 << 6) | (b4              ) );
        //
        if ( len <= 4 ) // should be < but no reason to do the decrement below
            return true;
        //
        len -= 4;
    }
    //
    return true;
}

} // namespace adawat
