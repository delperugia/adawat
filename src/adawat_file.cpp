// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <cstring>
#include <fcntl.h>
#include <fstream>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

#include "adawat_file.h"

namespace adawat {

namespace {

    // Reserves space in the container based on the stream (file) size
    template < class myType >
    bool reserve_by_stream_size( myType &  p_container, std::ifstream &  p_file )
    {
        // Get the file size
        p_file.seekg( 0, std::ios::end );
        auto file_size = p_file.tellg();
        p_file.seekg( 0, std::ios::beg );
        //
        if ( file_size < 0 )
            return false;
        //
        // Reserve capacity
        p_container.clear();
        p_container.reserve( static_cast< size_t >( file_size ) );
        //
        return true;
    }

    //-------------------------------------------------------------------------
    // Create a directory and returns true if this directory exist
    bool mkdir_exit ( const std::string &  p_path,
                      mode_t               p_directory_mode )
    {
        return mkdir( p_path.c_str(),
                      p_directory_mode ) == 0   // successful...
               ||
               errno == EEXIST;                 // ... or already exist
    }

    //-------------------------------------------------------------------------
    // Recursively create a path, starting from the given position
    bool create_path ( const std::string &  p_path,
                       size_t               p_from,
                       mode_t               p_directory_mode )
    {
        size_t pos = p_path.rfind( '/', p_from );
        //
        if ( pos == std::string::npos || pos == 0 )  // no more previous folder
            return false;
        //
        // Try to create this level of the path
        if ( mkdir_exit( p_path.substr( 0, pos ), p_directory_mode ) )
            return true;
        //
        if ( errno == ENOENT )  // previous level doesn't exist
            return create_path( p_path, pos - 1, p_directory_mode )  // create previous...
                   &&
                   mkdir_exit( p_path.substr( 0, pos ).c_str(),      // ... and current
                               p_directory_mode );
        else
            return false;  // other error
    }

    //-------------------------------------------------------------------------
    // Create a file, creating also the path if requested
    int create_with_path ( const std::string &  p_file_name,
                           bool                 p_create_folder,
                           mode_t               p_file_mode,
                           mode_t               p_directory_mode )
    {
        constexpr int flags = O_CREAT | O_WRONLY | O_TRUNC;
        //
        int hFile = open( p_file_name.c_str(), flags, p_file_mode );
        //
        if ( hFile == -1 && errno == ENOENT )               // folder doesn't exist
            if ( p_create_folder )                          // and requested
                if ( create_path( p_file_name,              // try to create the path
                                  p_file_name.length() -1,
                                  p_directory_mode ) )
                    hFile = open( p_file_name.c_str(),
                                  flags,
                                  p_file_mode );            // then retry to create file
        //
        return hFile;
    }

};

//-------------------------------------------------------------------------
// Append the string into a text file, returns true on success
// Creates the file if needed
bool file_append ( const std::string &  p_file_name, std::string_view  p_content )
{
    std::ofstream file( p_file_name, std::ios_base::app );
    //
    if ( file.is_open() )
    {
        file << p_content;
        //
        return file.good();
    }
    //
    return false;
}

//-------------------------------------------------------------------------
// Read the content of a text file, returns true on success
bool file_read ( const std::string &  p_file_name, std::string &  p_content )
{
    std::ifstream file( p_file_name, std::ios::in );
    //
    if ( file.is_open() && file.good() && reserve_by_stream_size( p_content, file ) )
    {
        p_content.insert( p_content.begin(), std::istreambuf_iterator< char >( file ), {} );
        //
        return true;
    }
    //
    return false;
}

//-------------------------------------------------------------------------
// Write the string into a text file, returns true on success
bool file_write ( const std::string &  p_file_name,
                  std::string_view     p_content,
                  bool                 p_create_folder  /* = false */,
                  mode_t               p_file_mode      /* = 0640  */,
                  mode_t               p_directory_mode /* = 0750  */ )
{
    bool successful = false;
    int  handle     = create_with_path( p_file_name,
                                        p_create_folder,
                                        p_file_mode,
                                        p_directory_mode );
    //
    if ( handle > 0 )
    {
        successful = write( handle, p_content.data(), p_content.length() )
                     ==
                     static_cast< ssize_t >( p_content.length() );
        //
        close( handle );
    }
    //
    return successful;
}

//-------------------------------------------------------------------------
// Append Data into a binary file, returns true on success
// Creates the file if needed
bool file_append ( const std::string &  p_file_name, const Data &  p_content )
{
    bool    successful = false;
    FILE *  handle     = fopen( p_file_name.c_str(), "at" );
    //
    if ( handle != nullptr ) {
        size_t written = fwrite( p_content.data(), 1, p_content.size(), handle );
        //
        successful = written == p_content.size() && ! ferror( handle );
        //
        fclose( handle );
    }
    //
    return successful;
}

//-------------------------------------------------------------------------
// Read the content of a binary file, returns true on success
bool file_read ( const std::string &  p_file_name, Data &  p_content )
{
    std::ifstream file( p_file_name, std::ios::in | std::ios::binary );
    //
    if ( file.is_open() && file.good() && reserve_by_stream_size( p_content, file ) )
    {
        p_content.insert( p_content.begin(), std::istreambuf_iterator< char >( file ), {} );
        //
        return true;
    }
    //
    return false;
}

//-------------------------------------------------------------------------
// Write Data into a binary file, returns true on success
bool file_write ( const std::string &  p_file_name,
                  const Data &         p_content,
                  bool                 p_create_folder  /* = false */,
                  mode_t               p_file_mode      /* = 0640  */,
                  mode_t               p_directory_mode /* = 0750  */ )
{
    bool successful = false;
    int  handle     = create_with_path( p_file_name,
                                        p_create_folder,
                                        p_file_mode,
                                        p_directory_mode );
    //
    if ( handle > 0 )
    {
        successful = write( handle, p_content.data(), p_content.size() )
                     ==
                     static_cast< ssize_t >( p_content.size() );
        //
        close( handle );
    }
    //
    return successful;
}

} // namespace adawat
