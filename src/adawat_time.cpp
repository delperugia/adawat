// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <climits>
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <regex>
#include <sstream>
#include <string.h>
#include <time.h>

#include "adawat_time.h"

namespace adawat {

//-------------------------------------------------------------------------
// Returns the number of seconds since midnight (00:00:00)
// to now or to the given absolute time, or -1 on error
int time_seconds_since_midnight ( std::time_t p_time /* = 0 */ )
{
    std::time_t reference = ( p_time == 0 ) ? time( nullptr ) : p_time;
    std::tm     local;
    //
    if ( localtime_r( &reference, &local ) == nullptr )
        return -1;
    //
    return local.tm_hour * 3600 +
           local.tm_min  *   60 +
           local.tm_sec;
}

//-------------------------------------------------------------------------
// Returns the absolute time of the last midnight (00:00:00)
// for now or for the given absolute time, or -1 on error
time_t time_last_midnight ( std::time_t p_time /* = 0 */ )
{
    std::time_t reference = ( p_time == 0 ) ? time( nullptr ) : p_time;
    std::tm     local;
    //
    if ( localtime_r( &reference, &local ) == nullptr )
        return -1;
    //
    local.tm_hour = 0;  // at midnight
    local.tm_min  = 0;
    local.tm_sec  = 0;
    //
    return mktime( &local );  // returns -1 on error
}

//-------------------------------------------------------------------------
// Returns the number of seconds to the next midnight (00:00:00)
// from now or from the given absolute time, or -1 on error
// It is not possible to simply use 86400 - seconds_since_midnight
// because of leap seconds
int time_seconds_to_midnight ( std::time_t p_time /* = 0 */ )
{
    std::time_t reference = ( p_time == 0 ) ? time( nullptr ) : p_time;
    std::tm     local;
    //
    if ( localtime_r( &reference, &local ) == nullptr )
        return -1;
    //
    local.tm_mday ++;   // move to next day
    local.tm_hour = 0;  // at midnight
    local.tm_min  = 0;
    local.tm_sec  = 0;
    //
    time_t then = mktime( &local );  // returns -1 on error
    if ( then == -1 )
        return -1;
    //
    return then - reference;
}

//-------------------------------------------------------------------------
// Returns the absolute time of the next midnight (00:00:00)
// from now or from the given absolute time, or -1 on error
time_t time_next_midnight ( std::time_t p_time /* = 0 */ )
{
    std::time_t reference = ( p_time == 0 ) ? time( nullptr ) : p_time;
    std::tm     local;
    //
    if ( localtime_r( &reference, &local ) == nullptr )
        return -1;
    //
    local.tm_mday ++;   // move to next day
    local.tm_hour = 0;  // at midnight
    local.tm_min  = 0;
    local.tm_sec  = 0;
    //
    return mktime( &local );  // returns -1 on error
}

//-------------------------------------------------------------------------
// Returns the RFC 3339 UTC string of now or the given absolute time,
// or empty string on error (e.g.: 1985-04-12T23:20:50Z)
std::string time_format_rfc3339_utc ( std::time_t p_time /* = 0 */ )
{
    std::time_t  reference = (p_time == 0 ) ? time( nullptr ) : p_time;
    std::tm      utc;
    char         text[ 64 ];
    //
    if ( gmtime_r( &reference, &utc ) == nullptr )
        return "";
    //
    strftime( text, sizeof( text ), "%Y-%m-%dT%H:%M:%SZ", &utc );
    //
    return text;
}

//-------------------------------------------------------------------------
// Returns the RFC 3339 local string of now or the given absolute time,
// or empty string on error (e.g.: 1996-12-19T16:39:57-08:00)
std::string time_format_rfc3339_local ( std::time_t p_time /* = 0 */ )
{
    std::time_t  reference = ( p_time == 0 ) ? time( nullptr ) : p_time;
    std::tm      local, utc;
    char         text[ 32 + 8 ];  // 32 for date and time, 8 for the timezone offset
    //
    // First the date and time
    if ( localtime_r( &reference, &local ) == nullptr )
        return "";
    //
    size_t length_text = strftime( text, 32, "%Y-%m-%dT%H:%M:%S", &local );
    if ( length_text == 0 )  // overflow
        return "";
    //
    // Then the timezone offset
    if ( gmtime_r( &reference, &utc ) == nullptr )
        return "";
    //
    // 4.1 Numeric offsets are calculated as "local time minus UTC"
    // Appended at the end of the date and time
    int length_timezone = snprintf( text + length_text, 8, "%+03d:%02d",
                                    (local.tm_hour - utc.tm_hour + 24) % 24,  // hours
                                    (local.tm_min  - utc.tm_min  + 60) % 60   // minutes
                                  );
    if ( length_timezone <= 0 || length_timezone >= 8 )  // error or overflow
        return "";
    //
    return text;
}

//-------------------------------------------------------------------------
// Returns the given RFC 3339 string has an absolute time,
// or -1 on error
std::time_t time_parse_rfc3339 ( const std::string & p_time )
{
    std::tm  utc;
    int      nb_fields, offset_hours, offset_minutes;
    char     separator, offset_sign;
    int      offset;  // in minutes
    //
    switch ( p_time.length() ) {  // only these two formats are recognized
        case 20: // 2018-09-29T17:31:00Z
            nb_fields = sscanf( p_time.c_str(), "%d-%d-%d%c%d:%d:%d%c",
                                & utc.tm_year, & utc.tm_mon, & utc.tm_mday,
                                & separator,
                                & utc.tm_hour, & utc.tm_min, & utc.tm_sec,
                                & offset_sign
                              );
            //
            if ( nb_fields != 8 )
                return -1;
            if ( separator != 'T' && separator != 't' && separator != ' ' )  // 5.6 lower or upper, app...may choose...a space character
                return -1;
            if ( offset_sign != 'Z' && offset_sign != 'z' )  // 5.6 lower or upper
                return -1;
            //
            offset = 0;
            break;
        case 25: // 2018-09-29T19:42:17+02:00
            nb_fields = sscanf( p_time.c_str(), "%d-%d-%d%c%d:%d:%d%c%d:%d",
                                & utc.tm_year, & utc.tm_mon,   & utc.tm_mday,
                                & separator,
                                & utc.tm_hour, & utc.tm_min,   & utc.tm_sec,
                                & offset_sign, & offset_hours, & offset_minutes
                              );
            //
            if ( nb_fields != 10 )
                return -1;
            if ( separator != 'T' && separator != 't' && separator != ' ' )  // 5.6 lower or upper, app...may choose...a space character
                return -1;
            if ( offset_sign != '+' && offset_sign != '-' )
                return -1;
            if ( offset_hours < 0 || offset_hours > 23 )
                return -1;
            if ( offset_minutes < 0 || offset_minutes > 59 )
                return -1;
            //
            offset  = offset_hours * 60 + offset_minutes;
            offset *= offset_sign=='+' ? 1 : -1;
            break;
        default:
            return -1;
    }
    //
    utc.tm_isdst  = 0;      // no DST with UTC
    utc.tm_year  -= 1900;   // years since 1900
    utc.tm_mon   -= 1;      // months since January – [0, 11]
    //
    time_t result = timegm( &utc );
    if ( result == -1 )
        return -1;
    //
    return result - offset * 60;
}

//-------------------------------------------------------------------------
// Returns the number of seconds corresponding to the given duration
//  e.g.: 35s = 35, 1d = 86400, 2d4h = 187200
// or -1 on error. Ignore white spaces, accepted units are:
//      s second    d day   y year
//      m minute    w week
//      h hour      o month
// Month 30.436875 days, Year is 365.2425 days
long long int time_parse_duration ( const std::string & p_duration )
{
    long long int duration = 0, i;
    //
    size_t begin = p_duration.find_first_not_of( " " );
    //
    while ( begin != std::string::npos ) {
        size_t end = p_duration.find_first_not_of( "0123456789", begin );
        if ( end == std::string::npos )
            return -1;  // unit missing
        //
        if ( sscanf( p_duration.substr( begin, end-begin ).c_str(),
                     "%lld",
                     &i ) != 1 )
            return -1;  // conversion failed
        //
        switch ( p_duration[end] ) {
            case 'y': i *= 31556952; break;  // 365.2425   * 86400
            case 'o': i *=  2629746; break;  //  30.436875 * 86400
            case 'w': i *=   604800; break;  //   7        * 86400
            case 'd': i *=    86400; break;
            case 'h': i *=     3600; break;
            case 'm': i *=       60; break;
            case 's':                break;
            default:  return -1;  // unknown unit
        }
        //
        duration += i;
        begin     = p_duration.find_first_not_of( " ", end + 1 );
    }
    //
    return duration;
}

//-------------------------------------------------------------------------
// Parse an ISO 8601 duration where format is "P(n)Y(n)M(n)DT(n)H(n)M(n)S"
// When using date greater_than_time elements (YMD), function tries to take
// into account the DST. By default duration are calculated starting now
// but another time reference can be passed.
long long int time_parse_iso8601_duration( const std::string & p_duration,
                                           std::time_t         p_time /* = 0 */ )
{
    std::regex  pattern( R"(P(?:(\d+(\.\d+)?)Y)?(?:(\d+(\.\d+)?)M)?(?:(\d+(\.\d+)?)W)?(?:(\d+(\.\d+)?)D)?)"
                         R"(T?(?:(\d+(\.\d+)?)H)?(?:(\d+(\.\d+)?)M)?(?:(\d+(\.\d+)?)S)?)" );
    std::smatch matches;
    //
    if ( ! regex_match( p_duration, matches, pattern ) )
        return -1;
    //
    // Add given duration to the reference time to calculate the duration
    std::time_t reference = ( p_time == 0 ) ? time( nullptr ) : p_time;
    std::tm     local;
    //
    if ( localtime_r( &reference, &local ) == nullptr )
        return -1;
    //
    double years   = matches[  1 ].matched ? stod( matches[  1 ].str() ) : 0;
    double months  = matches[  3 ].matched ? stod( matches[  3 ].str() ) : 0;
    double weeks   = matches[  5 ].matched ? stod( matches[  5 ].str() ) : 0;
    double days    = matches[  7 ].matched ? stod( matches[  7 ].str() ) : 0;
    double hours   = matches[  9 ].matched ? stod( matches[  9 ].str() ) : 0;
    double minutes = matches[ 11 ].matched ? stod( matches[ 11 ].str() ) : 0;
    double seconds = matches[ 13 ].matched ? stod( matches[ 13 ].str() ) : 0;
    //
    // Handle the decimal parts
    months  += 12        * modf( years  , & years    ); // a year is 12 months
    days    +=  7        * weeks;                       // a week is 7 days
    days    += 30.436875 * modf( months , & months   ); // this one is an approximation
    hours   += 24        * modf( days   , & days     ); // a day is 24 hours
    minutes += 60        * modf( hours  , & hours    ); // an hour is 60 minutes
    seconds += 60        * modf( minutes, & minutes  ); // an minute is 60 seconds
    //
    // Add to current time
    local.tm_year += years;
    local.tm_mon  += months;
    local.tm_mday += days;
    local.tm_hour += hours;
    local.tm_min  += minutes;
    local.tm_sec  += seconds;
    //
    // If one of the day parts is set, adjust DST according to the given date
    if ( matches[ 1 ].matched || matches[ 3 ].matched || matches[ 7 ].matched )
        local.tm_isdst = -1;
    //
    time_t then = mktime( &local );  // returns -1 on error
    if ( then == -1 )
        return -1;
    //
    return then - reference;
}

}  // namespace adawat
