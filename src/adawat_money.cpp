// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <cassert>
#include <cmath>
#include <inttypes.h>
#include <numeric>
#include <stdexcept>
#include <vector>

#include "adawat_money.h"

namespace adawat {

//-------------------------------------------------------------------------
//  Currency
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
// Currencies are represented internally using a uint32_t. This function
// converts from the 3 letters code to this representation.
//  USD -> 0x555344  EUR -> 0x455552  JPY -> 0x4A5059  GBP -> 0x47425000
//  AUD -> 0x415544  CAD -> 0x434144  CHF -> 0x434846  CNY -> 0x434E5900
constexpr
uint32_t Currency::encode ( char p_a, char p_b, char p_c )
{
    return static_cast< uint32_t >( p_a ) << 16 |
           static_cast< uint32_t >( p_b ) <<  8 |
           static_cast< uint32_t >( p_c );
}

//-------------------------------------------------------------------------
// Expects a 3 letters currency code (ISO 4217)
bool Currency::set ( const std::string &  p_code )
{
    if ( p_code.length() != 3 )
        return false;
    //
    if ( ! std::isalnum( p_code[ 0 ] ) ||
         ! std::isalnum( p_code[ 1 ] ) ||
         ! std::isalnum( p_code[ 2 ] ) )
        return false;
    //
    m_currency = encode( p_code[ 0 ], p_code[ 1 ], p_code[ 2 ] );
    //
    return true;
}

//-------------------------------------------------------------------------
// Add or replace a new currency definition for all instances present or future of Currency
// Alter the static m_currencies member
bool Currency::global_define_currency ( const std::string &  p_code,
                                        const std::string &  p_symbol,
                                        const std::string &  p_symbol_local,
                                        unsigned             p_number,
                                        unsigned             p_precision,
                                        unsigned             p_subunit )
{
    Currency new_code;
    //
    if ( ! new_code.set( p_code ) )
        return false;
    //
    CurrencyDefinition new_definition = {
        p_symbol,
        p_symbol_local,
        p_number,
        p_precision,
        p_subunit
    };
    //
    m_currencies[ new_code.m_currency ] = new_definition;
    //
    return true;
}

//-------------------------------------------------------------------------
// Returns true if the Currency has not yet been set
bool Currency::is_set ( void ) const
{
    return m_currency != 0;
}

//-------------------------------------------------------------------------
// Returns the 3 letters alphabetic code of a currency
std::string Currency::get_code ( void ) const
{
    std::string text;
    //
    text.resize( 3 );
    text[ 0 ] = static_cast<char>( ( m_currency >> 16 ) & 0xFF );
    text[ 1 ] = static_cast<char>( ( m_currency >>  8 ) & 0xFF );
    text[ 2 ] = static_cast<char>( ( m_currency       ) & 0xFF );
    //
    return text;
}

// https://en.wikipedia.org/wiki/ISO_4217#Currency_numbers
// https://en.wikipedia.org/wiki/Euro#Currency_sign
// Characters in symbol are limited to the ISO-8859-15 charset

//-------------------------------------------------------------------------
Currency::CurrencyDefinitions Currency::m_currencies = {
    //                         symbol local   number precision subunit
    { encode( 'A','F','N' ), { "Af"   ,"؋"   , 971,   4,        2 } },
    { encode( 'A','R','S' ), { "$"    ,"$"   ,  32,   3,        2 } },
    { encode( 'A','U','D' ), { "$"    ,"$"   ,  36,   5,        2 } },
    { encode( 'B','R','L' ), { "R$"   ,"R$"  , 986,   5,        2 } },
    { encode( 'C','A','D' ), { "$"    ,"$"   , 124,   6,        2 } },
    { encode( 'C','D','F' ), { "FC"   ,"FC"  , 976,   2,        2 } },
    { encode( 'C','H','F' ), { "Fr"   ,"Fr"  , 756,   6,        2 } },
    { encode( 'C','N','Y' ), { "¥"    ,"¥"   , 156,   5,        2 } },
    { encode( 'C','O','P' ), { "$"    ,"$"   , 170,   2,        2 } },
    { encode( 'D','K','K' ), { "kr"   ,"kr"  , 208,   5,        2 } },
    { encode( 'D','Z','D' ), { "DA"   ,"دج"  ,  12,   3,        2 } },
    { encode( 'E','G','P' ), { "£E"   ,"£E"  , 818,   4,        2 } },
    { encode( 'E','T','B' ), { "Br"   ,"ብር"  , 230,   4,        2 } },
    { encode( 'E','U','R' ), { "€"    ,"€"   , 978,   6,        2 } },
    { encode( 'G','B','P' ), { "£"    ,"£"   , 826,   6,        2 } },
    { encode( 'H','K','D' ), { "$"    ,"元"   , 344,   5,        2 } },
    { encode( 'I','N','R' ), { "Rs"   ,"₹"   , 356,   4,        2 } },
    { encode( 'J','O','D' ), { "JD"   ,"د.أ" , 400,   6,        3 } },
    { encode( 'J','P','Y' ), { "¥"    ,"¥"   , 392,   3,        0 } },
    { encode( 'K','R','W' ), { "W"    ,"₩"   , 410,   1,        0 } },
    { encode( 'L','Y','D' ), { "LD"   ,"ل.د" , 434,   5,        3 } },
    { encode( 'M','A','D' ), { "DH"   ,"د.م.", 504,   5,        2 } },
    { encode( 'M','G','A' ), { "Ar"   ,"Ar"  , 969,   1,        2 } },  // 1/5
    { encode( 'M','R','U' ), { "UM"   ,"أ.م.", 929,   4,        2 } },  // 1/5
    { encode( 'M','W','K' ), { "K"    ,"K"   , 454,   5,        2 } },
    { encode( 'M','X','N' ), { "$"    ,"$"   , 484,   4,        2 } },
    { encode( 'N','G','N' ), { "N"    ,"₦"   , 566,   3,        2 } },
    { encode( 'N','Z','D' ), { "$"    ,"$"   , 554,   5,        2 } },
    { encode( 'P','K','R' ), { "Rs"   ,"₨"  , 586,   3,        0 } },
    { encode( 'P','L','N' ), { "zl"   ,"zł"  , 985,   5,        2 } },
    { encode( 'R','U','B' ), { "P"    ,"₽"   , 643,   4,        2 } },
    { encode( 'R','W','F' ), { "RF"   ,"RF"  , 646,   2,        0 } },
    { encode( 'S','E','K' ), { "kr"   ,"kr"  , 752,   5,        2 } },
    { encode( 'S','G','D' ), { "$"    ,"$"   , 702,   5,        2 } },
    { encode( 'S','Y','P' ), { "£S"   ,"£S"  , 760,   3,        2 } },
    { encode( 'T','H','B' ), { "Bt"   ,"฿"   , 764,   4,        2 } },
    { encode( 'T','N','D' ), { "DT"   ,"DT"  , 788,   5,        3 } },
    { encode( 'T','R','Y' ), { "TL"   ,"₺"   , 949,   5,        2 } },
    { encode( 'T','W','D' ), { "NT$"  ,"圓"   , 901,   4,        2 } },
    { encode( 'T','Z','S' ), { "TSh"  ,"TSh" , 834,   2,        2 } },
    { encode( 'U','G','X' ), { "USh"  ,"USh" , 800,   2,        0 } },
    { encode( 'U','S','D' ), { "$"    ,"$"   , 840,   6,        2 } },
    { encode( 'V','N','D' ), { "D"    ,"₫"   , 704,   1,        0 } },
    { encode( 'X','A','F' ), { "FCFA" ,"FCFA", 950,   3,        0 } },
    { encode( 'X','O','F' ), { "FCFA" ,"FCFA", 952,   3,        0 } },
    { encode( 'X','X','X' ), { "x"    ,"¤"   , 999,   6,        2 } },
    { encode( 'Z','A','R' ), { "R"    ,"R"   , 710,   4,        2 } },
    { encode( 'Z','M','W' ), { "K"    ,"K"   , 967,   4,        2 } },
};

//-------------------------------------------------------------------------
// Returns the numeric code of a currency (default: 999)
//  EUR 0x455552 -> €
unsigned Currency::get_numeric_code ( void ) const
{
    auto x = m_currencies.find( m_currency );
    //
    if ( x == m_currencies.end() )
        return 999;
    else
        return x->second.number;
}

//-------------------------------------------------------------------------
// Returns the national symbol of a currency (default: o)
//  EUR 0x455552 -> €, THB 0x544842 -> Bt
std::string Currency::get_symbol ( void ) const
{
    auto x = m_currencies.find( m_currency );
    //
    if ( x == m_currencies.end() )
        return "o";
    else
        return x->second.symbol;
}

//-------------------------------------------------------------------------
// Returns the local national symbol of a currency (default: ¤)
//  EUR 0x455552 -> €; THB 0x544842 -> ฿
std::string Currency::get_symbol_local ( void ) const
{
    auto x = m_currencies.find( m_currency );
    //
    if ( x == m_currencies.end() )
        return "¤";
    else
        return x->second.symbol_local;
}

//-------------------------------------------------------------------------
// Returns the number of digits to use to represent an amount. This is
// no the official currency sub-division but much smaller in order to
// represent small fees or commissions (default: 6)
//  EUR 0x455552 -> 6
unsigned Currency::get_precision ( void ) const
{
    auto x = m_currencies.find( m_currency );
    //
    if ( x == m_currencies.end() )
        return 6;
    else
        return x->second.precision;
}

//-------------------------------------------------------------------------
// Returns the number of digits to use to represent an amount.
// This is the common currency sub-division (default: 2)
//  EUR 0x455552 -> 2
unsigned Currency::get_subunit ( void ) const
{
    auto x = m_currencies.find( m_currency );
    //
    if ( x == m_currencies.end() )
        return 2;
    else
        return x->second.subunit;
}

//-------------------------------------------------------------------------
bool Currency::operator == ( Currency const &  p_b ) const
{
    return m_currency == p_b.m_currency;
}

//-------------------------------------------------------------------------
bool Currency::operator != ( Currency const &  p_b ) const
{
    return m_currency != p_b.m_currency;
}

//-------------------------------------------------------------------------
//  Amount
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
std::string Amount::to_string ( result  p_result )
{
    switch( p_result )
    {
        case result::ok:                     return "ok";
        case result::overflow:               return "overflow";
        case result::division_by_zero:       return "division_by_zero";
        case result::incompatible_currency:  return "incompatible_currency";
        case result::invalid_value:          return "invalid_value";
        case result::invalid_currency:       return "invalid_currency";
        case result::invalid_mode:           return "invalid_mode";
        case result::invalid_precision:      return "invalid_precision";
        default:    assert( false );
                    throw std::invalid_argument( "Amount::to_string" );
    }
}

//-------------------------------------------------------------------------
// Comparison, returns true if the two amounts are equals
bool Amount::operator == ( const Amount & p_b ) const
{
    return m_value     == p_b.m_value      &&
           m_precision == p_b.m_precision  &&
           m_currency  == p_b.m_currency;
}

//-------------------------------------------------------------------------
// Comparison, returns true if the two amounts are not equals
// or have different currency
bool Amount::operator != ( const Amount & p_b ) const
{
    return m_value     != p_b.m_value      ||
           m_precision != p_b.m_precision  ||
           m_currency  != p_b.m_currency;
}

//-------------------------------------------------------------------------
// Returns true if the value is zero
bool Amount::is_zero ( void ) const
{
    return m_value == 0;
}

//-------------------------------------------------------------------------
// Returns true if the value is greater than zero
bool Amount::is_positive ( void ) const
{
    return m_value > 0;
}

//-------------------------------------------------------------------------
// Returns true if the value is lower than zero
bool Amount::is_negative ( void ) const
{
    return m_value < 0;
}

//-------------------------------------------------------------------------
// Comparison, set p_result: <b: -1, =b: 1, >b: 1
Amount::result Amount::comp ( const Amount & p_b, int & p_result ) const
{
    if ( m_currency != p_b.m_currency || m_precision != p_b.m_precision )
        return result::incompatible_currency;
    //
         if ( m_value < p_b.m_value )    p_result = -1;
    else if ( m_value > p_b.m_value )    p_result =  1;
    else                                 p_result =  0;
    //
    return result::ok;
}

//-------------------------------------------------------------------------
// Set the amount with the given value and currency
//  "12.5","EUR" -> 12500000,0x455552,6
Amount::result Amount::set ( const std::string &  p_value,
                             const std::string &  p_currency )
{
    m_set = false;
    //
    if ( ! m_currency.set( p_currency ) )
        return result::invalid_currency;
    //
    m_precision = m_currency.get_precision();
    //
    auto convertion_result = convert( p_value, m_precision, m_value );
    //
    m_set = convertion_result == result::ok;
    //
    return convertion_result;
}

//-------------------------------------------------------------------------
// Returns true if the Amount has not yet been set
// Note: if set was called but failed, returns true
bool Amount::is_set ( void ) const
{
    return m_set && m_currency.is_set();
}

//-------------------------------------------------------------------------
// Retrieve the amount as a string
//  12500000,0x455552,6 -> "12.5"
std::string Amount::value ( void ) const
{
    int64_t     bias  = std::pow( 10, m_precision );  // m_precision max is 18
    auto        value = std::div( m_value, bias );    // bias cannot be 0
    std::string text;
    //
    // Adds the sign (std::div puts the sign in rem if quot is 0)
    if ( m_value < 0 )
        text += '-';
    //
    // Adds the integer part
    text += std::to_string( std::abs( value.quot ) );
    //
    // Then eventually the decimal part
    if ( value.rem != 0 ) {
        text += ".";
        //
        for ( int64_t decimal = std::abs( value.rem );
                      decimal != 0 && bias >= 10; ) {
            bias    /= 10;  // bias >= 1
            auto d   = std::div( decimal, bias );
            text    += "0123456789"[ d.quot ];
            decimal  = d.rem;
        }
    }
    //
    return text;
}

//-------------------------------------------------------------------------
// Retrieve the currency as a string
//  12500000,0x455552,6 -> "EUR"
std::string Amount::currency ( void ) const
{
    return m_currency.get_code();
}

//-------------------------------------------------------------------------
// Retrieve the currency numeric code
//  12500000,0x455552,6 -> 978
unsigned Amount::numeric_code ( void ) const
{
    return m_currency.get_numeric_code();
}

//-------------------------------------------------------------------------
// Retrieve the symbol as a string
//  12500000,0x455552,6 -> "€"
std::string Amount::symbol ( void ) const
{
    return m_currency.get_symbol();
}

//-------------------------------------------------------------------------
// Returns the local national symbol of a currency (default: ¤)
//  12500000,0x455552,6 -> €; 12500000,0x544842,4 -> ฿
std::string Amount::symbol_local ( void ) const
{
    return m_currency.get_symbol_local();
}

//-------------------------------------------------------------------------
// Retrieve the precision
//  12500000,0x455552,6 -> 6
unsigned Amount::precision ( void ) const
{
    return m_precision;
}

//-------------------------------------------------------------------------
// Returns the common currency sub-division
//  12500000,0x455552,6 -> 2
unsigned  Amount::subunit ( void ) const
{
    return m_currency.get_subunit();
}

//-------------------------------------------------------------------------
// Retrieves the object amount as a formatted string
//   p_group_mode:    0 none, 3 thousands, 4 myriad or 7 (Indian 2 2 3)
//   p_currency_mode: 0 none, 1 code before, 2 code after, 3 symbol before,
//                    4 symbol after, 5 local symbol before, 6 local symbol after
// The amount is truncated to the given precision (round down)
std::string Amount::format ( unsigned             p_precision          /* = 32  */,
                             const std::string &  p_decimal_separator  /* = "." */,
                             unsigned             p_group_mode         /* = 3   */,
                             const std::string &  p_group_separator    /* = " " */,
                             unsigned             p_currency_mode      /* = 2   */,
                             const std::string &  p_currency_separator /* = " " */ ) const
{
    std::string text = std::to_string( m_value );
    unsigned    sign = m_value < 0 ? 1 : 0;
    //
    // Add missing leading 0                                     // with precision = 6:
    if ( text.length() <= (m_precision + sign) )                 // "-1000001" minimal length: 8
        text.insert( sign,                                       // eventually after the -
                     (m_precision + sign + 1) - text.length(),   // "-123"  ((6+1+1)-4=4) "-0000123"
                    '0' );
    //
    // Remove extra decimals
    unsigned  final_precision;
    //
    if ( p_precision < m_precision ) {                           // less requested than available
        text.erase( text.length() - ( m_precision - p_precision ) );
        final_precision = p_precision;
    } else {                                                     // cannot have more than what is available
        final_precision = m_precision;
    }
    //
    // Add decimal separator
    unsigned decimal_position = text.length() - final_precision;
    if ( final_precision > 0 )
        text.insert( decimal_position, p_decimal_separator );
    //
    // Do digits grouping
    std::vector< unsigned > groups;
    unsigned                group = 0, pos = decimal_position;
    //
    switch( p_group_mode )
    {
        case 3:  groups = { 3, 3, 3 }; break;  // only one group size: 3
        case 4:  groups = { 4, 4, 4 }; break;  // only one group size: 4
        case 7:  groups = { 3, 2, 2 }; break;  // grouped by 3,2,2 starting from decimal separator
        default: groups = { 20 };      break;  // one big group with all digits
    }
    //
    while ( ( pos - sign ) > groups[ group ] ) {  // no separator between - and first digit
        pos -= groups[ group ];
        text.insert( pos, p_group_separator );
        group = ( group + 1 ) % 3;  // move to next group size
    }
    //
    // Add currency
    switch( p_currency_mode ) {
        case 1:  text  = m_currency.get_code()         + p_currency_separator         + text; break;
        case 2:  text += p_currency_separator          + m_currency.get_code();               break;
        case 3:  text  = m_currency.get_symbol()       + p_currency_separator         + text; break;
        case 4:  text += p_currency_separator          + m_currency.get_symbol();             break;
        case 5:  text  = m_currency.get_symbol_local() + p_currency_separator         + text; break;
        case 6:  text += p_currency_separator          + m_currency.get_symbol_local();       break;
        default: break;
    }
    //
    return text;
}

//-------------------------------------------------------------------------
// Add an amount to the current object
//  12500000,6,0x455552 + 500000,6,0x455552 = 13000000,6,0x455552
Amount::result Amount::add ( const Amount &  p_amount )
{
    if ( m_currency != p_amount.m_currency || m_precision != p_amount.m_precision )
        return result::incompatible_currency;
    //
    if ( __builtin_add_overflow( m_value, p_amount.m_value, & m_value ) )
        return result::overflow;
    //
    return result::ok;
}

//-------------------------------------------------------------------------
// Subtract an amount from the current object
//  12500000,6,0x455552 - 500000,6,0x455552 = 12000000,6,0x455552
Amount::result Amount::sub ( const Amount &  p_amount )
{
    if ( m_currency != p_amount.m_currency || m_precision != p_amount.m_precision )
        return result::incompatible_currency;
    //
    if ( __builtin_sub_overflow( m_value, p_amount.m_value, & m_value ) )
        return result::overflow;
    //
    return result::ok;
}

//-------------------------------------------------------------------------
// Multiply the amount by a value
//  12500000,6,0x455552 * 2 = 25000000,6,0x455552
Amount::result Amount::mul ( const std::string &  p_value )
{
    int64_t         numerator, denominator;
    Amount::result  res;
    //
    if ( ( res = fraction( p_value, numerator, denominator ) ) != result::ok  )
        return res;
    //
    if ( numerator != 1 )
        if ( __builtin_mul_overflow( m_value, numerator, & m_value ) )
            return result::overflow;
    //
    if ( denominator != 1 )
        m_value /= denominator;
    //
    return result::ok;
}

//-------------------------------------------------------------------------
// Divide the amount by a value
//  12500000,6,0x455552 / 5 = 2500000,6,0x455552
Amount::result Amount::div ( const std::string &  p_value )
{
    int64_t         numerator, denominator;
    Amount::result  res;
    //
    if ( ( res = fraction( p_value, numerator, denominator ) ) != result::ok  )
        return res;
    //
    if ( numerator == 0 )
        return result::division_by_zero;
    //
    if ( denominator != 1 )
        if ( __builtin_mul_overflow( m_value, denominator, & m_value ) )
            return result::overflow;
    //
    if ( numerator != 1 )
        m_value /= numerator;
    //
    return result::ok;
}

//-------------------------------------------------------------------------
// Set the amount to the given percentage
//  12500000,6,0x455552 % 5 = 625000,6,0x455552
Amount::result Amount::percent ( const std::string &  p_value )
{
    int64_t         numerator, denominator;
    Amount::result  res;
    //
    if ( ( res = fraction( p_value, numerator, denominator ) ) != result::ok  )
        return res;
    //
    if ( numerator != 1 )
        if ( __builtin_mul_overflow( m_value, numerator, & m_value ) )
            return result::overflow;
    //
    m_value /= denominator * 100;
    //
    return result::ok;
}

//-------------------------------------------------------------------------
// Set the current object to the minimal value of the two amount
//  12500000,6,0x455552 & 500000,6,0x455552 = 500000,6,0x455552
Amount::result Amount::min ( const Amount & p_amount )
{
    if ( m_currency != p_amount.m_currency || m_precision != p_amount.m_precision )
        return result::incompatible_currency;
    //
    m_value = std::min( m_value, p_amount.m_value );
    //
    return result::ok;
}

//-------------------------------------------------------------------------
// Set the current object to the maximal value of the two amount
//  12500000,6,0x455552 & 500000,6,0x455552 = 12500000,6,0x455552
Amount::result Amount::max ( const Amount & p_amount )
{
    if ( m_currency != p_amount.m_currency || m_precision != p_amount.m_precision )
        return result::incompatible_currency;
    //
    m_value = std::max( m_value, p_amount.m_value );
    //
    return result::ok;
}

//-------------------------------------------------------------------------
// Rounds a number according to the given mode
//   p_mode:      1 up, 2 down, 3 commercial
//   p_precision: 0 - 5                      0.123456
// Examples:    round up, precision 2
//                  2.0864... → 2.09 EUR
//                  4.1701... → 4.18 EUR
//              round down, precision 2
//                  2.0864... → 2.08 EUR
//                  4.1701... → 4.17 EUR
//              round commercial, precision 2
//                  2.0864... → 2.09 EUR
//                  4.1701... → 4.17 EUR

namespace {
    //
    // Returns 10 ^ p_precision
    //  p_precision: typical range: 0 - 6, maximum 18
    int64_t pow ( int64_t p_precision )
    {
        static const int64_t pow10[19] =
        { 1,                        //  0
          10,                       //  1
          100,                      //  2
          1000,                     //  3
          10000,                    //  4
          100000,                   //  5
          1000000,                  //  6
          10000000,                 //  7
          100000000,                //  8
          1000000000,               //  9
          10000000000,              // 10
          100000000000,             // 11
          1000000000000,            // 12
          10000000000000,           // 13
          100000000000000,          // 14
          1000000000000000,         // 15
          10000000000000000,        // 16
          100000000000000000,       // 17
          1000000000000000000 };    // 18
        //
        return pow10[ std::max( 0l, std::min( p_precision, 18l ) ) ];
    }
    //
    // Rounds the value by excess to the requested precision
    // Assumes: p_value_precision > p_required_precision
    int64_t round_up ( int64_t p_value, unsigned p_value_precision, unsigned p_required_precision )
    {
        auto exponent = pow( p_value_precision - p_required_precision );
        auto division = std::div( p_value, exponent );
        //
        if ( division.rem != 0 )
            division.quot++;
        //
        return division.quot * exponent;
    }
    //
    // Rounds the value by default to the requested precision
    // Assumes: p_value_precision > p_required_precision
    int64_t round_down ( int64_t p_value, unsigned p_value_precision, unsigned p_required_precision )
    {
        auto exponent = pow( p_value_precision - p_required_precision );
        //
        p_value /= exponent; // remove n digits
        p_value *= exponent; // add n 0
        //
        return p_value;
    }
    //
    // Rounds the value by excess or default depending on the last digit to the requested precision
    // Assumes: p_value_precision > p_required_precision
    int64_t round_commercial ( int64_t p_value, unsigned p_value_precision, unsigned p_required_precision )
    {
        auto exponent = pow( p_value_precision - p_required_precision - 1 );     // keep the highest digit it test it
        //
        p_value /= exponent;                                                     // remove n-1 digits
        //
        if ( ( p_value % 10 ) < 5 ) { p_value /= 10;            p_value *= 10; } // replace highest digit by 0,
        else                        { p_value /= 10; p_value++; p_value *= 10; } // and possibly increase value
        //
        p_value *= exponent;                                                     // add n-1 0
        //
        return p_value;
    }
}

Amount::result Amount::round ( unsigned  p_mode,
                               unsigned  p_precision )
{
    if ( p_precision  > m_precision )  // invalid precision
        return result::invalid_precision;
    if ( p_precision == m_precision )  // no rounding
        return result::ok;
    //
    // checked: p_precision < m_precision
    //
    switch( p_mode )
    {
        case 1: // up
            m_value = round_up( m_value, m_precision, p_precision );
            break;
        case 2: // down
            m_value = round_down( m_value, m_precision, p_precision );
            break;
        case 3: // commercial
            m_value = round_commercial( m_value, m_precision, p_precision );
            break;
        default:
            return result::invalid_mode;
    }
    //
    return result::ok;
}

//-------------------------------------------------------------------------
// Convert a string representing a real into an infinite precision signed,
// biased by the exponent given in p_precision (6 -> * 1000000)
//  "12.34",6      -> 12340000
//  "12.3400000",6 -> 12340000
Amount::result Amount::convert ( std::string  p_text,
                                 unsigned     p_precision,
                                 int64_t &    p_value )
{
    size_t dot           = p_text.find( '.' );
    size_t nb_zero_added;
    //
    if ( dot == std::string::npos ) {
        nb_zero_added = p_precision;
    } else {
        while ( p_text.back() == '0' )                    // remove trailing 0
            p_text.pop_back();                            // '12.3400000' -> '12.34'
        //
        size_t nb_decimals = p_text.length() - dot - 1;   // '12.34'   5-2-1 = 2
        if ( nb_decimals > p_precision )                  // '1.0000001' and also
            return result::invalid_precision;             // '.1' (size_t overflow)
        //
        nb_zero_added = p_precision - nb_decimals;        // 6-2 = 4
        p_text.erase( dot, 1 );                           // '1234'
    }
    //
    p_text.insert( p_text.end(), nb_zero_added, '0' );    // '12340000'
    //
    // By adding the scan of an extra char t, we check that there is
    // no trailing character after the number
    char t;
    if ( sscanf( p_text.c_str(), "%" SCNd64 "%c", & p_value, & t) != 1 )
        return result::invalid_value;
    //
    // Check overflow
    if ( p_value == INT64_MIN || p_value == INT64_MAX )
        return result::overflow;
    //
    return result::ok;
}

//-------------------------------------------------------------------------
// Convert a string representing a real into an infinite precision fraction
// Fraction is reduce to minimize overflow risks
//  "3.14" -> 157/50  "42" -> 42/1   "0.1" -> 1/10   "18.6" -> 93/5
Amount::result Amount::fraction ( std::string  p_text,
                                  int64_t &    p_numerator,
                                  int64_t &    p_denominator )
{
    p_denominator = 1;
    //
    size_t dot = p_text.find( '.' );
    //
    if ( dot != std::string::npos ) {
        size_t decimals = p_text.length() - dot - 1;   // '3.14'   4-1-1 = 2
        //
        while ( decimals-- )
            p_denominator *= 10;                        // 100
        //
        p_text.erase( dot, 1 );                         // '314'
    }
    //
    // By adding the scan of an extra char t, we check that there is
    // no trailing character after the number
    char t;
    if ( sscanf( p_text.c_str(), "%" SCNd64 "%c", & p_numerator, & t) != 1 )
        return result::invalid_value;
    //
    // Check overflow
    if ( p_numerator == INT64_MIN || p_numerator == INT64_MAX )
        return result::overflow;
    //
    // Try to reduce fraction: common divisor can only be 2 and 5 (p_denominator is a multiple of 10)
    for ( int64_t i : { 2, 5 } ) {
        for (;;) {
            auto n = std::div( p_numerator,   i );
            auto d = std::div( p_denominator, i );
            //
            if ( n.rem != 0 || d.rem != 0 ) break;
            //
            p_numerator   = n.quot;
            p_denominator = d.quot;
        }
    }
    //
    return result::ok;
}

} // namespace adawat
