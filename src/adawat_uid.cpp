// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <atomic>
#include <cassert>
#include <cinttypes>
#include <cmath>
#include <cstdlib>
#include <inttypes.h>
#include <mutex>
#include <stdexcept>
#include <time.h>

#include "adawat_crypto.h"
#include "adawat_encoding.h"
#include "adawat_string.h"
#include "adawat_uid.h"

namespace adawat {

namespace {

    //-------------------------------------------------------------------------
    // Convert p_value into a 6 characters long string, using base 62.
    // p_value must be lower than 62^6 (56800235584, 0xd398ed040, 35bits)
    // since output it 6 characters long
    std::string base62 ( long long int  p_value )
    {
        if ( p_value >= 56800235584 ) {
            assert( false );
            throw std::invalid_argument( "base62" );
        }
        //
        char          buffer[ 7 ];
        std::lldiv_t  d;
        //
        d.quot = p_value;
        //
        for ( int i = 5; i >= 0; i-- ) {
            d = std::lldiv( d.quot, 62 );
            buffer[ i ] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"[ d.rem ];
        }
        //
        buffer[ 6 ] = 0;
        //
        return buffer;
    }

};  // namespace

//-------------------------------------------------------------------------
// Get the alphabet corresponding to the given type
std::tuple< size_t, const char * >
get_alphabet ( uid_alphabet  p_alphabet )
{
    switch ( p_alphabet )
    {
    case uid_alphabet::digits:            return { 10, "0123456789" };
    case uid_alphabet::upper_alpha:       return { 26, "ABCDEFGHIJKLMNOPQRSTUVWXYZ" };
    case uid_alphabet::lower_alpha:       return { 26, "abcdefghijklmnopqrstuvwxyz" };
    case uid_alphabet::upper_hexadecimal: return { 16, "0123456789ABCDEF" };
    case uid_alphabet::lower_hexadecimal: return { 16, "0123456789abcdef" };
    case uid_alphabet::upper_alphanum:    return { 36, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ" };
    case uid_alphabet::lower_alphanum:    return { 36, "0123456789abcdefghijklmnopqrstuvwxyz" };
    case uid_alphabet::base32:            return { 32, "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567" };
    case uid_alphabet::base32_crockford:  return { 32, "0123456789ABCDEFGHJKMNPQRSTVWXYZ" };
    case uid_alphabet::base62:            return { 62, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" };
    default:    assert( false );
                throw std::invalid_argument( "get_alphabet" );
    }
}

//-------------------------------------------------------------------------
// Returns a unique, pseudo linear, 22 to 26 characters long identifier.
// The returned value is optimized for a given period (the period
// of time the id will be kept and should not conflict with others).
std::string uid_ts ( uid_type  p_type, bool  p_secured /* = false */ )
{
    // Prefetch random data: we need at most three bytes
    Data random;
    //
    crypto_random( 3, random, false );
    //
    // Build the time prefix
    long long int   prefix[4];
    struct timespec ts;
    size_t          length;
    //
    timespec_get( & ts, TIME_UTC );
    //
    switch ( p_type )
    {
    case uid_type::short_ts:  // 4096s - 2 bytes time prefix
        {
            prefix[ 0 ] =  ( ts.tv_sec >> 4 ) & 0xFF;       // upper 8 bits
            prefix[ 1 ] = (( ts.tv_sec << 4 ) & 0xF0 ) |    // lower 4 bits
                           ( ts.tv_nsec / 62500000 );       // 1e9 / 2^4 bits -> max = 15
            prefix[ 2 ] = random[ 0 ];
            prefix[ 3 ] = random[ 1 ];
            //
            length = p_secured ? 24 : 22;
        }
        break;
    case uid_type::medium_ts:  // 48d - 3 bytes time prefix
        {
            prefix[ 0 ] =  ( ts.tv_sec >> 14 ) & 0xFF;       // upper 8 bits
            prefix[ 1 ] =  ( ts.tv_sec >>  6 ) & 0xFF;       // middle 8 bits
            prefix[ 2 ] = (( ts.tv_sec <<  2 ) & 0xFC ) |    // lower 6 bits
                           ( ts.tv_nsec / 250000000 );       // 1e9 / 2^2 bits -> max = 3
            prefix[ 3 ] = random[ 0 ];
            //
            length = p_secured ? 25 : 22;
        }
        break;
    case uid_type::long_ts:  // 136y - 4 bytes time prefix
        {
            prefix[ 0 ] = ( ts.tv_sec >> 24 ) & 0xFF;
            prefix[ 1 ] = ( ts.tv_sec >> 16 ) & 0xFF;
            prefix[ 2 ] = ( ts.tv_sec >>  8 ) & 0xFF;
            prefix[ 3 ] =   ts.tv_sec         & 0xFF;
            //
            length = p_secured ? 26 : 22;
        }
        break;
    default:    assert( false );
                throw std::invalid_argument( "uid_ts" );
    }
    //
    // Start the UId by the time prefix then fill with random data
    long long int time_prefix = prefix[ 0 ] << 24 |  // 32 bits time prefix                      
                                prefix[ 1 ] << 16 |
                                prefix[ 2 ] <<  8 |
                                prefix[ 3 ];
    long long int extra_bits  = static_cast<long long int>( random[ 0 ] & 0x03 ); // base62() accepts 35 bits
    //
    return
        base62( ( time_prefix << 3 ) + extra_bits )
        +
        uid_no_ts( length - 6, uid_alphabet::base62 ); // random data ( 95bits@22, 107bits@24, 113bits@25, 119bits@26 )
}

//-------------------------------------------------------------------------
// Returns a fixed length uid, without timestamp, using the
// given alphabet.
std::string  uid_no_ts ( size_t        p_length,
                         uid_alphabet  p_alphabet /* = uid_alphabet::base62 */ )
{
    // Retrieve the alphabet to use
    auto [ alphabet_length, alphabet ] = get_alphabet( p_alphabet );
    //
    // Prefetch random data
    //
    // Since data above alphabet size are ignored, we need to
    // generate more (use cross-multiplication)
    size_t  initial_prefetch_size = p_length * 256 / alphabet_length;
    size_t  next_prefetch_size    = initial_prefetch_size / 8 + 1;   // 12% more
    //
    Data    prefetch_random;
    //
    size_t  p = 0;  // prefetch_random reading pointers
    //
    crypto_random( initial_prefetch_size + next_prefetch_size,
                   prefetch_random,
                   false );
    //
    // Then fill the UId with random data
    std::string uid;
    //
    uid.reserve( p_length );
    //
    for (;;)
    {
        // Until done or no more random data...
        while ( p < prefetch_random.size() && uid.length() < p_length ) {
            auto next_random = prefetch_random[ p++ ];
            //
            // Ignore value not in the alphabet size
            if ( next_random < alphabet_length )
                uid += alphabet[ next_random ];
        }
        //
        // ... done
        if ( uid.length() >= p_length )
            return uid;
        //
        // ... no more random data
        crypto_random( next_prefetch_size, prefetch_random, false );
        //
        p = 0;  // reset read pointer
    }
}

//-------------------------------------------------------------------------
// Generates an id that is guaranteed to be unique per day *per process*.
// The day period is extended to take into account clock drifts (87300
// instead of 86400). The id is always 6 digits long, base62 encoded.
// The period between two different ids is 5us; if the generated id is
// the same as the previous one, retries.
// Since caller is slowed down (rather than generating id in the future)
// another process with the same id on a given machine cannot generate
// the same id (@ the stop/start process time is more that 5us & the
// OS reallocate process id immediately). So the couple pid / uid can
// be consider unique per server.
std::string uid_daily ( void )
{
    // Generate the id
    static std::mutex   s_lock;
    static int64_t      s_last_id = 0;
    //
    struct timespec     ts;  // seconds and nanoseconds
    long long int       id;  // in fives of microsecond
    //
    for (;;) {
        std::lock_guard lock( s_lock ); // need to lock the generator (clock_gettime) and last_id
        //
        // Get time with nanosecond as id
        clock_gettime( CLOCK_MONOTONIC, &ts );
        //
        id = ( ts.tv_sec % 87300ll ) * 200000ll +    // extended day period in fives of microsecond
             ( ts.tv_nsec            /   5000ll );   // nanoseconds in fives of microsecond
        //
        // Continue if it is the same as last generated one
        if ( s_last_id != id ) {
            s_last_id = id;
            break;
        }
    }
    //
    // Convert to text
    return base62( id );
}

//-------------------------------------------------------------------------
// Returns an UUID v4 or v7 unique id
std::string uid_uuid( uuid_type           p_type        /* = uuid_type::v4 */,
                      uuid_namespace      p_namespace   /* = uuid_namespace::dns */,
                      const std::string & p_name        /* = "" */ )
{
    Data data;
    //
    crypto_random( 16, data, false );
    //
    switch ( p_type )
    {
    case uuid_type::v1:
    case uuid_type::v6:
        {
            struct timespec ts;
            //
            timespec_get( &ts, TIME_UTC );
            //
            uint64_t timestamp = static_cast< uint64_t >( ts.tv_sec        ) * 10'000'000 +
                                 static_cast< uint64_t >( ts.tv_nsec / 100 )              +
                                 0x01B21DD213814000; // number of 100-nanosecond interval from 1582/10/15 to 1970/01/01
            //
            if ( p_type == uuid_type::v1 )
            {
                data[ 0 ] =   ( timestamp >> 24 ) & 0xFF;
                data[ 1 ] =   ( timestamp >> 16 ) & 0xFF;
                data[ 2 ] =   ( timestamp >>  8 ) & 0xFF;
                data[ 3 ] =   ( timestamp       ) & 0xFF;
                data[ 4 ] =   ( timestamp >> 40 ) & 0xFF;
                data[ 5 ] =   ( timestamp >> 32 ) & 0xFF;
                data[ 6 ] = ( ( timestamp >> 56 ) & 0x0F ) | 0x10;  // version 1
                data[ 7 ] =   ( timestamp >> 48 ) & 0xFF;
            }
            else // v6
            {
                data[ 0 ] =   ( timestamp >> 52 ) & 0xFF;
                data[ 1 ] =   ( timestamp >> 44 ) & 0xFF;
                data[ 2 ] =   ( timestamp >> 36 ) & 0xFF;
                data[ 3 ] =   ( timestamp >> 28 ) & 0xFF;
                data[ 4 ] =   ( timestamp >> 20 ) & 0xFF;
                data[ 5 ] =   ( timestamp >> 12 ) & 0xFF;
                data[ 6 ] = ( ( timestamp >>  8 ) & 0x0F ) | 0x60;  // version 6
                data[ 7 ] =   ( timestamp       ) & 0xFF;
            }
            //
            static std::atomic_uint16_t sequence = rand();
            //
            sequence++;
            //
            data[ 8 ] =  ( ( sequence >> 8 ) & 0x3F ) | 0x80;  // sequence hi, variant 1
            data[ 9 ] =      sequence        & 0xFF;           // sequence low
            //
            // Use random instead of MAC address in bytes 10-15, as allowed by RFC4122 4.1.6
            data[ 10 ] |= 0x01; // and set the multicast bit in that case
        }
        break;
    case uuid_type::v3:
    case uuid_type::v5:
        {
            Data hash_input;
            switch ( p_namespace ) {
                case uuid_namespace::dns:
                    hash_input = Data( { 0x6b, 0xa7, 0xb8, 0x10, 0x9d, 0xad, 0x11, 0xd1, 0x80, 0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8 } );
                    break;
                case uuid_namespace::url:
                    hash_input = Data( { 0x6b, 0xa7, 0xb8, 0x11, 0x9d, 0xad, 0x11, 0xd1, 0x80, 0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8 } );
                    break;
                case uuid_namespace::oid:
                    hash_input = Data( { 0x6b, 0xa7, 0xb8, 0x12, 0x9d, 0xad, 0x11, 0xd1, 0x80, 0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8 } );
                    break;
                case uuid_namespace::x500:
                    hash_input = Data( { 0x6b, 0xa7, 0xb8, 0x14, 0x9d, 0xad, 0x11, 0xd1, 0x80, 0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8 } );
                    break;
                default:
                    assert( false );
                    throw std::invalid_argument( "uid_uuid" );
            }
            //
            hash_input.insert( hash_input.end(), p_name.begin(), p_name.end() );
            //
            if ( p_type == uuid_type::v3 ) {
                crypto_digest( hash_input, data, EVP_md5() );
            } else {  // v5
                crypto_digest( hash_input, data, EVP_sha1() );
                data.resize( 16 );  // truncate to 128 bits
            }
            //
            data[ 6 ] = ( data[ 6 ] & 0x0F ) | ( p_type == uuid_type::v3 ? 0x30 : 0x50 );  // version 3 or 5
            data[ 8 ] = ( data[ 8 ] & 0x3F ) | 0x80;  // variant 1
        }
        break;
    case uuid_type::v4:
        data[ 6 ] = ( data[ 6 ] & 0x0F ) | 0x40; // version 4
        data[ 8 ] = ( data[ 8 ] & 0x3F ) | 0x80; // variant 1
        break;
    case uuid_type::v7:
        {
            struct timespec  ts;
            //
            timespec_get( & ts, TIME_UTC );
            //
            uint64_t nb_ms = static_cast<uint64_t>( ts.tv_sec              ) * 1'000 +
                             static_cast<uint64_t>( ts.tv_nsec / 1'000'000 );
            //
            data[ 0 ] = ( nb_ms >> 40 ) & 0xFF;
            data[ 1 ] = ( nb_ms >> 32 ) & 0xFF;
            data[ 2 ] = ( nb_ms >> 24 ) & 0xFF;
            data[ 3 ] = ( nb_ms >> 16 ) & 0xFF;
            data[ 4 ] = ( nb_ms >>  8 ) & 0xFF;
            data[ 5 ] = ( nb_ms       ) & 0xFF;
            //
            data[ 6 ] = ( data[ 6 ] & 0x0F ) | 0x70; // version 7
            data[ 8 ] = ( data[ 8 ] & 0x3F ) | 0x80; // variant 1
        }
        break;
    default:
        assert( false );
        throw std::invalid_argument( "uid_uuid" );
    }
    //
    std::string uuid = encoding_base16_encode( data, true );
    //
    uuid.reserve( 36 );
    uuid.insert (  8, 1, '-');
    uuid.insert ( 13, 1, '-');
    uuid.insert ( 18, 1, '-');
    uuid.insert ( 23, 1, '-');
    //
    return uuid;
}

//-------------------------------------------------------------------------
// Convert uid_type to string
std::string uid_type_cvt ( uid_type  p_type )
{
    switch ( p_type )
    {
    case uid_type::short_ts:  return "uid_short_ts";
    case uid_type::medium_ts: return "uid_medium_ts";
    case uid_type::long_ts:   return "uid_long_ts";
    default:    assert( false );
                throw std::invalid_argument( "uid_type_cvt" );
    }
}

//-------------------------------------------------------------------------
// Convert uid_type from string
bool uid_type_cvt ( const std::string &  p_text,
                    uid_type &           p_type )
{
    if ( p_text == "uid_short_ts"  ) { p_type = uid_type::short_ts;  return true; }
    if ( p_text == "uid_medium_ts" ) { p_type = uid_type::medium_ts; return true; }
    if ( p_text == "uid_long_ts"   ) { p_type = uid_type::long_ts;   return true; }
    //
    return false;
}

//-------------------------------------------------------------------------
// Convert uid_alphabet to string
std::string uid_alphabet_cvt ( uid_alphabet  p_alphabet )
{
    switch ( p_alphabet )
    {
    case uid_alphabet::digits:            return "uid_digits";
    case uid_alphabet::upper_alpha:       return "uid_upper_alpha";
    case uid_alphabet::lower_alpha:       return "uid_lower_alpha";
    case uid_alphabet::upper_hexadecimal: return "uid_upper_hexadecimal";
    case uid_alphabet::lower_hexadecimal: return "uid_lower_hexadecimal";
    case uid_alphabet::upper_alphanum:    return "uid_upper_alphanum";
    case uid_alphabet::lower_alphanum:    return "uid_lower_alphanum";
    case uid_alphabet::base32:            return "uid_base32";
    case uid_alphabet::base32_crockford:  return "uid_base32_crockford";
    case uid_alphabet::base62:            return "uid_base62";
    default:    assert( false );
                throw std::invalid_argument( "uid_alphabet_cvt" );
    }
}

//-------------------------------------------------------------------------
// Convert uid_alphabet from string
bool uid_alphabet_cvt ( const std::string &  p_text,
                        uid_alphabet &       p_alphabet )
{
    if ( p_text == "uid_digits"            ) { p_alphabet = uid_alphabet::digits           ; return true; }
    if ( p_text == "uid_upper_alpha"       ) { p_alphabet = uid_alphabet::upper_alpha      ; return true; }
    if ( p_text == "uid_lower_alpha"       ) { p_alphabet = uid_alphabet::lower_alpha      ; return true; }
    if ( p_text == "uid_upper_hexadecimal" ) { p_alphabet = uid_alphabet::upper_hexadecimal; return true; }
    if ( p_text == "uid_lower_hexadecimal" ) { p_alphabet = uid_alphabet::lower_hexadecimal; return true; }
    if ( p_text == "uid_upper_alphanum"    ) { p_alphabet = uid_alphabet::upper_alphanum   ; return true; }
    if ( p_text == "uid_lower_alphanum"    ) { p_alphabet = uid_alphabet::lower_alphanum   ; return true; }
    if ( p_text == "uid_base32"            ) { p_alphabet = uid_alphabet::base32           ; return true; }
    if ( p_text == "uid_base32_crockford"  ) { p_alphabet = uid_alphabet::base32_crockford ; return true; }
    if ( p_text == "uid_base62"            ) { p_alphabet = uid_alphabet::base62           ; return true; }
    //
    return false;
}

} // namespace adawat
