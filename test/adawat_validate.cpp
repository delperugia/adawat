// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// Copyright 2018 Pierre del Perugia
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <algorithm>
#include <atomic>
#include <list>
#include <map>
#include <math.h>
#include <openssl/err.h>
#include <set>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "adawat_crashhandler.h"
#include "adawat_crypto.h"
#include "adawat_encoding.h"
#include "adawat_error_control.h"
#include "adawat_file.h"
#include "adawat_money.h"
#include "adawat_stats.h"
#include "adawat_string.h"
#include "adawat_threading_pool.hpp"
#include "adawat_threading_queue.hpp"
#include "adawat_time.h"
#include "adawat_uid.h"
#include "adawat_url.h"

using namespace std::literals::chrono_literals;
using namespace std::string_literals;
using namespace adawat;

bool g_Verbose = false;

//-------------------------------------------------------------------------
void start ( const char * p_cszModule )
{
    printf("%s:", p_cszModule);
}

//-------------------------------------------------------------------------
bool check ( const char * p_cszModule, unsigned p_uSection, unsigned p_uTest, bool p_bCondition )
{
    if ( g_Verbose || ! p_bCondition )
        printf("\n   %s.%02u.%02u: %s", p_cszModule, p_uSection, p_uTest, p_bCondition ? "ok" : "error");
    //
    return p_bCondition;
}

//-------------------------------------------------------------------------
bool stop ( const char * p_cszModule, bool p_bCondition )
{
    if ( g_Verbose || ! p_bCondition )
        printf("\n%s: %s\n", p_cszModule, p_bCondition ? "ok" : "error");
    else
        printf(" ok\n");
    //
    return p_bCondition;
}

//-------------------------------------------------------------------------
bool validate_common ( void )
{
    bool        ok = true;
    //
    Data        d1, d2( "543"s), d3( "876"s );
    std::string s1, s2;
    //
    s1 = "9876543210";
    d1 = s1;
    s2 = d1.str();
    //
    ok = ok && check("common",1, 1, s1 == s2 );
    ok = ok && check("common",1, 2, encoding_base16_encode( d1 = "210" ) == "323130" );
    ok = ok && check("common",1, 3, encoding_base16_encode( d2         ) == "353433" );
    ok = ok && check("common",1, 4, encoding_base16_encode( d3         ) == "383736" );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_crypto ( void )
{
    bool        ok = true;
    //
    std::string sShortMessage_1 = "Hello~";
    std::string sShortMessage_2 = " world";
    std::string sShortMessage   = sShortMessage_1 + sShortMessage_2; // "Hello~ world", less than one block
    std::string sMediumMessage  = "Hello, big world";                // one block
    std::string sLargeMessage   = "Hello, extraordinary world";      // more than one block
    std::string sXtraMessage    = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
    std::string sKey            = "0123456789ABCDEF";
    std::string sIV             = "FEDCBA9876543210";            // 46454443424139383736353433323130
    //
    Data data_s1( sShortMessage_1.begin(), sShortMessage_1.end() );
    Data data_s2( sShortMessage_2.begin(), sShortMessage_2.end() );
    Data data_s ( sShortMessage  .begin(), sShortMessage  .end() );
    Data data_m ( sMediumMessage .begin(), sMediumMessage .end() );
    Data data_l ( sLargeMessage  .begin(), sLargeMessage  .end() );
    Data data_x ( sXtraMessage   .begin(), sXtraMessage   .end() );
    Data data_k ( sKey           .begin(), sKey           .end() );
    Data data_v ( sIV            .begin(), sIV            .end() );
    Data data_o, data_oc[10];
    //
    ok = ok && check("crypto",1, 1, crypto_digest(data_s, data_o, EVP_sha256())         && encoding_base16_encode(data_o) == "B812C0E495984F3BEF4D328AABB1627C27CC293D15742AB269554CFFAB152667" );
    ok = ok && check("crypto",1, 2, crypto_digest(data_m, data_o, EVP_sha256())         && encoding_base16_encode(data_o) == "C2E92B22C763C78B046485B6BCF3BF3029591A6C20B6811C8A870CC4B9621CB0" );
    ok = ok && check("crypto",1, 3, crypto_digest(data_l, data_o, EVP_sha256())         && encoding_base16_encode(data_o) == "F34198E1FADCBE51AB7303DD878AA6CDEBC8191A518F920088B7D6B203965135" );
    ok = ok && check("crypto",1, 4, crypto_digest(data_x, data_o, EVP_sha256())         && encoding_base16_encode(data_o) == "2D8C2F6D978CA21712B5F6DE36C9D31FA8E96A4FA5D8FF8B0188DFB9E7C171BB" );
    ok = ok && check("crypto",1, 5, crypto_digest(data_x, data_o, EVP_md5())            && encoding_base16_encode(data_o) == "DB89BB5CEAB87F9C0FCC2AB36C189C2C" );
    ok = ok && check("crypto",1, 6, crypto_digest(data_x, data_o, EVP_sha1())           && encoding_base16_encode(data_o) == "CD36B370758A259B34845084A6CC38473CB95E27" );
    ok = ok && check("crypto",1, 7, crypto_digest(data_x, data_o, EVP_sha512())         && encoding_base16_encode(data_o) == "8BA760CAC29CB2B2CE66858EAD169174057AA1298CCD581514E6DB6DEE3285280EE6E3A54C9319071DC8165FF061D77783100D449C937FF1FB4CD1BB516A69B9" );
    //
    ok = ok && check("crypto",1, 8, crypto_digest(sShortMessage, data_o, EVP_sha256())  && encoding_base16_encode(data_o) == "B812C0E495984F3BEF4D328AABB1627C27CC293D15742AB269554CFFAB152667" );
    ok = ok && check("crypto",1, 9, crypto_digest({{std::cref(sShortMessage_1)},{std::cref(sShortMessage_2)}},
                                                                 data_o, EVP_sha256())  && encoding_base16_encode(data_o) == "B812C0E495984F3BEF4D328AABB1627C27CC293D15742AB269554CFFAB152667" );
    ok = ok && check("crypto",1,10, crypto_digest({{std::cref(data_s1)},{std::cref(data_s2)}},
                                                                 data_o, EVP_sha256())  && encoding_base16_encode(data_o) == "B812C0E495984F3BEF4D328AABB1627C27CC293D15742AB269554CFFAB152667" );
    //
    ok = ok && check("crypto",2, 1, crypto_hmac  (data_k, data_s, data_o, crypto_md_from_name( "sha256" ) )
                                                                                        && encoding_base16_encode(data_o) == "973C4F2CDEB7E7092AA5623AC08AFCC77D20F9A68E59829E379D1C447CAD71AD" );
    ok = ok && check("crypto",2, 2, crypto_hmac  (data_k, data_m, data_o, EVP_sha256()) && encoding_base16_encode(data_o) == "8D6409B02BD97B889181DB06F4C0F85BE644530763B1FBBA578B3101E30F01D8" );
    ok = ok && check("crypto",2, 3, crypto_hmac  (data_k, data_l, data_o, EVP_sha256()) && encoding_base16_encode(data_o) == "7D5B46E640EAEC2EBFCD29B83307DF4DE0A4AA52092F3ED743ABDC1A5562ED22" );
    ok = ok && check("crypto",2, 4, crypto_hmac  (data_k, data_x, data_o, EVP_sha256()) && encoding_base16_encode(data_o) == "0EE52CDE26EA0DE6661D3B3E58ECC3DF7587399AB4B0EF64E0A3BF4997C62F45" );
    ok = ok && check("crypto",2, 5, crypto_hmac  (data_k, data_x, data_o, EVP_md5())    && encoding_base16_encode(data_o) == "30EE3B34B23B1375693C0700D919705C" );
    ok = ok && check("crypto",2, 6, crypto_hmac  (data_k, data_x, data_o, EVP_sha1())   && encoding_base16_encode(data_o) == "37506F28F8535CFB4CDABFCA7E47415A3AD74192" );
    ok = ok && check("crypto",2, 7, crypto_hmac  (data_k, data_x, data_o, EVP_sha512()) && encoding_base16_encode(data_o) == "E7FCE0C8BBDCFFABBC50DE2A07DA114FFE8AC72B6AA7DC5DD50A4DFFE6A821A0C566319B0673F079ECCAA2EF1E89E219BD7C37811EA1E2742133CFF9A1AE28F3" );
    //
    ok = ok && check("crypto",2, 8, crypto_hmac  (data_k, sShortMessage, data_o, crypto_md_from_name( "sha256" ) )
                                                                                        && encoding_base16_encode(data_o) == "973C4F2CDEB7E7092AA5623AC08AFCC77D20F9A68E59829E379D1C447CAD71AD" );
    ok = ok && check("crypto",2, 9, crypto_hmac  (data_k, {{std::cref(sShortMessage_1)},{std::cref(sShortMessage_2)}}, data_o, crypto_md_from_name( "sha256" ) )
                                                                                        && encoding_base16_encode(data_o) == "973C4F2CDEB7E7092AA5623AC08AFCC77D20F9A68E59829E379D1C447CAD71AD" );

    ok = ok && check("crypto",2,10, crypto_hmac  (data_k, {{std::cref(data_s1)},{std::cref(data_s2)}}, data_o, crypto_md_from_name( "sha256" ) )
                                                                                        && encoding_base16_encode(data_o) == "973C4F2CDEB7E7092AA5623AC08AFCC77D20F9A68E59829E379D1C447CAD71AD" );
    //
    ok = ok && check("crypto",3, 1, crypto_cipher(data_k, data_v, data_s, data_oc[0], true, crypto_cipher_from_name( "aes-128-cbc" ), true)
                                                                                                                           && encoding_base16_encode(data_oc[0]) == "36F0DB7A918A035B27B3479D664F45DF" );
    ok = ok && check("crypto",3, 2, crypto_cipher(data_k, data_v, data_m, data_oc[1], true,  EVP_aes_128_cbc(),     false) && encoding_base16_encode(data_oc[1]) == "C16AFD541B7899E0FE16792A3B677C05" );
    ok = ok && check("crypto",3, 3, crypto_cipher(data_k, data_v, data_m, data_oc[2], true,  EVP_aes_128_cbc(),     true)  && encoding_base16_encode(data_oc[2]) == "C16AFD541B7899E0FE16792A3B677C05F916308DCA044067C7201E025CAAC901" );
    ok = ok && check("crypto",3, 4, crypto_cipher(data_k, data_v, data_l, data_oc[3], true,  EVP_aes_128_cbc(),     true)  && encoding_base16_encode(data_oc[3]) == "EED1EB88559F51F2ED533A8FC6A4062550CFD461850DD1CC3056292DC96B3315" );
    ok = ok && check("crypto",3, 5, crypto_cipher(data_k, data_v, data_x, data_oc[4], true,  EVP_aes_128_cbc(),     true)  && encoding_base16_encode(data_oc[4]) == "0DDA033ADC84344144D15669A454817E4984CC3ABF446ADF68B81FD08C33F99F8B94F4BF5D8A9C4A81FFEFA15911259C155D71668772C3AEA71B1B1705900BE26B22858BEFF6677B9A135044FF3D13DD4F8C0FAC96088AF6E35022EFB6C43027BD0FECCA3BFB5BCF97FA10E350BE50DEBA074A950E13A47B1E0C2F54DADF5AFDA50FEB3F77241BC9F7421673EA471B01CA68051D4CF5198607C1BF422CC50A30848358EC0C16EA8906780C0DCBC3EE65AC1DFC6EECD3933BDECD39CF3B954D053C5EFCCB690B79023BD597597417FBF1692A3924216DC7084B1FF1D28678DC8C3C600FD765FA372AD8E9799E07154261F8F28D3EBF10896AF5499D26D3DEF998ADBF68F90E69612903D0C196650ECAE3D5125BD59914D2297CB20676DAFECAAAC7E037BC12D73637E2D9E9E662009DD44960AEA9ACE8A0CFCE850431E2683CD561E9F5238FB3F0CBEE329DF79643C29D717561840CF46F232E7DDF7D56A66949011320FF19FE78938D958243F32FE515D53EEB19BE90D1400D999A71D25E6F6DACC55B9413DCB3E7C4243C10634EE08A753116E5B1229A32CBC5D0A4ADF155CEB3533BE548DE8847058BAAF1F822A7D9811BBB4C7188832E4797B9B1F0D5CE88" );
    //
    // these two were only verified by self-decryption, no public decypher found
    ok = ok && check("crypto",3, 6, crypto_cipher(data_k, data_v, data_x, data_oc[5], true,  EVP_camellia_128_cbc(),true)  && encoding_base16_encode(data_oc[5]) == "E72378975879C18A1C3F92CAFDC8BB70C20834C76B1CCB53D6798030625865B82F7EFE3F732469DBD1BEF7B6FB0209E6F4D6DE4CDB748E0842AD9713E850C1A02BA0680F14473E932C9A3BC7D631FF8A4E519D097D3E5C9836500DC7D781FAFC11FF19D7EEBC17B0F46030F20B56ED6860B4EEA9E9368F143EFC4B182BB40DA78A4C28537C3AEAEB74DADD015BA3BCA4EEE65CD9C9ECE9A4C76C4CDA2DBD00F0D8F3EC590AF8B7373AC52AB44A7F4D0AEF0C730FA9CA8D094FAA5417A8F71827318D69A2E13B43BA9B41896ECA596EC1EE354AEAE9978BCEF25CAC15184532225AB6C00A10AD98CA9BF6FC9CDC9C146F27CA7C1472C33D9671B485991B0B674FB0E875993ED0D6BD7304D87EDB9CB228383C656D00F2BE9D6E4D118B45A2E832DE2BB9E95169DD9A11BEA94730FB4D3C384C95D631A7FEDA3EA26EBA525C0C839A1C127F1E5FA33AED02F7E382CD1C533B2E1D7283E1A4280F33D07CC33C09F19A86F737BE24CB108C05A7AE2B0DBB58E8F56955B494090A5585106FFE91393625E01554B8D81354D2086B5467087B7EFC6D18AE5320C5CA4BD517F6F0539ECA97A13CB4101726137F56F3FA091C786EC062CF3C3FE405DFC8289D934B774197" );
#if OPENSSL_VERSION_NUMBER < 0x30000000L
    // now a Legacy algorithm in OpenSSL v3
    ok = ok && check("crypto",3, 7, crypto_cipher(data_k, data_v, data_x, data_oc[6], true,  EVP_seed_cbc(),        true)  && encoding_base16_encode(data_oc[6]) == "41767CAD3CAAD46B7C57B7B6CF9DBD850A4EDCAAF2D9ECF7D303ADB41C1C97EDE0BC185EB134E3B33DB1DCB4593042F74E1F2D2D9A9BF97C9562B67313439DA60E1F9E3F3D1C0AB16B2CEA6C0519FC863B9F43FC7F8F945B3F377C93CE55E7CA90CBA7B4B854E247996361A5FE8BD54171E25E3AFB20481A986B77C6A5FBC945EDC7D227F5B4F96A87468CD07FB2B12B52F4048DBE434CA0891C6952869DEBC636853ADA271B0168A9B970D742D7356C7BA9DAD7CCF4B09676FF7589E25C85CD98E0D657CE465D094BE6EE13E9E16198EE4B5F84BFD36B7C5C49BC7C0E8A230FE87396786D14A5B35A626EE3ED51EC3EC184EC947D75B5A6D28402514FB3B7FD1F33B472A66070488BC0EA01F9D58BA487CE6251F5BD69C420E6EEDA737362570BC7A20DF6F343ABF4CC69FB01C99B1C96205F28CDDF74F776F0AE48FC7D5780A7C649D05D50BEDE5CBDB905C4BE3693DB2B5904BEF84FFFDBE45C315ADFEA4D01A469F52B69DBC09CD4E7C05FB557C07697A5D7A8EEFD37AE60853126CB6A493EC1A9C5DDC442D5652F5E47080B18095FFBB743BF8545717E6933D26AE799189CBCA1B3B2911649ED61F4AF4D88DD14D40929FDC221519823D00B50F7ACA1D1" );
#endif
    //
    ok = ok && check("crypto",4, 1, crypto_cipher(data_k, data_v, data_oc[0], data_o, false, EVP_aes_128_cbc(),     true)  && data_o == data_s);
    ok = ok && check("crypto",4, 2, crypto_cipher(data_k, data_v, data_oc[1], data_o, false, EVP_aes_128_cbc(),     false) && data_o == data_m);
    ok = ok && check("crypto",4, 3, crypto_cipher(data_k, data_v, data_oc[2], data_o, false, EVP_aes_128_cbc(),     true)  && data_o == data_m);
    ok = ok && check("crypto",4, 4, crypto_cipher(data_k, data_v, data_oc[3], data_o, false, EVP_aes_128_cbc(),     true)  && data_o == data_l);
    ok = ok && check("crypto",4, 5, crypto_cipher(data_k, data_v, data_oc[4], data_o, false, EVP_aes_128_cbc(),     true)  && data_o == data_x);
    //
    // RFC test vector:
    // https://tools.ietf.org/html/rfc4868#section-2.7.1
    ok = ok && check("crypto",5, 1, crypto_hmac( Data({0xb,0xb,0xb,0xb,0xb,0xb,0xb,0xb,0xb,0xb,0xb,0xb,0xb,0xb,0xb,0xb,0xb,0xb,0xb,0xb}),
                                                 Data({0x48,0x69,0x20,0x54,0x68,0x65,0x72,0x65}),
                                                 data_o, EVP_sha256() ) &&
                                    encoding_base16_encode(data_o) == "B0344C61D8DB38535CA8AFCEAF0BF12B881DC200C9833DA726E9376C2E32CFF7" );
    //
    // https://tools.ietf.org/html/rfc6070
    // https://stackoverflow.com/questions/5130513/pbkdf2-hmac-sha2-test-vectors/5136918
    ok = ok && check("crypto",6, 1, crypto_pbkdf2(Data({0x70,0x61,0x73,0x73,0x00,0x77,0x6f,0x72,0x64}), Data({0x73,0x61,0x00,0x6c,0x74}), data_o, 4096, 16, EVP_sha1()) &&
                                    encoding_base16_encode(data_o) == "56FA6AA75548099DCC37D7F03425E0C3" );
    ok = ok && check("crypto",6, 2, crypto_pbkdf2(Data({0x70,0x61,0x73,0x73,0x77,0x6f,0x72,0x64}), Data({0x73,0x61,0x6c,0x74}), data_o, 4096, 20, EVP_sha1()) &&
                                    encoding_base16_encode(data_o) == "4B007901B765489ABEAD49D926F721D065A429C1" );
    ok = ok && check("crypto",6, 3, crypto_pbkdf2(Data({0x70,0x61,0x73,0x73,0x77,0x6f,0x72,0x64}), Data({0x73,0x61,0x6c,0x74}), data_o, 4096, 32) &&
                                    encoding_base16_encode(data_o) == "C5E478D59288C841AA530DB6845C4C8D962893A001CE4E11A4963873AA98134A" );
    ok = ok && check("crypto",6, 4, crypto_pbkdf2(data_m, data_k, data_o, 10'000, 32) &&
                                    encoding_base16_encode(data_o) == "EE711DC8777F67D835F34BA198F853CABA6AA563FD3EED782C36AF3AB7DC161F" );
    ok = ok && check("crypto",6, 5, crypto_pbkdf2(data_m, data_k, data_o, 10'0000, 32) &&
                                    encoding_base16_encode(data_o) == "BC53C762D72B5F3E572C058F0F51F040CBAA5BB650432760F9E2318BD732590A" );
    //
    ok = ok && check("crypto",7, 1,   crypto_equal (data_k, data_k) );
    ok = ok && check("crypto",7, 2, ! crypto_equal (data_k, data_v) );
    ok = ok && check("crypto",7, 3, ! crypto_equal (data_k, data_s) );
    ok = ok && check("crypto",7, 4,   crypto_equal ("1234", "1234") );
    ok = ok && check("crypto",7, 5, ! crypto_equal ("1234", "1235") );
    ok = ok && check("crypto",7, 6, ! crypto_equal ("1234", "123" ) );
    //
    std::set< std::string > values;
    std::string             value;
    //
    values.clear();
    for ( unsigned n = 0; n < 1000; n++ ) {
        crypto_random_prng( 512, data_o );
        value = data_o.str();
        //
        ok = ok && check("crypto",8, 1, values.count( value ) == 0 );
        values.insert( value );
    }
    //
    values.clear();
    for ( unsigned n = 0; n < 1000; n++ ) {
        crypto_random_prng( 5, data_o );
        value = data_o.str();
        //
        ok = ok && check("crypto",8, 2, values.count( value ) == 0 );
        values.insert( value );
    }
    //
    values.clear();
    for ( unsigned n = 0; n < 1000; n++ ) {
        ok = ok && check("crypto",8, 3, crypto_random( 512, data_o, false) );
        value = data_o.str();
        //
        ok = ok && check("crypto",8, 5, values.count( value ) == 0 );
        values.insert( value );
    }
    //
    values.clear();
    for ( unsigned n = 0; n < 1000; n++ ) {
        ok = ok && check("crypto",8, 5, crypto_random( 4, data_o, false) );
        value = data_o.str();
        //
        ok = ok && check("crypto",8, 6, values.count( value ) == 0 );
        values.insert( value );
    }
    //
    ok = ok && check("crypto",8, 97, ! crypto_random( 512, data_o, true) ); // max allowed size is 256 with true
    //
    const struct { uint64_t c; unsigned n; std::string h; } HOTP_TV[] = {
        // HOTP test vectors
        {                   0, 6,   "755224" },
        {                   1, 6,   "287082" },
        {                   2, 6,   "359152" },
        {                   3, 6,   "969429" },
        {                   4, 6,   "338314" },
        {                   5, 6,   "254676" },
        {                   6, 6,   "287922" },
        {                   7, 6,   "162583" },
        {                   8, 6,   "399871" },
        {                   9, 6,   "520489" },
        // TOTP test vectors
        {             59 / 30, 8, "94287082" },
        {  1'111'111'109 / 30, 8, "07081804" },
        {  1'111'111'111 / 30, 8, "14050471" },
        {  1'234'567'890 / 30, 8, "89005924" },
        {  2'000'000'000 / 30, 8, "69279037" },
        { 20'000'000'000 / 30, 8, "65353130" }
    };
    //
    Data         otp_k("12345678901234567890");
    std::string  otp;
    //
    for ( const auto & test: HOTP_TV )
        ok = ok && check("crypto", 9, 1, crypto_hotp( otp_k, test.c, test.n, otp ) && otp == test.h );
    //
    for ( unsigned u = 1; u<=9 ; u ++ )
        ok = ok && check("crypto", 9, 2, crypto_totp( otp_k, u, otp ) && otp.length() == u );
    //
    ok = ok && check("crypto", 9, 3, ! crypto_hotp( otp_k, 1, 11, otp ) );   // invalid size
    ok = ok && check("crypto", 9, 4, ! crypto_totp( otp_k, 6, otp, 0 ) );    // invalid steps
    ok = ok && check("crypto", 9, 5, ! crypto_totp( otp_k, 6, otp, 30, time(nullptr) + 5 ) );  // invalid t0
    ok = ok && check("crypto", 9, 6, ! crypto_totp( otp_k, 6, otp, 30, 9223372036854775807) ); // invalid t0
    //
    Data  key_private, key_public, key;
    std::vector< std::tuple< std::string, std::string, size_t, size_t  >> algos = {
        // algorithm      parameters    min   max
        { "secp112r1"   , ""          , 33  , 36  },
        { "sect163r2"   , ""          , 46  , 48  },
        { "prime192v1"  , ""          , 53  , 56  },
        { "secp224r1"   , ""          , 61  , 64  },
        { "prime256v1"  , ""          , 69  , 72  },
        { "secp384r1"   , ""          , 101 , 104 },
        { "secp521r1"   , ""          , 137 , 139 },
        { "ed25519"     , ""          , 64  , 64  },
        { "ed448"       , ""          , 114 , 114 },
        { "rsa"         , ""          , 256 , 256 },
        { "rsa"         , "bits=1024" , 128 , 128 },
        { "rsa"         , "bits=4096" , 512 , 512 },
        { "rsa_pss"     , ""          , 256 , 256 },
        { "rsa_pss"     , "bits=1024" , 128 , 128 },
        { "rsa_pss"     , "bits=4096" , 512 , 512 }
    };
    //
    if ( ok )
        for ( const auto & [ algo, params, sig_len_min, sig_len_max ]: algos )
        {
            ok = ok && check("crypto", 10, 1,
                crypto_pkey_generate( algo, params, key_private, key_public ) &&
                crypto_pkey_sign( key_private, data_x, data_o )               &&
                crypto_pkey_verify( key_public, data_x, data_o )              &&
                data_o.size() >= sig_len_min                                  &&
                data_o.size() <= sig_len_max );
            //
            if ( ! ok ) {
                printf( " %s: sig length %zu\n", algo.c_str(), data_o.size() );
                break;
            }
        }
    //
    // echo -n "FEDCBA9876543210" > message.txt                 # same as data_v
    // openssl ecparam -genkey -name secp112r1 -noout -out private_secp112r1.pem
    // openssl ec -in private_secp112r1.pem  -pubout -out public_secp112r1.pem
    // openssl dgst -sha256 -sign private_secp112r1.pem < message.txt > signature_secp112r1.bin
    // cat public_secp112r1.pem                                 # key_public
    // hexdump -C signature_secp112r1.bin                       # in data_o
    data_o = Data( { 0x30, 0x20, 0x02, 0x0e, 0x42, 0x38, 0xf9, 0x85, 0xdc, 0x4f, 0x0b, 0x27, 0x84, 0x43, 0xfb, 0xc2,
                     0x7a, 0xb6, 0x02, 0x0e, 0x5e, 0x1d, 0x48, 0xb1, 0x05, 0x00, 0xca, 0x26, 0x0a, 0x45, 0x08, 0xf3,
                     0xa3, 0x29 } ); // signature generated by openssl for data_v, public key b64 below
    encoding_base64_decode( "MDIwEAYHKoZIzj0CAQYFK4EEAAYDHgAEIUgiNMQ2DTR2tbZ4q/hERu117cgAntXAIeO6KQ==", key_public );
    //
    ok = ok && check("crypto", 10, 2, crypto_pkey_verify( key_public, data_v, data_o ) );
    //
    std::string pem;
    bool        success, is_private;
    //
    ok = ok && check("crypto", 10, 3, crypto_pkey_to_PEM  ( key_public, pem ) );
    std::tie( success, is_private ) = crypto_pkey_from_PEM( key, pem );
    ok = ok && check("crypto", 10, 4, success && ! is_private && key == key_public );
    //
    ok = ok && check("crypto", 10, 5, ! crypto_pkey_generate( "rsa",     "z=3", key_private, key_public ) );
    ok = ok && check("crypto", 10, 6, ! crypto_pkey_generate( "rsa_pss", "z=3", key_private, key_public ) );
    ok = ok && check("crypto", 10, 7, ! crypto_pkey_generate( "zzz",     "",    key_private, key_public ) );
    ok = ok && check("crypto", 10, 8, ! crypto_pkey_to_PEM  ( data_s, pem ) );
    std::tie( success, is_private ) = crypto_pkey_from_PEM( key_private, "zzz" );
    ok = ok && check("crypto", 10, 9, ! success );
    ok = ok && check("crypto", 10, 10, ! crypto_pkey_sign( data_s, data_x, data_o ) );
    ok = ok && check("crypto", 10, 11, ! crypto_pkey_verify( data_s, data_x, data_o ) );
    //
    for ( const auto & [ algo, params, sig_len_min, sig_len_max ]: algos )
    {
        ok = ok && check("crypto", 11, 1, crypto_pkey_generate( algo, params, key_private, key_public ) );
        //
        ok = ok && check("crypto", 11, 2, crypto_pkey_to_PEM  ( key_public, pem ) );
        std::tie( success, is_private ) = crypto_pkey_from_PEM( key, pem );
        ok = ok && check("crypto", 11, 3, success && ! is_private && key == key_public);
        //
        ok = ok && check("crypto", 11, 4, crypto_pkey_to_PEM  ( key_private, pem ) );
        std::tie( success, is_private ) = crypto_pkey_from_PEM( key, pem );
        ok = ok && check("crypto", 11, 5, success && is_private && key == key_private);
        //
        if ( ! ok )
            break;
    }
    //
    ok = ok && check("crypto", 12, 1, ! crypto_last_errors().empty() );
    ERR_clear_error();
    ok = ok && check("crypto", 12, 2, crypto_last_errors().empty() );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_encoding ( void )
{
    bool        ok = true;
    //
    Data  data_s = { 0x01, 0xFF };  // lower than 3 and 5
    Data  data_m = { 0x01, 0xFF, 0x03, 0xFE, 0x05, 0xFD, 0x07, 0xFC, 0x08, 0xFB };  // multiple of 5, not 3
    Data  data_l = { 0x01, 0xFF, 0x03, 0xFE, 0x05, 0xFD, 0x07, 0xFC, 0x08, 0xFB, 0x09, 0xFA };  // multiple of 3, not 5
    Data  data_x = { 0x01, 0xFF, 0x03, 0xFE, 0x05, 0xFD, 0x07, 0xFC, 0x08, 0xFB, 0x09, 0xFA, 0x07, 0xF9, 0x08, 0xF8, 0x07 };  // neither multiple of 3, not 5, odd
    Data  data_o;
    //
    // Tested against http://tomeko.net/online_tools/hex_to_base32.php?lang=en
    //
    ok = ok && check("encoding",1,  1, encoding_base16_encode(data_s)                 == "01FF" );
    ok = ok && check("encoding",1,  2, encoding_base16_encode(data_m)                 == "01FF03FE05FD07FC08FB" );
    ok = ok && check("encoding",1,  3, encoding_base16_encode(data_l)                 == "01FF03FE05FD07FC08FB09FA" );
    ok = ok && check("encoding",1,  4, encoding_base16_encode(data_x)                 == "01FF03FE05FD07FC08FB09FA07F908F807" );
    //
    ok = ok && check("encoding",1, 11, encoding_base16_encode(data_s, true)           == "01ff" );
    ok = ok && check("encoding",1, 12, encoding_base16_encode(data_m, true)           == "01ff03fe05fd07fc08fb" );
    ok = ok && check("encoding",1, 13, encoding_base16_encode(data_l, true)           == "01ff03fe05fd07fc08fb09fa" );
    ok = ok && check("encoding",1, 14, encoding_base16_encode(data_x, true)           == "01ff03fe05fd07fc08fb09fa07f908f807" );
    //
    ok = ok && check("encoding",2,  1, encoding_base32_encode(data_s, false, false)   == "AH7Q" );
    ok = ok && check("encoding",2,  2, encoding_base32_encode(data_m, false, false)   == "AH7QH7QF7UD7YCH3" );
    ok = ok && check("encoding",2,  3, encoding_base32_encode(data_l, false, false)   == "AH7QH7QF7UD7YCH3BH5A" );
    ok = ok && check("encoding",2,  4, encoding_base32_encode(data_x, false, false)   == "AH7QH7QF7UD7YCH3BH5AP6II7ADQ" );
    ok = ok && check("encoding",2,  5, encoding_base32_encode(data_s, false, true )   == "AH7Q====" );
    ok = ok && check("encoding",2,  6, encoding_base32_encode(data_m, false, true )   == "AH7QH7QF7UD7YCH3" );
    ok = ok && check("encoding",2,  7, encoding_base32_encode(data_l, false, true )   == "AH7QH7QF7UD7YCH3BH5A====" );
    ok = ok && check("encoding",2,  8, encoding_base32_encode(data_x, false, true )   == "AH7QH7QF7UD7YCH3BH5AP6II7ADQ====" );
    ok = ok && check("encoding",2,  9, encoding_base32_encode(data_s, true,  false)   == "07VG" );
    ok = ok && check("encoding",2, 10, encoding_base32_encode(data_m, true,  false)   == "07VG7VG5VK3VO27R" );
    ok = ok && check("encoding",2, 11, encoding_base32_encode(data_l, true,  false)   == "07VG7VG5VK3VO27R17T0" );
    ok = ok && check("encoding",2, 12, encoding_base32_encode(data_x, true,  false)   == "07VG7VG5VK3VO27R17T0FU88V03G" );
    ok = ok && check("encoding",2, 13, encoding_base32_encode(data_s, true,  true )   == "07VG====" );
    ok = ok && check("encoding",2, 14, encoding_base32_encode(data_m, true,  true )   == "07VG7VG5VK3VO27R" );
    ok = ok && check("encoding",2, 15, encoding_base32_encode(data_l, true,  true )   == "07VG7VG5VK3VO27R17T0====" );
    ok = ok && check("encoding",2, 16, encoding_base32_encode(data_x, true,  true )   == "07VG7VG5VK3VO27R17T0FU88V03G====" );
    //
    ok = ok && check("encoding",3,  1, encoding_base64_encode(data_s, base64_type::standard, false)   == "Af8");
    ok = ok && check("encoding",3,  2, encoding_base64_encode(data_m, base64_type::standard, false)   == "Af8D/gX9B/wI+w");
    ok = ok && check("encoding",3,  3, encoding_base64_encode(data_l, base64_type::standard, false)   == "Af8D/gX9B/wI+wn6");
    ok = ok && check("encoding",3,  4, encoding_base64_encode(data_x, base64_type::standard, false)   == "Af8D/gX9B/wI+wn6B/kI+Ac");
    ok = ok && check("encoding",3,  5, encoding_base64_encode(data_s, base64_type::standard, true)    == "Af8=");
    ok = ok && check("encoding",3,  6, encoding_base64_encode(data_m, base64_type::standard, true)    == "Af8D/gX9B/wI+w==");
    ok = ok && check("encoding",3,  7, encoding_base64_encode(data_l, base64_type::standard, true)    == "Af8D/gX9B/wI+wn6");
    ok = ok && check("encoding",3,  8, encoding_base64_encode(data_x, base64_type::standard, true)    == "Af8D/gX9B/wI+wn6B/kI+Ac=");
    ok = ok && check("encoding",3,  9, encoding_base64_encode(data_s, base64_type::url,  false)   == "Af8");
    ok = ok && check("encoding",3, 10, encoding_base64_encode(data_m, base64_type::url,  false)   == "Af8D_gX9B_wI-w");
    ok = ok && check("encoding",3, 11, encoding_base64_encode(data_l, base64_type::url,  false)   == "Af8D_gX9B_wI-wn6");
    ok = ok && check("encoding",3, 12, encoding_base64_encode(data_x, base64_type::url,  false)   == "Af8D_gX9B_wI-wn6B_kI-Ac");
    ok = ok && check("encoding",3, 13, encoding_base64_encode(data_s, base64_type::url,  true)    == "Af8=");
    ok = ok && check("encoding",3, 14, encoding_base64_encode(data_m, base64_type::url,  true)    == "Af8D_gX9B_wI-w==");
    ok = ok && check("encoding",3, 15, encoding_base64_encode(data_l, base64_type::url,  true)    == "Af8D_gX9B_wI-wn6");
    ok = ok && check("encoding",3, 16, encoding_base64_encode(data_x, base64_type::url,  true)    == "Af8D_gX9B_wI-wn6B_kI-Ac=");
    ok = ok && check("encoding",3, 17, encoding_base64_encode(data_s, base64_type::xxencoding,  false)   == "+Tw");
    ok = ok && check("encoding",3, 18, encoding_base64_encode(data_x, base64_type::xxencoding,  false)   == "+Tw1zULx-zk6ykbu-zY6y+Q");
    //
    ok = ok && check("encoding",4, 1, encoding_base16_decode("01FF",                                data_o) && data_o == data_s);
    ok = ok && check("encoding",4, 2, encoding_base16_decode("01FF03FE05FD07FC08FB",                data_o) && data_o == data_m);
    ok = ok && check("encoding",4, 3, encoding_base16_decode("01FF03FE05FD07FC08FB09FA",            data_o) && data_o == data_l);
    ok = ok && check("encoding",4, 4, encoding_base16_decode("01FF03FE05FD07FC08FB09FA07F908F807",  data_o) && data_o == data_x);
    ok = ok && check("encoding",4, 5, encoding_base16_decode("01ff03fe05fd07fc08fb09fa07f908f807",  data_o) && data_o == data_x);
    ok = ok && check("encoding",4, 6, ! encoding_base16_decode("0", data_o) );
    ok = ok && check("encoding",4, 7, ! encoding_base16_decode("Z0", data_o) );
    ok = ok && check("encoding",4, 8, ! encoding_base16_decode("0Z", data_o) );
    //
    ok = ok && check("encoding",5,  1, encoding_base32_decode("AH7Q",                               data_o, false) && data_o == data_s);
    ok = ok && check("encoding",5,  2, encoding_base32_decode("AH7QH7QF7UD7YCH3",                   data_o, false) && data_o == data_m);
    ok = ok && check("encoding",5,  3, encoding_base32_decode("AH7QH7QF7UD7YCH3BH5A",               data_o, false) && data_o == data_l);
    ok = ok && check("encoding",5,  4, encoding_base32_decode("AH7QH7QF7UD7YCH3BH5AP6II7ADQ",       data_o, false) && data_o == data_x);
    ok = ok && check("encoding",5,  5, encoding_base32_decode("AH7Q====",                           data_o, false) && data_o == data_s);
    ok = ok && check("encoding",5,  6, encoding_base32_decode("AH7QH7QF7UD7YCH3",                   data_o, false) && data_o == data_m);
    ok = ok && check("encoding",5,  7, encoding_base32_decode("AH7QH7QF7UD7YCH3BH5A====",           data_o, false) && data_o == data_l);
    ok = ok && check("encoding",5,  8, encoding_base32_decode("AH7QH7QF7UD7YCH3BH5AP6II7ADQ====",   data_o, false) && data_o == data_x);
    ok = ok && check("encoding",5,  9, encoding_base32_decode("07VG",                               data_o, true ) && data_o == data_s);
    ok = ok && check("encoding",5, 10, encoding_base32_decode("07VG7VG5VK3VO27R",                   data_o, true ) && data_o == data_m);
    ok = ok && check("encoding",5, 11, encoding_base32_decode("07VG7VG5VK3VO27R17T0",               data_o, true ) && data_o == data_l);
    ok = ok && check("encoding",5, 12, encoding_base32_decode("07VG7VG5VK3VO27R17T0FU88V03G",       data_o, true ) && data_o == data_x);
    ok = ok && check("encoding",5, 13, encoding_base32_decode("07VG====",                           data_o, true ) && data_o == data_s);
    ok = ok && check("encoding",5, 14, encoding_base32_decode("07VG7VG5VK3VO27R",                   data_o, true ) && data_o == data_m);
    ok = ok && check("encoding",5, 15, encoding_base32_decode("07VG7VG5VK3VO27R17T0====",           data_o, true ) && data_o == data_l);
    ok = ok && check("encoding",5, 16, encoding_base32_decode("07VG7VG5VK3VO27R17T0FU88V03G====",   data_o, true ) && data_o == data_x);
    ok = ok && check("encoding",5, 17, encoding_base32_decode("07vg7vg5vk3vo27r17t0fu88v03g====",   data_o, true ) && data_o == data_x);
    ok = ok && check("encoding",5, 18, ! encoding_base32_decode("!", data_o, true ) );
    ok = ok && check("encoding",5, 19, ! encoding_base32_decode("!", data_o, false ) );
    ok = ok && check("encoding",5, 20, ! encoding_base32_decode("07VG7VG!VK3VO27R", data_o, true ) );
    ok = ok && check("encoding",5, 21, ! encoding_base32_decode("07VG7VG!VK3VO27R", data_o, false ) );
    //
    ok = ok && check("encoding",6,  1, encoding_base64_decode("Af8=",                               data_o, base64_type::standard) && data_o == data_s);
    ok = ok && check("encoding",6,  2, encoding_base64_decode("Af8D/gX9B/wI+w==",                   data_o, base64_type::standard) && data_o == data_m);
    ok = ok && check("encoding",6,  3, encoding_base64_decode("Af8D/gX9B/wI+wn6",                   data_o, base64_type::standard) && data_o == data_l);
    ok = ok && check("encoding",6,  4, encoding_base64_decode("Af8D/gX9B/wI+wn6B/kI+Ac=",           data_o, base64_type::standard) && data_o == data_x);
    ok = ok && check("encoding",6,  5, encoding_base64_decode("Af8",                                data_o, base64_type::standard) && data_o == data_s);
    ok = ok && check("encoding",6,  6, encoding_base64_decode("Af8D/gX9B/wI+w",                     data_o, base64_type::standard) && data_o == data_m);
    ok = ok && check("encoding",6,  7, encoding_base64_decode("Af8D/gX9B/wI+wn6",                   data_o, base64_type::standard) && data_o == data_l);
    ok = ok && check("encoding",6,  8, encoding_base64_decode("Af8D/gX9B/wI+wn6B/kI+Ac",            data_o, base64_type::standard) && data_o == data_x);
    ok = ok && check("encoding",6,  9, encoding_base64_decode("Af8=",                               data_o, base64_type::url ) && data_o == data_s);
    ok = ok && check("encoding",6, 10, encoding_base64_decode("Af8D_gX9B_wI-w==",                   data_o, base64_type::url ) && data_o == data_m);
    ok = ok && check("encoding",6, 11, encoding_base64_decode("Af8D_gX9B_wI-wn6",                   data_o, base64_type::url ) && data_o == data_l);
    ok = ok && check("encoding",6, 12, encoding_base64_decode("Af8D_gX9B_wI-wn6B_kI-Ac=",           data_o, base64_type::url ) && data_o == data_x);
    ok = ok && check("encoding",6, 13, encoding_base64_decode("Af8",                                data_o, base64_type::url ) && data_o == data_s);
    ok = ok && check("encoding",6, 14, encoding_base64_decode("Af8D_gX9B_wI-w",                     data_o, base64_type::url ) && data_o == data_m);
    ok = ok && check("encoding",6, 15, encoding_base64_decode("Af8D_gX9B_wI-wn6",                   data_o, base64_type::url ) && data_o == data_l);
    ok = ok && check("encoding",6, 16, encoding_base64_decode("Af8D_gX9B_wI-wn6B_kI-Ac",            data_o, base64_type::url ) && data_o == data_x);
    ok = ok && check("encoding",6, 17, encoding_base64_decode("+Tw",                                data_o, base64_type::xxencoding ) && data_o == data_s);
    ok = ok && check("encoding",6, 18, encoding_base64_decode("+Tw1zULx-zk6ykbu-zY6y+Q",            data_o, base64_type::xxencoding ) && data_o == data_x);
    ok = ok && check("encoding",6, 19, ! encoding_base64_decode("!", data_o, base64_type::standard ) );
    ok = ok && check("encoding",6, 20, ! encoding_base64_decode("!", data_o, base64_type::url ) );
    ok = ok && check("encoding",6, 21, ! encoding_base64_decode("!", data_o, base64_type::xxencoding ) );
    ok = ok && check("encoding",6, 22, ! encoding_base64_decode("Af8D_gX9!_wI-wn6", data_o, base64_type::standard ) );
    ok = ok && check("encoding",6, 23, ! encoding_base64_decode("Af8D_gX9!_wI-wn6", data_o, base64_type::url ) );
    ok = ok && check("encoding",6, 24, ! encoding_base64_decode("Af8D_gX9!_wI-wn6", data_o, base64_type::xxencoding ) );
    //
    // RFC test vectors https://tools.ietf.org/html/rfc4648#section-10
    //
    ok = ok && check("encoding",7,  1, encoding_base64_encode(Data({}))                                == "");
    ok = ok && check("encoding",7,  2, encoding_base64_encode(Data({'f'}))                             == "Zg==");
    ok = ok && check("encoding",7,  3, encoding_base64_encode(Data({'f','o'}))                         == "Zm8=");
    ok = ok && check("encoding",7,  4, encoding_base64_encode(Data({'f','o','o'}))                     == "Zm9v");
    ok = ok && check("encoding",7,  5, encoding_base64_encode(Data({'f','o','o','b'}))                 == "Zm9vYg==");
    ok = ok && check("encoding",7,  6, encoding_base64_encode(Data({'f','o','o','b','a'}))             == "Zm9vYmE=");
    ok = ok && check("encoding",7,  7, encoding_base64_encode(Data({'f','o','o','b','a','r'}))         == "Zm9vYmFy");
    //
    ok = ok && check("encoding",8,  1, encoding_base32_encode(Data({}),                        false)  == "");
    ok = ok && check("encoding",8,  2, encoding_base32_encode(Data({'f'}),                     false)  == "MY======");
    ok = ok && check("encoding",8,  3, encoding_base32_encode(Data({'f','o'}),                 false)  == "MZXQ====");
    ok = ok && check("encoding",8,  4, encoding_base32_encode(Data({'f','o','o'}),             false)  == "MZXW6===");
    ok = ok && check("encoding",8,  5, encoding_base32_encode(Data({'f','o','o','b'}),         false)  == "MZXW6YQ=");
    ok = ok && check("encoding",8,  6, encoding_base32_encode(Data({'f','o','o','b','a'}),     false)  == "MZXW6YTB");
    ok = ok && check("encoding",8,  7, encoding_base32_encode(Data({'f','o','o','b','a','r'}), false)  == "MZXW6YTBOI======");
    ok = ok && check("encoding",8,  8, encoding_base32_encode(Data({}),                        true)   == "");
    ok = ok && check("encoding",8,  9, encoding_base32_encode(Data({'f'}),                     true)   == "CO======");
    ok = ok && check("encoding",8, 10, encoding_base32_encode(Data({'f','o'}),                 true)   == "CPNG====");
    ok = ok && check("encoding",8, 11, encoding_base32_encode(Data({'f','o','o'}),             true)   == "CPNMU===");
    ok = ok && check("encoding",8, 12, encoding_base32_encode(Data({'f','o','o','b'}),         true)   == "CPNMUOG=");
    ok = ok && check("encoding",8, 13, encoding_base32_encode(Data({'f','o','o','b','a'}),     true)   == "CPNMUOJ1");
    ok = ok && check("encoding",8, 14, encoding_base32_encode(Data({'f','o','o','b','a','r'}), true)   == "CPNMUOJ1E8======");
    //
    ok = ok && check("encoding",9,  1, encoding_base16_encode(Data({}))                                == "");
    ok = ok && check("encoding",9,  2, encoding_base16_encode(Data({'f'}))                             == "66");
    ok = ok && check("encoding",9,  3, encoding_base16_encode(Data({'f','o'}))                         == "666F");
    ok = ok && check("encoding",9,  4, encoding_base16_encode(Data({'f','o','o'}))                     == "666F6F");
    ok = ok && check("encoding",9,  5, encoding_base16_encode(Data({'f','o','o','b'}))                 == "666F6F62");
    ok = ok && check("encoding",9,  6, encoding_base16_encode(Data({'f','o','o','b','a'}))             == "666F6F6261");
    ok = ok && check("encoding",9,  7, encoding_base16_encode(Data({'f','o','o','b','a','r'}))         == "666F6F626172");
    //
    ok = ok && check("encoding",10,  1, encoding_base64_decode(""        ,         data_o, base64_type::standard) && data_o == Data({}))                       ;
    ok = ok && check("encoding",10,  2, encoding_base64_decode("Zg=="    ,         data_o, base64_type::standard) && data_o == Data({'f'}))                    ;
    ok = ok && check("encoding",10,  3, encoding_base64_decode("Zm8="    ,         data_o, base64_type::standard) && data_o == Data({'f','o'}))                ;
    ok = ok && check("encoding",10,  4, encoding_base64_decode("Zm9v"    ,         data_o, base64_type::standard) && data_o == Data({'f','o','o'}))            ;
    ok = ok && check("encoding",10,  5, encoding_base64_decode("Zm9vYg==",         data_o, base64_type::standard) && data_o == Data({'f','o','o','b'}))        ;
    ok = ok && check("encoding",10,  6, encoding_base64_decode("Zm9vYmE=",         data_o, base64_type::standard) && data_o == Data({'f','o','o','b','a'}))    ;
    ok = ok && check("encoding",10,  7, encoding_base64_decode("Zm9vYmFy",         data_o, base64_type::standard) && data_o == Data({'f','o','o','b','a','r'}));
    //
    ok = ok && check("encoding",11,  1, encoding_base32_decode(""                , data_o, false) && data_o == Data({}))                       ;
    ok = ok && check("encoding",11,  2, encoding_base32_decode("MY======"        , data_o, false) && data_o == Data({'f'}))                    ;
    ok = ok && check("encoding",11,  3, encoding_base32_decode("MZXQ===="        , data_o, false) && data_o == Data({'f','o'}))                ;
    ok = ok && check("encoding",11,  4, encoding_base32_decode("MZXW6==="        , data_o, false) && data_o == Data({'f','o','o'}))            ;
    ok = ok && check("encoding",11,  5, encoding_base32_decode("MZXW6YQ="        , data_o, false) && data_o == Data({'f','o','o','b'}))        ;
    ok = ok && check("encoding",11,  6, encoding_base32_decode("MZXW6YTB"        , data_o, false) && data_o == Data({'f','o','o','b','a'}))    ;
    ok = ok && check("encoding",11,  7, encoding_base32_decode("MZXW6YTBOI======", data_o, false) && data_o == Data({'f','o','o','b','a','r'}));
    ok = ok && check("encoding",11,  8, encoding_base32_decode(""                , data_o, true ) && data_o == Data({}))                       ;
    ok = ok && check("encoding",11,  9, encoding_base32_decode("CO======"        , data_o, true ) && data_o == Data({'f'}))                    ;
    ok = ok && check("encoding",11, 10, encoding_base32_decode("CPNG===="        , data_o, true ) && data_o == Data({'f','o'}))                ;
    ok = ok && check("encoding",11, 11, encoding_base32_decode("CPNMU==="        , data_o, true ) && data_o == Data({'f','o','o'}))            ;
    ok = ok && check("encoding",11, 12, encoding_base32_decode("CPNMUOG="        , data_o, true ) && data_o == Data({'f','o','o','b'}))        ;
    ok = ok && check("encoding",11, 13, encoding_base32_decode("CPNMUOJ1"        , data_o, true ) && data_o == Data({'f','o','o','b','a'}))    ;
    ok = ok && check("encoding",11, 14, encoding_base32_decode("CPNMUOJ1E8======", data_o, true ) && data_o == Data({'f','o','o','b','a','r'}));
    //
    ok = ok && check("encoding",12,  1, encoding_base16_decode(""            ,     data_o) && data_o == Data({}))                       ;
    ok = ok && check("encoding",12,  2, encoding_base16_decode("66"          ,     data_o) && data_o == Data({'f'}))                    ;
    ok = ok && check("encoding",12,  3, encoding_base16_decode("666F"        ,     data_o) && data_o == Data({'f','o'}))                ;
    ok = ok && check("encoding",12,  4, encoding_base16_decode("666F6F"      ,     data_o) && data_o == Data({'f','o','o'}))            ;
    ok = ok && check("encoding",12,  5, encoding_base16_decode("666F6F62"    ,     data_o) && data_o == Data({'f','o','o','b'}))        ;
    ok = ok && check("encoding",12,  6, encoding_base16_decode("666F6F6261"  ,     data_o) && data_o == Data({'f','o','o','b','a'}))    ;
    ok = ok && check("encoding",12,  7, encoding_base16_decode("666F6F626172",     data_o) && data_o == Data({'f','o','o','b','a','r'}));
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_string ( void )
{
    bool             ok = true;
    std::string      s, t;
    std::string_view sv, tv;
    //
    //-------
    s = "";        str_lower(s); ok = ok && check("string",1,1, s == ""   );
    s = "ABC";     str_lower(s); ok = ok && check("string",1,2, s == "abc");
    s = "abc";     str_lower(s); ok = ok && check("string",1,3, s == "abc");
    //
    s = "";        str_upper(s); ok = ok && check("string",2,1, s == ""   );
    s = "ABC";     str_upper(s); ok = ok && check("string",2,2, s == "ABC");
    s = "abc";     str_upper(s); ok = ok && check("string",2,3, s == "ABC");
    //
    //-------
    s = "";        str_ltrim(s); ok = ok && check("string",3,1, s == ""      );
    s = " abcd";   str_ltrim(s); ok = ok && check("string",3,2, s == "abcd"  );
    s = "abcd ";   str_ltrim(s); ok = ok && check("string",3,3, s == "abcd " );
    s = " abcd ";  str_ltrim(s); ok = ok && check("string",3,4, s == "abcd " );
    s = " ab cd "; str_ltrim(s); ok = ok && check("string",3,5, s == "ab cd ");
    s = " ";       str_ltrim(s); ok = ok && check("string",3,6, s == ""      );
    //
    //-------
    s = "";        str_rtrim(s); ok = ok && check("string",4,1, s == ""      );
    s = " abcd";   str_rtrim(s); ok = ok && check("string",4,2, s == " abcd" );
    s = "abcd ";   str_rtrim(s); ok = ok && check("string",4,3, s == "abcd"  );
    s = " abcd ";  str_rtrim(s); ok = ok && check("string",4,4, s == " abcd" );
    s = " ab cd "; str_rtrim(s); ok = ok && check("string",4,5, s == " ab cd");
    s = " ";       str_rtrim(s); ok = ok && check("string",4,6, s == ""      );
    //
    //-------
    s = "";        str_trim(s);  ok = ok && check("string",5,1, s == ""      );
    s = " abcd";   str_trim(s);  ok = ok && check("string",5,2, s == "abcd"  );
    s = "abcd ";   str_trim(s);  ok = ok && check("string",5,3, s == "abcd"  );
    s = " abcd ";  str_trim(s);  ok = ok && check("string",5,4, s == "abcd"  );
    s = " ab cd "; str_trim(s);  ok = ok && check("string",5,5, s == "ab cd" );
    s = " ";       str_trim(s);  ok = ok && check("string",5,6, s == ""      );
    //
    //-------
    s = "";        str_replace(s,"","");      ok = ok && check("string",6,1, s == ""       );
    s = "abcabcd"; str_replace(s,"ab","AB");  ok = ok && check("string",6,2, s == "ABcABcd");
    s = "abcabcd"; str_replace(s,"cd","CD");  ok = ok && check("string",6,3, s == "abcabCD");
    s = "abcabcd"; str_replace(s,"a","A");    ok = ok && check("string",6,4, s == "AbcAbcd");
    s = "abcabcd"; str_replace(s,"d","D");    ok = ok && check("string",6,5, s == "abcabcD");
    s = "abcabcd"; str_replace(s,"c","C",1);  ok = ok && check("string",6,6, s == "abCabcd");
    s = "x.c";     str_replace(s,".","\\.");  ok = ok && check("string",6,7, s == "x\\.c"  );
    //
    //-------
    std::vector< std::string >      A, R;
    std::vector< std::string_view > Av, Rv;
    //
    A = str_split("", ",");            R = {};                 ok = ok && check("string",7, 1, A == R);
    A = str_split("a,b,c", ",");       R = {"a","b","c"};      ok = ok && check("string",7, 2, A == R);
    A = str_split("ab,cd,ef", ",");    R = {"ab","cd","ef"};   ok = ok && check("string",7, 3, A == R);
    A = str_split("1,2,3,4", ",", 2);  R = {"1","2","3,4"};    ok = ok && check("string",7, 4, A == R);
    A = str_split("a,,b", ",");        R = {"a","","b"};       ok = ok && check("string",7, 5, A == R);
    A = str_split("a,b,", ",");        R = {"a","b",""};       ok = ok && check("string",7, 6, A == R);
    A = str_split(",b,c", ",");        R = {"","b","c"};       ok = ok && check("string",7, 7, A == R);
    A = str_split(",", ",");           R = {"",""};            ok = ok && check("string",7, 8, A == R);
    A = str_split(",,", ",", 1);       R = {"",","};           ok = ok && check("string",7, 9, A == R);
    A = str_split(",,", ",", 2);       R = {"","",""};         ok = ok && check("string",7,10, A == R);
    A = str_split("aa:*:bb", ":*:");   R = {"aa","bb"};        ok = ok && check("string",7,11, A == R);
    A = str_split("aa", ":*:");        R = {"aa"};             ok = ok && check("string",7,12, A == R);
    A = str_split(":*:", ":*:");       R = {"",""};            ok = ok && check("string",7,13, A == R);
    A = str_split("aa:*:", ":*:");     R = {"aa",""};          ok = ok && check("string",7,14, A == R);
    //
    Av = str_split_view("", ",");            Rv = {};                 ok = ok && check("string",7,11, Av == Rv);
    Av = str_split_view("a,b,c", ",");       Rv = {"a","b","c"};      ok = ok && check("string",7,12, Av == Rv);
    Av = str_split_view("ab,cd,ef", ",");    Rv = {"ab","cd","ef"};   ok = ok && check("string",7,13, Av == Rv);
    Av = str_split_view("1,2,3,4", ",", 2);  Rv = {"1","2","3,4"};    ok = ok && check("string",7,14, Av == Rv);
    Av = str_split_view("a,,b", ",");        Rv = {"a","","b"};       ok = ok && check("string",7,15, Av == Rv);
    Av = str_split_view("a,b,", ",");        Rv = {"a","b",""};       ok = ok && check("string",7,16, Av == Rv);
    Av = str_split_view(",b,c", ",");        Rv = {"","b","c"};       ok = ok && check("string",7,17, Av == Rv);
    Av = str_split_view(",", ",");           Rv = {"",""};            ok = ok && check("string",7,18, Av == Rv);
    Av = str_split_view(",,", ",", 1);       Rv = {"",","};           ok = ok && check("string",7,19, Av == Rv);
    Av = str_split_view(",,", ",", 2);       Rv = {"","",""};         ok = ok && check("string",7,20, Av == Rv);
    Av = str_split_view("aa:*:bb", ":*:");   Rv = {"aa","bb"};        ok = ok && check("string",7,21, Av == Rv);
    Av = str_split_view("aa", ":*:");        Rv = {"aa"};             ok = ok && check("string",7,22, Av == Rv);
    Av = str_split_view(":*:", ":*:");       Rv = {"",""};            ok = ok && check("string",7,23, Av == Rv);
    Av = str_split_view("aa:*:", ":*:");     Rv = {"aa",""};          ok = ok && check("string",7,24, Av == Rv);
    //
    //-------
    ok = ok && check("string",8,1, str_join(std::vector<int>        ({}),",")                  == ""          );
    ok = ok && check("string",8,2, str_join(std::vector<int>        ({1, 2, 3, 4, 5}),",")     == "1,2,3,4,5" );
    ok = ok && check("string",8,3, str_join(std::vector<std::string>({"a","b","c"}),",")         == "a,b,c"     );
    ok = ok && check("string",8,4, str_join(std::list<int>          ({1, 2, 3, 4, 5}),",")       == "1,2,3,4,5" );
    ok = ok && check("string",8,5, str_join(std::set<std::string>   ({"a","b","z" }),",") == "a,b,z"     );
    //
    //-------
    ok = ok && check("string",9,1, str_sprintf("%u",     1) == "1"           );
    ok = ok && check("string",9,2, str_sprintf("%2048u", 1).length() == 2048 );
    //
    //-------
    ok = ok && check("string",10, 1, str_starts_with("", "")          == true);
    ok = ok && check("string",10, 2, str_starts_with("abcdef", "ab")  == true);
    ok = ok && check("string",10, 3, str_starts_with("abcdef", "bc")  == false);
    ok = ok && check("string",10, 4, str_starts_with("ab",     "abc") == false);
    //
    //-------
    ok = ok && check("string",11, 1, str_ends_with("", "")         == true);
    ok = ok && check("string",11, 2, str_ends_with("abcdef", "ef") == true);
    ok = ok && check("string",11, 3, str_ends_with("abcdef", "de") == false);
    ok = ok && check("string",11, 4, str_ends_with("abc",    "ab") == false);
    ok = ok && check("string",11, 4, str_ends_with("a",      "ab") == false);
    //
    //-------
    s=""; t=str_token(s);          ok = ok && check("string",12, 1, t==""    && s=="");
    s=" "; t=str_token(s);         ok = ok && check("string",12, 2, t==""    && s=="");
    s="a1a"; t=str_token(s);       ok = ok && check("string",12, 3, t=="a1a" && s=="");
    s=" a2a"; t=str_token(s);      ok = ok && check("string",12, 4, t=="a2a" && s=="");
    s=" a3a "; t=str_token(s);     ok = ok && check("string",12, 5, t=="a3a" && s=="");
    s="a4a bbb"; t=str_token(s);   ok = ok && check("string",12, 6, t=="a4a" && s=="bbb");
    s=" a5a bbb"; t=str_token(s);  ok = ok && check("string",12, 7, t=="a5a" && s=="bbb");
    s="a6a bbb "; t=str_token(s);  ok = ok && check("string",12, 8, t=="a6a" && s=="bbb ");
    s=" a7a bbb "; t=str_token(s); ok = ok && check("string",12, 9, t=="a7a" && s=="bbb ");
    //
    sv=""; tv=str_token_view(sv);          ok = ok && check("string",12, 11, tv==""    && sv=="");
    sv=" "; tv=str_token_view(sv);         ok = ok && check("string",12, 12, tv==""    && sv=="");
    sv="a1a"; tv=str_token_view(sv);       ok = ok && check("string",12, 13, tv=="a1a" && sv=="");
    sv=" a2a"; tv=str_token_view(sv);      ok = ok && check("string",12, 14, tv=="a2a" && sv=="");
    sv=" a3a "; tv=str_token_view(sv);     ok = ok && check("string",12, 15, tv=="a3a" && sv=="");
    sv="a4a bbb"; tv=str_token_view(sv);   ok = ok && check("string",12, 16, tv=="a4a" && sv=="bbb");
    sv=" a5a bbb"; tv=str_token_view(sv);  ok = ok && check("string",12, 17, tv=="a5a" && sv=="bbb");
    sv="a6a bbb "; tv=str_token_view(sv);  ok = ok && check("string",12, 18, tv=="a6a" && sv=="bbb ");
    sv=" a7a bbb "; tv=str_token_view(sv); ok = ok && check("string",12, 19, tv=="a7a" && sv=="bbb ");
    //
    //-------
    ok = ok && check( "string", 13,  1,   str_csv_has_value( "ab", "ab,cd"     ) );
    ok = ok && check( "string", 13,  2,   str_csv_has_value( "cd", "ab,cd"     ) );
    ok = ok && check( "string", 13,  3,   str_csv_has_value( "cd", "ab,cd,ef"  ) );
    ok = ok && check( "string", 13,  4, ! str_csv_has_value( "ab", "abb,cd"    ) );
    ok = ok && check( "string", 13,  5, ! str_csv_has_value( "cd", "ab,ccd"    ) );
    ok = ok && check( "string", 13,  6, ! str_csv_has_value( "cd", "ab,cdd,ef" ) );
    ok = ok && check( "string", 13,  7, ! str_csv_has_value( "cd", "ab,cdef"   ) );
    ok = ok && check( "string", 13,  8, ! str_csv_has_value( "cd", "abcd,ef"   ) );
    ok = ok && check( "string", 13,  9,   str_csv_has_value( "ab", "ab"        ) );
    ok = ok && check( "string", 13, 10,   str_csv_has_value( "ab", ",ab"       ) );
    ok = ok && check( "string", 13, 11,   str_csv_has_value( "ab", "ab,"       ) );
    ok = ok && check( "string", 13, 12,   str_csv_has_value( "ab", ",ab,"      ) );
    //
    return ok;
}

//-------------------------------------------------------------------------
// Note: system timezone must be set to 'Europe/Paris':
//  sudo timedatectl set-timezone Europe/Paris
bool validate_time ( void )
{
    // This validation assumes for some tests that the timezone
    // is set to CET
    time_t            now    = time(NULL);
    const struct tm * lt     = localtime( & now );
    bool              bad_tz = strcmp( lt->tm_zone, "CET"  ) != 0 &&
                               strcmp( lt->tm_zone, "CEST" ) != 0;
    //
    if ( bad_tz )
        printf( " !CET" );
    //
    bool        ok = true;
    //
    ok = ok && check("time",1,1,time_format_rfc3339_utc  (1330534800) == "2012-02-29T17:00:00Z");
    ok = ok && check("time",1,2,time_format_rfc3339_utc  (1406847600) == "2014-07-31T23:00:00Z");
    //
    if ( ! bad_tz )
    {
        ok = ok && check("time",1,3,time_format_rfc3339_local(1330534800) == "2012-02-29T18:00:00+01:00");
        ok = ok && check("time",1,4,time_format_rfc3339_local(1406847600) == "2014-08-01T01:00:00+02:00");
        //
        ok = ok && check("time",2,1,time_format_rfc3339_utc(time_last_midnight(1330534800)) == "2012-02-28T23:00:00Z");
        ok = ok && check("time",2,2,time_format_rfc3339_utc(time_last_midnight(1406847600)) == "2014-07-31T22:00:00Z");
        ok = ok && check("time",2,3,time_format_rfc3339_utc(time_next_midnight(1330534800)) == "2012-02-29T23:00:00Z");
        ok = ok && check("time",2,4,time_format_rfc3339_utc(time_next_midnight(1406847600)) == "2014-08-01T22:00:00Z");
    }
    //
    long int s = time_seconds_since_midnight(),
             t = time_seconds_to_midnight();
    ok = ok && check("time",3,1, (s+t) == 86400 );
    //
    ok = ok && check("time",4,1,time_parse_rfc3339("2012-02-29T18:00:00+01:00") == 1330534800);
    ok = ok && check("time",4,2,time_parse_rfc3339("2014-08-01T01:00:00+02:00") == 1406847600);
    ok = ok && check("time",4,3,time_parse_rfc3339("2012-02-29T17:00:00Z")      == 1330534800);
    ok = ok && check("time",4,4,  -1 == time_parse_rfc3339("2012-02-29T17:00000Z") );
    ok = ok && check("time",4,5,  -1 == time_parse_rfc3339("2012-02-29T17:00:00Y") );
    ok = ok && check("time",4,6,  -1 == time_parse_rfc3339("2012-02-29y17:00:00Z") );
    ok = ok && check("time",4,7,  -1 == time_parse_rfc3339("2012/02/29 17:00:00Z") );
    ok = ok && check("time",4,8,  -1 == time_parse_rfc3339("2012-02-29T18:00000+01:00") );
    ok = ok && check("time",4,9,  -1 == time_parse_rfc3339("2012-02-29T18:00:00*01:00") );
    ok = ok && check("time",4,10, -1 == time_parse_rfc3339("2012-02-29=18:00:00+01:00") );
    ok = ok && check("time",4,11, -1 == time_parse_rfc3339("2012-02-29T18:00:00+A:00") );
    ok = ok && check("time",4,12, -1 == time_parse_rfc3339("2012-02-29T18:00:00+01:99") );
    ok = ok && check("time",4,13, -1 == time_parse_rfc3339("2012-02-29T18:00:00+24:00") );
    ok = ok && check("time",4,14, -1 == time_parse_rfc3339("2012-02-29T18:00:00-25:00") );
    //
    ok = ok && check("time",5, 1,time_parse_duration("1s")      == 1);
    ok = ok && check("time",5, 2,time_parse_duration(" 1s")     == 1);
    ok = ok && check("time",5, 3,time_parse_duration("1s ")     == 1);
    ok = ok && check("time",5, 4,time_parse_duration(" 1s ")    == 1);
    ok = ok && check("time",5, 5,time_parse_duration("2m1s")    == 121);
    ok = ok && check("time",5, 6,time_parse_duration(" 2m1s")   == 121);
    ok = ok && check("time",5, 7,time_parse_duration("2m1s ")   == 121);
    ok = ok && check("time",5, 8,time_parse_duration(" 2m1s ")  == 121);
    ok = ok && check("time",5, 9,time_parse_duration("2m1s")    == 121);
    ok = ok && check("time",5,10,time_parse_duration("2x 1s")   == -1);
    ok = ok && check("time",5,11,time_parse_duration("2m 1x")   == -1);
    ok = ok && check("time",5,12,time_parse_duration("s")       == -1);
    ok = ok && check("time",5,13,time_parse_duration("1")       == -1);
    ok = ok && check("time",5,14,time_parse_duration("1x")      == -1);
    ok = ok && check("time",5,15,time_parse_duration("m 1s")    == -1);
    ok = ok && check("time",5,16,time_parse_duration("2 1s")    == -1);
    ok = ok && check("time",5,17,time_parse_duration("x 1s")    == -1);
    ok = ok && check("time",5,18,time_parse_duration("2m s")    == -1);
    ok = ok && check("time",5,19,time_parse_duration("2m 1")    == -1);
    ok = ok && check("time",5,20,time_parse_duration("2m x")    == -1);
    ok = ok && check("time",5,21,time_parse_duration("2m 1x")   == -1);
    ok = ok && check("time",5,23,time_parse_duration("1h")      == 3600);
    ok = ok && check("time",5,23,time_parse_duration("1d")      == 86400);
    ok = ok && check("time",5,23,time_parse_duration("1w")      == 604800);
    ok = ok && check("time",5,22,time_parse_duration("1o")      == 2629746);
    ok = ok && check("time",5,23,time_parse_duration("1y")      == 31556952);
    ok = ok && check("time",5,24,time_parse_duration("2m 1s")   == 121);
    //
    // Note: may fail is DST occurs during the period
    ok = ok && check("time",6,1,time_parse_iso8601_duration("PT1S"    ) == 1 );
    ok = ok && check("time",6,2,time_parse_iso8601_duration("PT36H"   ) == 36 * 3600 );
    ok = ok && check("time",6,3,time_parse_iso8601_duration("P1DT12H" ) == 86400 + 12 * 3600 );
    ok = ok && check("time",6,4,time_parse_iso8601_duration("P23DT23H") == 23 * 86400 + 23 * 3600 );
    ok = ok && check("time",6,5,time_parse_iso8601_duration("P1D"     ) == 24 * 3600 );
    ok = ok && check("time",6,6,time_parse_iso8601_duration("PT6H"    ) == 6  * 3600 );
    ok = ok && check("time",6,7,time_parse_iso8601_duration("PT24H"   ) == 24 * 3600 );
    ok = ok && check("time",6,8,time_parse_iso8601_duration("P1DT1H"  ) == 25 * 3600 );
    ok = ok && check("time",6,9,time_parse_iso8601_duration("PT1H2M3S") == 3600 + 2 * 60 + 3 );
    //
    ok = ok && check("time",6,10,time_parse_iso8601_duration("") == -1 );
    ok = ok && check("time",6,11,time_parse_iso8601_duration("T1H") == -1 );
    ok = ok && check("time",6,12,time_parse_iso8601_duration("PH2") == -1 );
    ok = ok && check("time",6,13,time_parse_iso8601_duration("P3Z") == -1 );
    ok = ok && check("time",6,14,time_parse_iso8601_duration("PT3Z") == -1 );
    ok = ok && check("time",6,15,time_parse_iso8601_duration("PT1S",9223372036854775807) == -1 );
    //
    time_t a_time = time_parse_rfc3339( "2020-06-01T00:00:00Z" );
    ok = ok && check("time",6,20,time_parse_iso8601_duration("P1Y1M1W",a_time) == 402 * 86400 );  // 365 + 30 + 7 (2021 is not bissextile, June has 30 days)
    //
    ok = ok && check("time",6,30,time_parse_iso8601_duration("PT0.1H0.5M7S",a_time) == 397 );
    ok = ok && check("time",6,31,time_parse_iso8601_duration("P0.5Y"       ,a_time) == 183 * 86400 + 3600 ); // June+July+...+November + 1 DST hour
    ok = ok && check("time",6,32,time_parse_iso8601_duration("P0.1Y0.2M0.3W0.4DT0.5H0.6M3.5S",a_time) == 3'861'737 );
    //
    // 1616886000 is 2021-03-27T23:00:00Z , 2H before French DST
    if ( ! bad_tz )
    {
        time_t summer_dst = time_parse_rfc3339( "2021-03-27T23:00:00Z" );
        ok = ok && check("time",7,1,time_parse_iso8601_duration("P1D"   ,summer_dst) == 23 * 3600 );
        ok = ok && check("time",7,2,time_parse_iso8601_duration("PT6H"  ,summer_dst) == 6  * 3600 );
        ok = ok && check("time",7,3,time_parse_iso8601_duration("PT24H" ,summer_dst) == 24 * 3600 );
        ok = ok && check("time",7,4,time_parse_iso8601_duration("P1DT1H",summer_dst) == 24 * 3600 );
        //
        time_t winter_dst = time_parse_rfc3339( "2020-10-24T22:00:00Z" );
        ok = ok && check("time",7,5,time_parse_iso8601_duration("P1D"   ,winter_dst) == 25 * 3600 );
        ok = ok && check("time",7,6,time_parse_iso8601_duration("PT6H"  ,winter_dst) == 6  * 3600 );
        ok = ok && check("time",7,7,time_parse_iso8601_duration("PT24H" ,winter_dst) == 24 * 3600 );
        ok = ok && check("time",7,8,time_parse_iso8601_duration("P1DT1H",winter_dst) == 26 * 3600 );
    }
    //
    ok = ok && check("time",8,1, time_seconds_since_midnight( 9223372036854775807 ) == -1);
    ok = ok && check("time",8,2, time_last_midnight( 9223372036854775807 ) == -1);
    ok = ok && check("time",8,3, time_seconds_to_midnight( 9223372036854775807 ) == -1);
    ok = ok && check("time",8,4, time_next_midnight( 9223372036854775807 ) == -1);
    ok = ok && check("time",8,5, time_format_rfc3339_utc( 9223372036854775807 ) == "");
    ok = ok && check("time",8,6, time_format_rfc3339_local( 9223372036854775807 ) == "");
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_thread_queue ( void )
{
    bool        ok = true;
    //
    ThreadQueue<unsigned>     q;
    unsigned                  u, y = 24, x = 4;
    std::optional< unsigned > p;
    //
    q.push(1);
    q.push(2);
    q.push(3);
    q.push(std::move(x));
    //
    ok = ok && check("queue",1,1, q.queue_size() == 4);
    //
    ok &= ( p=q.pop(1s), p.has_value() ) && check("queue",2,1, p.value() == 1);
    ok &= ( p=q.pop(1s), p.has_value() ) && check("queue",2,2, p.value() == 2);
    ok &= ( p=q.pop(1s), p.has_value() ) && check("queue",2,3, p.value() == 3);
    ok &= ( p=q.pop(1s), p.has_value() ) && check("queue",2,4, p.value() == 4);
    ok = ok && check("queue",2,5, ( p=q.pop(1ms), !p.has_value() ));
    //
    ok = ok && check("queue",3,1, q.count_consumers()== 0);
    //
    q.push(11);
    q.clear();
    ok = ok && check("queue",4,1, q.queue_size() == 0 && ( p=q.pop(1ms), !p.has_value() ));
    //
    q.clear();
    q.push(12, 10ms);
    ok = ok && check("queue",5,1, q.queue_size() == 0 && q.waiting_size() == 1 && ( p=q.pop(1ms), !p.has_value() ));
    std::this_thread::sleep_for (20ms);
    ok = ok && check("queue",5,2, q.queue_size() == 1 && q.waiting_size() == 0 && ( p=q.pop(1ms), p.has_value() ) && p.value() == 12);
    //
    q.clear();
    q.push(y , 20ms); // 24
    q.push(22, 10ms);
    q.push(21,  5ms);
    q.push(23, 15ms);
    std::this_thread::sleep_for (25ms);
    ok = ok && check("queue",6,1, q.queue_size() == 4 && q.waiting_size() == 0 &&
                    ( p=q.pop(1ms), p.has_value() ) && p.value() == 21 &&
                    ( p=q.pop(1ms), p.has_value() ) && p.value() == 22 &&
                    ( p=q.pop(1ms), p.has_value() ) && p.value() == 23 &&
                    ( p=q.pop(1ms), p.has_value() ) && p.value() == 24 );
    //
    //--------------------
    {
        q.clear();
        q.push(1);
        q.push(2);
        q.push(3);
        u = 0;
        //
        auto [ st, rem, tot ] = q.browse_queue(
            [ & u ]( const auto & e ) -> std::tuple<bool,bool>
            {
                u += e;
                //
                if ( e == 2 )
                    return { true, true }; // stop, remove
                else
                    return { false, false }; // stop, remove
            }
        );
        //
        ok = ok && check("queue",7,1, q.queue_size() == 2 &&
                                 ( p=q.pop(1ms), p.has_value() ) && p.value() == 1 &&
                                 ( p=q.pop(1ms), p.has_value() ) && p.value() == 3 &&
                                 u == 3 &&
                                 st == true && rem == 1 && tot == 2 );
    }
    //
    //--------------------
    {
        q.clear();
        q.push(1, 1s);
        q.push(2, 1s);
        //
        auto [ st, rem, tot ] = q.browse_waiting(
            [](auto & tp, auto & e) -> std::tuple<bool, bool> {
                return {false, true}; // stop, remove
            });
        //
        ok = ok && check("queue",7,2, q.waiting_size() == 0 &&
                                 st == false && rem == 2 && tot == 0 );
    }
    //
    return ok;
}

//-------------------------------------------------------------------------
std::atomic_uint g_uCounter;
//
bool validate_thread_pool ( void )
{
    bool     ok = true;
    //
    {
        class W : public ThreadPool<unsigned>
        {
            protected:
                void execute (unsigned && p_u) override
                {
                    std::this_thread::sleep_for (50ms);
                    g_uCounter += p_u;
                }
        };
        //
        W        w;
        unsigned x = 1;
        //
        w.set_min_threads     ( 1  );
        w.set_max_threads     ( 2  );
        w.set_linger_delay    ( 1s );
        //
        ok = ok && check("thread",0,1, w.min_threads() == 1 );
        ok = ok && check("thread",0,2, w.max_threads() == 2 );
        ok = ok && check("thread",0,3, w.linger_delay() == 1s );
        //
        g_uCounter.store (0);
        ok = ok && check("thread",1,1, w.push (x));
        std::this_thread::sleep_for (100ms);
        ok = ok && check("thread",1,2, w.count_jobs_pending()==0 && w.count_threads()==1 && w.idle() && g_uCounter==1);
        //
        g_uCounter.store (0);
        ok = ok && check("thread",2,1, w.push (1) && w.push (2));
        std::this_thread::sleep_for (100ms);
        ok = ok && check("thread",2,2, w.count_jobs_pending()==0 && w.count_threads()==2 && w.idle() && g_uCounter==3);
        //
        g_uCounter.store (0);
        ok = ok && check("thread",3,1, w.push (1) && w.push (2) && w.push (3));
        std::this_thread::sleep_for (10ms);
        ok = ok && check("thread",3,2, w.count_jobs_pending()==1 && w.count_threads()==2 && ! w.idle() && g_uCounter==0);
        std::this_thread::sleep_for (65ms);
        ok = ok && check("thread",3,3, w.count_jobs_pending()==0 && w.count_threads()==2 && ! w.idle() && g_uCounter==3);
        std::this_thread::sleep_for (150ms);
        ok = ok && check("thread",3,4, w.count_jobs_pending()==0 && w.count_threads()==2 &&   w.idle() && g_uCounter==6);
        //
        g_uCounter.store (0);
        ok = ok && check("thread",4,1, w.push (std::move(x)));
        std::this_thread::sleep_for (1000ms);  // wait 2nd thread to stop and exit
        ok = ok && check("thread",4,2, w.count_jobs_pending()==0 && w.count_threads()==1 && w.idle() && g_uCounter==1);
        //
        {
            g_uCounter.store (0);
            w.stop(true);
            w.start();
            //
            ok = ok && check("thread",5,1, w.push (1) && w.push (2));                // count thread = 2
            std::this_thread::sleep_for (10ms);                                 // 2 running (queue empty)
            ok = ok && check("thread",5,2, w.count_jobs_running() == 2);
            ok = ok && check("thread",5,3, w.push (3) && w.push (4) &&
                                    w.push (5) && w.push (6,40ms));           // Fill the queue
            ok = ok && check("thread",5,4, w.count_jobs_running() == 2 && w.count_jobs_pending()==3 && w.count_jobs_waiting()==1);
            std::this_thread::sleep_for (60ms);                                 // 2 are done, waiting was pushed
            ok = ok && check("thread",5,5, w.count_jobs_running() == 2 && w.count_jobs_pending()==2 && w.count_jobs_waiting()==0);
            std::this_thread::sleep_for (190ms);                                // 150ms to complete 2*3 jobs: all must be finished now
            ok = ok && check("thread",5,6, w.count_jobs_pending()==0 && w.count_threads()==2 && w.idle() && g_uCounter==21);
        }
        //
        {
            w.stop(true);
            w.start();
            //
            w.push (6, 2s);
            w.push (6, 1s);
            w.push (2, 1s);
            auto [ st, rem, tot ] = w.browse_waiting(
                [](const auto & tp, const auto & e) -> std::tuple<bool, bool> {
                    return {false, e == 6 && tp >= (std::chrono::steady_clock::now() + 1.5s) }; // stop, remove
                });
            //
            ok = ok && check("thread",6,1, w.count_jobs_pending()==0 && w.count_jobs_waiting()==2 &&
                                    st == false && rem == 1 && tot == 2 );
        }
        //
        {
            w.stop(true);
            //
            ok = ok && check("thread",7,1, ! w.push( 1 ) );
            ok = ok && check("thread",7,2, ! w.push( std::move(x) ) );
            ok = ok && check("thread",7,3, ! w.push( x ) );
        }
    }
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_file ( void )
{
    bool        ok = true;
    std::string s;
    //
    ok = ok && check("file",1,1,file_write ( "_adawat_validate.tmp", "123\nabc\n"s ) );
    ok = ok && check("file",1,2,file_append( "_adawat_validate.tmp", "456"s ) );
    ok = ok && check("file",1,3,file_read  ( "_adawat_validate.tmp", s ) && s == "123\nabc\n456" );
    //
    Data d, e;
    d.assign( { 0, 255, 64, 0, 128, 1, 26, 4, 0 } );
    ok = ok && check("file",2,1,file_write ( "_adawat_validate.tmp", d ) );
    ok = ok && check("file",2,2,file_read  ( "_adawat_validate.tmp", e ) && d == e );
    d.assign( { 1, 2, 3, 10, 13, 10, 7, 8, 9, 10 } );
    ok = ok && check("file",2,3,file_append( "_adawat_validate.tmp", d ) );
    d.assign( { 0, 255, 64, 0, 128, 1, 26, 4, 0, 1, 2, 3, 10, 13, 10, 7, 8, 9, 10 } );
    ok = ok && check("file",2,4,file_read  ( "_adawat_validate.tmp", e ) && d == e );
    //
    ok = ok && check("file",3,1, crypto_random( 1'000'000, d, false )       &&
                                 file_write ( "_adawat_validate.tmp", d )   &&
                                 file_read  ( "_adawat_validate.tmp", e )   &&
                                 d == e );
    //
    ok = ok && check("file",4,1,file_write ( "/tmp/_adawat/sub/_adawat_validate.tmp", "sub-file"s,
                                             true ) );
    ok = ok && check("file",4,2,file_read  ( "/tmp/_adawat/sub/_adawat_validate.tmp", s ) && s == "sub-file" );
    //
    ok = ok && check("file",5,1, ! file_write ( "bad/", "sub-file"s, true ) );
    ok = ok && check("file",5,2, ! file_read  ( "bad.tmp", s ) );
    ok = ok && check("file",5,3, ! file_read  ( "bad.tmp", d ) );
    ok = ok && check("file",5,4, ! file_append( "bad/", s ) );
    //
    unlink( "_adawat_validate.tmp" );
    unlink( "/tmp/_adawat/sub/_adawat_validate.tmp" );
    rmdir ( "/tmp/_adawat/sub" );
    rmdir ( "/tmp/_adawat" );
    //
    return ok;
}

//-------------------------------------------------------------------------
    bool add_test( const std::string & p_currency,
                   const std::string & p_a, const std::string & p_b,
                   const std::string & p_c ) {
        Amount a, b, c;
        //
        return  a.set( p_a, p_currency ) == Amount::result::ok &&
                b.set( p_b, p_currency ) == Amount::result::ok &&
                c.set( p_c, p_currency ) == Amount::result::ok &&
                a.add( b )               == Amount::result::ok &&
                a == c;
    }

    bool sub_test( const std::string & p_currency,
                   const std::string & p_a, const std::string & p_b,
                   const std::string & p_c ) {
        Amount  a, b, c;
        //
        return  a.set( p_a, p_currency ) == Amount::result::ok &&
                b.set( p_b, p_currency ) == Amount::result::ok &&
                c.set( p_c, p_currency ) == Amount::result::ok &&
                a.sub( b )               == Amount::result::ok &&
                a == c;
    }

    bool mul_test( const std::string & p_currency,
                   const std::string & p_a, const std::string & p_b,
                   const std::string & p_c ) {
        Amount a, c;
        //
        return  a.set( p_a, p_currency ) == Amount::result::ok &&
                c.set( p_c, p_currency ) == Amount::result::ok &&
                a.mul( p_b )             == Amount::result::ok &&
                a == c;
    }

    bool div_test( const std::string & p_currency,
                   const std::string & p_a, const std::string & p_b,
                   const std::string & p_c ) {
        Amount a, c;
        //
        return  a.set( p_a, p_currency ) == Amount::result::ok &&
                c.set( p_c, p_currency ) == Amount::result::ok &&
                a.div( p_b )             == Amount::result::ok &&
                a == c;
    }

    bool percent_test( const std::string & p_currency,
                       const std::string & p_a, const std::string & p_b,
                       const std::string & p_c ) {
        Amount a, c;
        //
        return  a.set( p_a, p_currency ) == Amount::result::ok &&
                c.set( p_c, p_currency ) == Amount::result::ok &&
                a.percent( p_b )         == Amount::result::ok &&
                a == c;
    }

    bool round_test( const std::string & p_currency,
                     const std::string & p_a, unsigned p_mode, uint32_t p_precision,
                     const std::string & p_c ) {
        Amount a, c;
        //
        return  a.set( p_a, p_currency ) == Amount::result::ok &&
                c.set( p_c, p_currency ) == Amount::result::ok &&
                a.round( p_mode, p_precision )   == Amount::result::ok &&
                a == c;
    }

bool validate_money ( void )
{
    bool        ok = true;
    int         i;
    Amount      a,b,c;
    //
    ok = ok && check("money",1,1, add_test( "EUR","1","2","3"));
    ok = ok && check("money",1,2, add_test( "EUR","1.501","0.5","2.001"));
    ok = ok && check("money",1,3, add_test( "EUR","10.5","-1.0","9.5"));
    ok = ok && check("money",1,4, add_test( "EUR","0.000001","99.999999","100"));
    ok = ok && check("money",1,5, add_test( "EUR","9223372036854.775804","0.000001","9223372036854.775805"));
    ok = ok && check("money",1,6, add_test( "EUR","-0.6","-0.4","-1"));
    ok = ok && check("money",1,7, add_test( "EUR","0.000001000","99.99999900","100"));
    ok = ok && check("money",1,8, add_test( "EUR","1.0","2.0","3.0"));
    //
    ok = ok && check("money",2,1, sub_test( "EUR","2","1","1"));
    ok = ok && check("money",2,2, sub_test( "EUR","1","2","-1"));
    ok = ok && check("money",2,2, sub_test( "EUR","-1","2","-3"));
    ok = ok && check("money",2,2, sub_test( "EUR","1","-3","4"));
    ok = ok && check("money",2,3, sub_test( "EUR","-9223372036854.775805","0.000001","-9223372036854.775806"));
    ok = ok && check("money",2,4, sub_test( "EUR","1","0.000001","0.999999"));
    //
    ok = ok && check("money",3,1, mul_test( "EUR","2","3","6"));
    ok = ok && check("money",3,2, mul_test( "EUR","3","0.333333","0.999999"));
    ok = ok && check("money",3,3, mul_test( "EUR","0.001","0.001","0.000001"));
    ok = ok && check("money",3,4, mul_test( "EUR","700000000000","10","7000000000000"));
    ok = ok && check("money",3,5, mul_test( "EUR","7000000000","1000","7000000000000"));
    ok = ok && check("money",3,6, mul_test( "EUR","7000000","1000000","7000000000000"));
    ok = ok && check("money",3,7, mul_test( "EUR","7000","1000000000","7000000000000"));
    ok = ok && check("money",3,8, mul_test( "EUR","70","100000000000","7000000000000"));
    ok = ok && check("money",3,1, mul_test( "EUR","810.5","0.05","40.525"));
    ok = ok && check("money",3,1, mul_test( "EUR","0.01","0.05","0.0005"));
    //
    ok = ok && check("money",4, 1, div_test( "EUR","10","3","3.333333"));
    ok = ok && check("money",4, 2, div_test( "EUR","-14","3.5","-4"));
    ok = ok && check("money",4, 3, div_test( "EUR","1","1000000","0.000001"));
    ok = ok && check("money",4, 4, div_test( "EUR","101.99","-1.186","-85.994940"));
    ok = ok && check("money",4, 5, div_test( "EUR","101.99","1.186","85.994940"));
    ok = ok && check("money",4, 6, div_test( "EUR","14","3.5","4"));
    ok = ok && check("money",4, 7, div_test( "EUR","4.1548","1834.9843","0.002264"));
    ok = ok && check("money",4, 8, div_test( "EUR","4886.000001","848761.4861","0.005756"));
    ok = ok && check("money",4, 9, div_test( "EUR","-100000000000","3","-33333333333.333333"));
    ok = ok && check("money",4,10, div_test( "EUR","100000000000","3","33333333333.333333"));
    ok = ok && check("money",4,11, div_test( "EUR","100000000000","1000","100000000"));
    ok = ok && check("money",4,12, div_test( "EUR","13846248.1548","-1834.9843","-7545.703881"));
    //
    ok = ok && check("money",5,1, percent_test( "EUR","4.50","19.6","0.882"));
    ok = ok && check("money",5,2, percent_test( "EUR","4897618464.1548","19.6","959933218.974340"));
    ok = ok && check("money",5,3, percent_test( "EUR","100000000000","7","7000000000"));
    //
    //  p_mode:      1 up, 2 down, 3 commercial
    ok = ok && check("money",6, 1, round_test( "EUR","10.501",1,2,"10.51"));
    ok = ok && check("money",6, 2, round_test( "EUR","10.501",2,2,"10.5"));
    ok = ok && check("money",6, 3, round_test( "EUR","10.501",3,2,"10.5"));
    ok = ok && check("money",6, 4, round_test( "EUR","10.505",1,2,"10.51"));
    ok = ok && check("money",6, 5, round_test( "EUR","10.505",2,2,"10.5"));
    ok = ok && check("money",6, 6, round_test( "EUR","10.505",3,2,"10.51"));
    ok = ok && check("money",6, 7, round_test( "EUR","10.507",1,2,"10.51"));
    ok = ok && check("money",6, 8, round_test( "EUR","10.507",2,2,"10.5"));
    ok = ok && check("money",6, 9, round_test( "EUR","10.507",3,2,"10.51"));
    ok = ok && check("money",6,10, round_test( "EUR","2.0864",1,2,"2.09"));
    ok = ok && check("money",6,11, round_test( "EUR","4.1729",1,2,"4.18"));
    ok = ok && check("money",6,12, round_test( "EUR","2.0864",2,2,"2.08"));
    ok = ok && check("money",6,13, round_test( "EUR","4.1729",2,2,"4.17"));
    ok = ok && check("money",6,14, round_test( "EUR","2.0864",3,2,"2.09"));
    ok = ok && check("money",6,15, round_test( "EUR","4.1729",3,2,"4.17"));
    ok = ok && check("money",6,16, round_test( "EUR","4.1729",3,4,"4.1729"));
    ok = ok && check("money",6,17, round_test( "EUR","4",3,4,"4"));
    //
    ok = ok && check("money",6,20, round_test( "TND","1.0990",1,3,"1.099"));
    ok = ok && check("money",6,21, round_test( "TND","1.0995",1,3,"1.100"));
    ok = ok && check("money",6,22, round_test( "TND","1.0999",1,3,"1.100"));
    ok = ok && check("money",6,23, round_test( "TND","1.1000",1,3,"1.100"));
    ok = ok && check("money",6,24, round_test( "TND","1.1001",1,3,"1.101"));
    ok = ok && check("money",6,25, round_test( "TND","1.1002",1,3,"1.101"));

    ok = ok && check("money",6,30, round_test( "TND","1.10002" ,1,3,"1.101"));
    ok = ok && check("money",6,31, round_test( "XAF","0.03"    ,1,0,"1"    ));  // XAF subunit: 0
    ok = ok && check("money",6,32, round_test( "EUR","1.123001",1,3,"1.124"));
    ok = ok && check("money",6,33, round_test( "EUR","1.12301" ,1,3,"1.124"));
    ok = ok && check("money",6,34, round_test( "EUR","1.1231"  ,1,3,"1.124"));
    ok = ok && check("money",6,35, round_test( "EUR","1.123"   ,1,3,"1.123"));
    ok = ok && check("money",6,36, round_test( "EUR","1.12"    ,1,3,"1.12" ));
    //
    ok = ok && check("money",6,40, round_test( "TND","1.0990",2,3,"1.099"));
    ok = ok && check("money",6,41, round_test( "TND","1.0995",2,3,"1.099"));
    ok = ok && check("money",6,42, round_test( "TND","1.0999",2,3,"1.099"));
    ok = ok && check("money",6,43, round_test( "TND","1.1000",2,3,"1.100"));
    ok = ok && check("money",6,44, round_test( "TND","1.1001",2,3,"1.100"));
    ok = ok && check("money",6,45, round_test( "TND","1.1002",2,3,"1.100"));
    //
    ok = ok && check("money",7,1, ! a.is_set()                                    &&
                                    a.set( "123.5", "EUR" ) == Amount::result::ok   &&
                                    a.is_set()                                      &&
                                    (a.value()+a.currency()) == "123.5EUR"             &&
                                    a.set( "123.0", "EUR" ) == Amount::result::ok   &&
                                    (a.value()+a.currency()) == "123EUR"               &&
                                    a.set( "123.01", "EUR" ) == Amount::result::ok  &&
                                    (a.value()+a.currency()) == "123.01EUR"            &&
                                    a.set( "0.1", "EUR" ) == Amount::result::ok     &&
                                    (a.value()+a.currency()) == "0.1EUR"               &&
                                    a.set( "0", "EUR" ) == Amount::result::ok       &&
                                    (a.value()+a.currency()) == "0EUR");
    ok = ok && check("money",7,2, a.set( "-123.5", "EUR" ) == Amount::result::ok  &&
                                  (a.value()+a.currency()) == "-123.5EUR"            &&
                                  a.set( "-123.0", "EUR" ) == Amount::result::ok  &&
                                  (a.value()+a.currency()) == "-123EUR"              &&
                                  a.set( "-123.01", "EUR" ) == Amount::result::ok &&
                                  (a.value()+a.currency()) == "-123.01EUR"           &&
                                  a.set( "-0.1", "EUR" ) == Amount::result::ok    &&
                                  (a.value()+a.currency()) == "-0.1EUR"              &&
                                  a.set( "-0", "EUR" ) == Amount::result::ok      &&
                                  (a.value()+a.currency()) == "0EUR");
    ok = ok && check("money",7,3, a.set( "123.5", "EUR" ) == Amount::result::ok        &&
                                  b.set( a.value(), a.currency() ) == Amount::result::ok &&
                                  a == b                                       &&
                                  a.set( "-0.5", "EUR" ) == Amount::result::ok         &&
                                  b.set( a.value(), a.currency() ) == Amount::result::ok &&
                                  a == b                                       &&
                                  a.set( "0.05", "EUR" ) == Amount::result::ok         &&
                                  b.set( a.value(), a.currency() ) == Amount::result::ok &&
                                  a == b );
    ok = ok && check("money",7,4, a.set( "1.0000001","EUR" )             == Amount::result::invalid_precision &&
                                  a.set( "1.00A1","EUR" )                == Amount::result::invalid_value &&
                                  a.set( "-11111111111111111111","EUR" ) == Amount::result::overflow);
    //
    ok = ok && check("money",8,1, a.set( "z",    "EUR" ) == Amount::result::invalid_value    &&
                                  a.set( "1",    "xx" )  == Amount::result::invalid_currency &&
                                  a.set( "1",    "E+R" ) == Amount::result::invalid_currency &&
                                  a.set( "0.01", "VND" ) == Amount::result::invalid_precision );
    ok = ok && check("money",8,2, a.set( "1", "EUR" ) == Amount::result::ok &&
                                  b.set( "1", "USD" ) == Amount::result::ok &&
                                  ! (a == b)                        &&
                                  a.add( b ) == Amount::result::incompatible_currency );
    ok = ok && check("money",8,3, a.set( "1", "EUR" ) == Amount::result::ok &&
                                  a.round( 9, 0 ) == Amount::result::invalid_mode &&
                                  a.round( 1, 7 ) == Amount::result::invalid_precision &&
                                  a.round( 1, 6 ) == Amount::result::ok );
    //
    ok = ok && check("money",9,1, a.set( "123456789.123456", "EUR" ) == Amount::result::ok  &&
                                  a.format( 32,"x",0,"y",0,"z") == "123456789x123456"     &&
                                  a.format( 32,"x",3,"y",0,"z") == "123y456y789x123456"   &&
                                  a.format( 32,"x",4,"y",0,"z") == "1y2345y6789x123456"   &&
                                  a.format( 32,"x",7,"y",0,"z") == "12y34y56y789x123456"  &&
                                  a.format( 32,"x",0,"y",1,"z") == "EURz123456789x123456" &&
                                  a.format( 32,"x",0,"y",2,"z") == "123456789x123456zEUR" &&
                                  a.format( 32,"x",0,"y",3,"z") == "€z123456789x123456"   &&
                                  a.format( 32,"x",0,"y",4,"z") == "123456789x123456z€"   &&
                                  a.format( 32,"x",0,"y",3,"")  == "€123456789x123456"    &&
                                  a.format( 32,"x",0,"y",4,"")  == "123456789x123456€"    &&
                                  a.format( 32,"xx",3,"yy",2,"zz") == "123yy456yy789xx123456zzEUR" );
    //
    ok = ok && check("money",9,2, a.set( "123456789.123456", "EUR" ) == Amount::result::ok     &&
                                  a.format( 6,"x",0,"y",0,"z") == "123456789x123456"   &&
                                  a.format( 2,"x",0,"y",0,"z") == "123456789x12"       &&
                                  a.format( 0,"x",3,"y",0,"z") == "123y456y789" );
    //
    ok = ok && check("money",10,1, a.set( "1", "EUR" ) == Amount::result::ok        &&
                                   b.set( "2", "EUR" ) == Amount::result::ok        &&
                                   c.set( "3", "USD" ) == Amount::result::ok        &&
                                   a.comp( b, i ) == Amount::result::ok && i == -1  &&
                                   b.comp( a, i ) == Amount::result::ok && i ==  1  &&
                                   a.comp( a, i ) == Amount::result::ok && i ==  0  &&
                                   a.comp( c, i ) == Amount::result::incompatible_currency );
    //
    ok = ok && check("money",10,2, a.set( "-1", "EUR" ) == Amount::result::ok  &&
                                   b.set( "0",  "EUR" ) == Amount::result::ok  &&
                                   c.set( "+1", "EUR" ) == Amount::result::ok  &&
                                   a.is_negative()                             &&
                                   b.is_zero()                                 &&
                                   c.is_positive() );
    //
    ok = ok && check("money",10,3, a.set( "1.1", "EUR" ) == Amount::result::ok  &&
                                   b.set( "1.1", "EUR" ) == Amount::result::ok  &&
                                   c.set( "1.2", "EUR" ) == Amount::result::ok  &&
                                   a == b                                       &&
                                   a != c );
    //
    ok = ok && check("money",11,1, a.set( "2", "XXX" ) == Amount::result::ok  &&
                                   a.format( a.subunit(), ".", 3, " ", 3 ) == "x 2.00" );
    ok = ok && check("money",11,2, a.set( "0.1", "XXX" ) == Amount::result::ok  &&
                                   a.format( a.subunit(), ".", 3, " ", 3 ) == "x 0.10" );
    ok = ok && check("money",11,3, a.set( "0.01", "XXX" ) == Amount::result::ok  &&
                                   a.format( a.subunit(), ".", 3, " ", 3 ) == "x 0.01" );
    ok = ok && check("money",11,4, a.set( "0.001", "XXX" ) == Amount::result::ok  &&
                                   a.format( a.subunit(), ".", 3, " ", 3 ) == "x 0.00" );
    ok = ok && check("money",11,5, a.set( "-0.1", "XXX" ) == Amount::result::ok  &&
                                   a.format( a.subunit(), ".", 3, " ", 3 ) == "x -0.10" );
    ok = ok && check("money",11,6, a.set( "-0.01", "XXX" ) == Amount::result::ok  &&
                                   a.format( a.subunit(), ".", 3, " ", 3 ) == "x -0.01" );
    ok = ok && check("money",11,7, a.set( "-0.001", "XXX" ) == Amount::result::ok  &&
                                   a.format( a.subunit(), ".", 3, " ", 3 ) == "x -0.00" );
    //
    ok = ok && check("money",11,11, a.set( "2", "XXX" ) == Amount::result::ok  &&
                                    a.format( a.subunit(), ".", 3, " ", 5 ) == "¤ 2.00" );
    ok = ok && check("money",11,12, a.set( "2", "XXX" ) == Amount::result::ok  &&
                                    a.format( a.subunit(), ".", 3, " ", 6 ) == "2.00 ¤" );
    //
    ok = ok && check("money",11,21, a.set( "1.23", "EUR" ) == Amount::result::ok  &&
                                    a.precision()     == 6    &&
                                    a.symbol()        == "€"  &&
                                    a.subunit()       == 2    &&
                                    a.numeric_code()  == 978 );
    //
    ok = ok && check("money",12,1, Amount::to_string( Amount::result::ok                     ) == "ok" );
    ok = ok && check("money",12,2, Amount::to_string( Amount::result::overflow               ) == "overflow" );
    ok = ok && check("money",12,3, Amount::to_string( Amount::result::division_by_zero       ) == "division_by_zero" );
    ok = ok && check("money",12,4, Amount::to_string( Amount::result::incompatible_currency  ) == "incompatible_currency" );
    ok = ok && check("money",12,5, Amount::to_string( Amount::result::invalid_value          ) == "invalid_value" );
    ok = ok && check("money",12,6, Amount::to_string( Amount::result::invalid_currency       ) == "invalid_currency" );
    ok = ok && check("money",12,7, Amount::to_string( Amount::result::invalid_mode           ) == "invalid_mode" );
    ok = ok && check("money",12,8, Amount::to_string( Amount::result::invalid_precision      ) == "invalid_precision" );
    //
    ok = ok && check("money",13,1, a.set( "1000000000", "EUR" ) == Amount::result::ok  &&
                                   a.mul( "1000000000" ) == Amount::result::overflow );
    ok = ok && check("money",13,2, a.set( "1", "EUR" ) == Amount::result::ok  &&
                                   a.div( "0" ) == Amount::result::division_by_zero );
    ok = ok && check("money",13,3, a.set( "1000000000", "EUR" ) == Amount::result::ok  &&
                                   a.percent( "1000000000" ) == Amount::result::overflow );
    ok = ok && check("money",13,4, a.set( "1", "EUR" ) == Amount::result::ok  &&
                                   a.mul( "1.a" ) == Amount::result::invalid_value );
    ok = ok && check("money",13,5, a.set( "9223372036854", "EUR" ) == Amount::result::ok  &&
                                   b.set( "9223372036854", "EUR" ) == Amount::result::ok  &&
                                   a.add( b ) == Amount::result::overflow );
    ok = ok && check("money",13,6, a.set( "1", "EUR" ) == Amount::result::ok  &&
                                   b.set( "1", "USD" ) == Amount::result::ok  &&
                                   a.sub( b ) == Amount::result::incompatible_currency );
    ok = ok && check("money",13,7, a.set( "9223372036854", "EUR" ) == Amount::result::ok  &&
                                   a.div( "0.1" ) == Amount::result::overflow );
    ok = ok && check("money",13,8, a.set( "1", "EUR" ) == Amount::result::ok  &&
                                   a.div( "1.a" ) == Amount::result::invalid_value );
    ok = ok && check("money",13,9, a.set( "1", "EUR" ) == Amount::result::ok  &&
                                   a.percent( "1.a" ) == Amount::result::invalid_value );
    ok = ok && check("money",13,10,a.set( "1", "EUR" ) == Amount::result::ok  &&
                                   a.div( "9223372036855000000" ) == Amount::result::overflow );
    ok = ok && check("money",13,11,a.set( "-1", "EUR" ) == Amount::result::ok  &&
                                   b.set( "9223372036854", "EUR" ) == Amount::result::ok  &&
                                   a.sub( b ) == Amount::result::overflow );
    //
    ok = ok && check("money",14,1, a.set( "1", "EUR" ) == Amount::result::ok  &&
                                   b.set( "2", "EUR" ) == Amount::result::ok  &&
                                   a.max( b )          == Amount::result::ok  &&
                                   a.value()           == "2" );
    ok = ok && check("money",14,2, a.set( "1", "EUR" ) == Amount::result::ok  &&
                                   b.set( "2", "EUR" ) == Amount::result::ok  &&
                                   a.min( b )          == Amount::result::ok  &&
                                   a.value()           == "1" );
    ok = ok && check("money",14,3, a.set( "2", "EUR" ) == Amount::result::ok  &&
                                   b.set( "1", "EUR" ) == Amount::result::ok  &&
                                   a.max( b )          == Amount::result::ok  &&
                                   a.value()           == "2" );
    ok = ok && check("money",14,4, a.set( "2", "EUR" ) == Amount::result::ok  &&
                                   b.set( "1", "EUR" ) == Amount::result::ok  &&
                                   a.min( b )          == Amount::result::ok  &&
                                   a.value()           == "1" );
    ok = ok && check("money",14,5, a.set( "-2", "EUR" ) == Amount::result::ok  &&
                                   b.set(  "1", "EUR" ) == Amount::result::ok  &&
                                   a.min( b )           == Amount::result::ok  &&
                                   a.value()            == "-2" );
    ok = ok && check("money",14,6, a.set(  "1", "EUR" ) == Amount::result::ok  &&
                                   b.set(  "1", "USD" ) == Amount::result::ok  &&
                                   a.min( b )           == Amount::result::incompatible_currency );
    ok = ok && check("money",14,6, a.set(  "1", "EUR" ) == Amount::result::ok  &&
                                   b.set(  "1", "USD" ) == Amount::result::ok  &&
                                   a.max( b )           == Amount::result::incompatible_currency );
    //
    ok = ok && check("money",15,1, a.set( "0", "YYY" ) == Amount::result::ok  &&
                                   a.currency()        == "YYY" &&
                                   a.numeric_code()    == 999   &&
                                   a.precision()       == 6     &&
                                   a.subunit()         == 2     &&
                                   a.symbol()          == "o"   &&
                                   a.symbol_local()    == "¤" );
    //
    {
        Currency adder;
        //
        ok = ok && check("money",16,1, ! adder.global_define_currency( "z"  , "0", "1", 123, 5, 1 ) );
        ok = ok && check("money",16,2,   adder.global_define_currency( "ABC", "0", "1", 123, 5, 1 ) );
        ok = ok && check("money",16,3,   adder.global_define_currency( "EUR", "2", "3", 125, 7, 3 ) );
        //
        ok = ok && check("money",16,3, a.set( "1.23", "ABC" ) == Amount::result::ok  &&
                                       a.precision()     == 5    &&
                                       a.symbol()        == "0"  &&
                                       a.symbol_local()  == "1"  &&
                                       a.subunit()       == 1    &&
                                       a.numeric_code()  == 123 );
        //
        ok = ok && check("money",16,3, a.set( "1.23", "EUR" ) == Amount::result::ok  &&
                                       a.precision()     == 7    &&
                                       a.symbol()        == "2"  &&
                                       a.symbol_local()  == "3"  &&
                                       a.subunit()       == 3    &&
                                       a.numeric_code()  == 125 );
    }
    //
    return ok;
}

//-------------------------------------------------------------------------

    bool cmp_alphabet ( std::tuple< size_t, const char * >  p_alphabet,
                        size_t                              p_expected_size,
                        const std::string &                 p_expected_characters )
    {
        auto [ sz, str ] = p_alphabet;
        //
        return sz  == p_expected_size &&
               str == p_expected_characters;
    }

bool validate_uid ( void )
{
    bool          ok = true;
    std::string   s1, s2;
    uid_type      t;
    uid_alphabet  a;
    std::tuple< size_t, const char * > a1, a2;
    //
    ok = ok && check("uid",1,1, (s1=uid_ts(uid_type::long_ts), s1 != ""));
    ok = ok && check("uid",1,2, (s2=uid_ts(uid_type::long_ts), s1 != s2));
    //
    a1 = get_alphabet( uid_alphabet::lower_alphanum );
    a2 = get_alphabet( uid_alphabet::upper_alphanum );
    //
    s1 = std::get<1>( a1 ); s2 = std::get<1>( a2 ); str_lower( s2 );
    ok = ok && check("uid",1,3, s1 == s2 );
    //
    s1 = std::get<1>( a1 ); s2 = std::get<1>( a2 ); str_upper( s1 );
    ok = ok && check("uid",1,4, s1 == s2 );
    //
    for ( auto v : { uid_type::short_ts, uid_type::medium_ts, uid_type::long_ts } )
        ok = ok && check("uid",2,1, uid_type_cvt(uid_type_cvt(v), t) && t == v);
    ok = ok && check("uid",2,1, ! uid_type_cvt( "abc"s, t ) );
    //
    for ( auto v : { uid_alphabet::digits, uid_alphabet::upper_alpha, uid_alphabet::lower_alpha, uid_alphabet::upper_alphanum,
                     uid_alphabet::lower_alphanum, uid_alphabet::base32, uid_alphabet::base32_crockford, uid_alphabet::base62,
                     uid_alphabet::upper_hexadecimal, uid_alphabet::lower_hexadecimal } )
        ok = ok && check("uid",2,2, uid_alphabet_cvt(uid_alphabet_cvt(v), a) && a == v);
    ok = ok && check("uid",2,2, ! uid_alphabet_cvt( "abc"s, a) );
    //
    for ( auto v : { uid_type::short_ts, uid_type::medium_ts, uid_type::long_ts } )
        ok = ok && check("uid",3,1, uid_ts(v).length() == 22);
    //
    ok = ok && check("uid",3,2, uid_ts( uid_type::short_ts , true ).length() == 24);
    ok = ok && check("uid",3,3, uid_ts( uid_type::medium_ts, true ).length() == 25);
    ok = ok && check("uid",3,4, uid_ts( uid_type::long_ts  , true ).length() == 26);
    //
    std::string uids[16];
    for ( int i = 0; i < 16; i++ )
        uids[ i ] = uid_daily();
    for ( int i = 1; i < 16; i++ )
        ok = ok && check("uid",4,1, uids[ i - 1] != uids[ i ] && uids[ i ].length() == 6 );
    //
    ok = ok && check("uid",5,1, (s1=uid_no_ts(3,uid_alphabet::digits),
                                 s1.length()==3 &&
                                 std::all_of(s1.begin(), s1.end(), [](char c) { return std::isdigit(c); })) );
    //
    ok = ok && check("uid",5,2, (s1=uid_no_ts(5,uid_alphabet::upper_alpha),
                                 s1.length()==5 &&
                                 std::all_of(s1.begin(), s1.end(), [](char c) { return std::isupper(c); })) );
    //
    ok = ok && check("uid",5,3, (s1=uid_no_ts(7,uid_alphabet::lower_alpha),
                                 s1.length()==7 &&
                                 std::all_of(s1.begin(), s1.end(), [](char c) { return std::islower(c); })) );
    //
    ok = ok && check("uid",5,4, (s1=uid_no_ts(11,uid_alphabet::base62),
                                 s1.length()==11 &&
                                 std::all_of(s1.begin(), s1.end(), [](char c) { return std::isalnum(c); })) );
    //
    ok = ok && check("uid",5,5, (s1=uid_no_ts(11,uid_alphabet::upper_hexadecimal),
                                 s1.length()==11 &&
                                 std::all_of(s1.begin(), s1.end(), [](char c) { return (std::isdigit(c)||std::isupper(c)) && std::isxdigit(c); })) );
    //
    ok = ok && check("uid",5,6, (s1=uid_no_ts(11,uid_alphabet::lower_hexadecimal),
                                 s1.length()==11 &&
                                 std::all_of(s1.begin(), s1.end(), [](char c) { return (std::isdigit(c)||std::islower(c)) && std::isxdigit(c); })) );
    //
    ok = ok && check("uid",6, 1, cmp_alphabet( get_alphabet( uid_alphabet::digits            ), 10, "0123456789" ) );
    ok = ok && check("uid",6, 2, cmp_alphabet( get_alphabet( uid_alphabet::upper_alpha       ), 26, "ABCDEFGHIJKLMNOPQRSTUVWXYZ" ) );
    ok = ok && check("uid",6, 3, cmp_alphabet( get_alphabet( uid_alphabet::lower_alpha       ), 26, "abcdefghijklmnopqrstuvwxyz" ) );
    ok = ok && check("uid",6, 4, cmp_alphabet( get_alphabet( uid_alphabet::upper_hexadecimal ), 16, "0123456789ABCDEF" ) );
    ok = ok && check("uid",6, 5, cmp_alphabet( get_alphabet( uid_alphabet::lower_hexadecimal ), 16, "0123456789abcdef" ) );
    ok = ok && check("uid",6, 6, cmp_alphabet( get_alphabet( uid_alphabet::upper_alphanum    ), 36, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ" ) );
    ok = ok && check("uid",6, 7, cmp_alphabet( get_alphabet( uid_alphabet::lower_alphanum    ), 36, "0123456789abcdefghijklmnopqrstuvwxyz" ) );
    ok = ok && check("uid",6, 8, cmp_alphabet( get_alphabet( uid_alphabet::base32            ), 32, "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567" ) );
    ok = ok && check("uid",6, 9, cmp_alphabet( get_alphabet( uid_alphabet::base32_crockford  ), 32, "0123456789ABCDEFGHJKMNPQRSTVWXYZ" ) );
    ok = ok && check("uid",6,10, cmp_alphabet( get_alphabet( uid_alphabet::base62            ), 62, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" ) );
    //
    // Test UUID
    ok = ok && check("uid",7,1, (s1=uid_uuid(),
                                 s1.length()==36 &&
                                 std::all_of(s1.begin(), s1.end(), [](char c) { return c=='-'||((std::isdigit(c)||std::islower(c)) && std::isxdigit(c)); })) );
    //
    ok = ok && check("uid",7,2, (s1=uid_uuid(),
                                 s1[8]=='-' && s1[13]=='-' && s1[18]=='-' && s1[23]=='-' ) );
    //
    ok = ok && check("uid",7,3, (s1=uid_uuid( uuid_type::v7 ),
                                 s1.length()==36 &&
                                 std::all_of(s1.begin(), s1.end(), [](char c) { return c=='-'||((std::isdigit(c)||std::islower(c)) && std::isxdigit(c)); })) );
    //
    ok = ok && check("uid",7,4, (s1=uid_uuid( uuid_type::v7 ),
                                 s1[8]=='-' && s1[13]=='-' && s1[18]=='-' && s1[23]=='-' ) );
    //
    ok = ok && check("uid",7,5, uid_uuid( uuid_type::v7 ).substr( 0, 12 ) == 
                                uid_uuid( uuid_type::v7 ).substr( 0, 12 ) ); // only check ten of ms
    //
    s1 = uid_uuid( uuid_type::v7 );
    std::this_thread::sleep_for(18ms);
    s2 = uid_uuid( uuid_type::v7 );
    ok = ok && check("uid",7,6, s1.substr( 0, 12 ) != s2.substr( 0, 12 ) ); // only check ten of ms
    //
    ok = ok && check("uid",7,7, (s1=uid_uuid(uuid_type::v3, uuid_namespace::dns, "www.example.com"),
                                 s1.length()==36 &&
                                 s1[14] == '3' && // version 3
                                ( s1[19] == '8' || s1[19] == '9' ) && // variant 1
                                 std::all_of(s1.begin(), s1.end(), [](char c) { return c=='-'||((std::isdigit(c)||std::islower(c)) && std::isxdigit(c)); })) );
    //
    ok = ok && check("uid",7,8, (s1=uid_uuid(uuid_type::v5, uuid_namespace::dns, "www.example.com"),
                                 s1.length()==36 &&
                                 s1[14] == '5' && // version 5
                                 ( s1[19] == '8' || s1[19] == '9' ) && // variant 1
                                 std::all_of(s1.begin(), s1.end(), [](char c) { return c=='-'||((std::isdigit(c)||std::islower(c)) && std::isxdigit(c)); })) );
    //
    ok = ok && check("uid",7,9, (s1=uid_uuid(uuid_type::v3),
                                 s1[8]=='-' && s1[13]=='-' && s1[18]=='-' && s1[23]=='-' ) );
    ok = ok && check("uid",7,10, (s1=uid_uuid(uuid_type::v5),
                                 s1[8]=='-' && s1[13]=='-' && s1[18]=='-' && s1[23]=='-' ) );
    //
    s1 = uid_uuid(uuid_type::v3, uuid_namespace::dns, "www.example.com");
    s2 = uid_uuid(uuid_type::v3, uuid_namespace::dns, "www.example.com");
    ok = ok && check("uid",7,11, s1 == s2);
    //
    s1 = uid_uuid(uuid_type::v5, uuid_namespace::dns, "www.example.com");
    s2 = uid_uuid(uuid_type::v5, uuid_namespace::dns, "www.example.com");
    ok = ok && check("uid",7,12, s1 == s2);
    //
    s1 = uid_uuid(uuid_type::v3, uuid_namespace::dns, "www.example.com");
    s2 = uid_uuid(uuid_type::v3, uuid_namespace::dns, "www.example.net");
    ok = ok && check("uid",7,13, s1 != s2);
    //
    s1 = uid_uuid(uuid_type::v5, uuid_namespace::dns, "www.example.com");
    s2 = uid_uuid(uuid_type::v5, uuid_namespace::url, "www.example.com");
    ok = ok && check("uid",7,14, s1 != s2);
    //
    ok = ok && check("uid",7,15, uid_uuid(uuid_type::v3, uuid_namespace::oid , "1.3.6.1.4.1.343")=="77bc1dc3-0a9f-3e7e-bfa5-3f611a660c80");
    ok = ok && check("uid",7,16, uid_uuid(uuid_type::v5, uuid_namespace::x500, "uid=user,ou=people,dc=example,dc=com")=="5324d252-c0de-5365-a19c-389275462a9c");
    //
    ok = ok && check("uid",7,17, (s1=uid_uuid(uuid_type::v1),
                                 s1.length()==36 &&
                                 std::all_of(s1.begin(), s1.end(), [](char c) { return c=='-'||((std::isdigit(c)||std::islower(c)) && std::isxdigit(c)); })) );
    //
    ok = ok && check("uid",7,18, (s1=uid_uuid(uuid_type::v1),
                                 s1[8]=='-' && s1[13]=='-' && s1[18]=='-' && s1[23]=='-' ) );
    //
    ok = ok && check("uid",7,19, (s1=uid_uuid(uuid_type::v1),
                                 s1[14] == '1' && // version 1
                                 ( s1[19] == '8' || s1[19] == '9' ) ) );
    //
    s1 = uid_uuid(uuid_type::v1);
    s2 = uid_uuid(uuid_type::v1);
    ok = ok && check("uid",7,20, s1.substr(  0,  8 ) != s2.substr(  0,  8 ) ); // low time
    ok = ok && check("uid",7,21, s1.substr(  8, 11 ) == s2.substr(  8, 11 ) ); // mid and hi time
    ok = ok && check("uid",7,22, s1.substr( 19,  4 ) != s2.substr( 19,  4 ) ); // clock seq
    //
    ok = ok && check("uid",7,30, (s1=uid_uuid(uuid_type::v6),
                                 s1.length()==36 &&
                                 std::all_of(s1.begin(), s1.end(), [](char c) { return c=='-'||((std::isdigit(c)||std::islower(c)) && std::isxdigit(c)); })) );
    //
    ok = ok && check("uid",7,31, (s1=uid_uuid(uuid_type::v6),
                                 s1[8]=='-' && s1[13]=='-' && s1[18]=='-' && s1[23]=='-' ) );
    //
    ok = ok && check("uid",7,32, (s1=uid_uuid(uuid_type::v6),
                                 s1[14] == '6' && // version 6
                                 ( s1[19] == '8' || s1[19] == '9' ) ) );
    //
    s1 = uid_uuid(uuid_type::v6);
    s2 = uid_uuid(uuid_type::v6);
    ok = ok && check("uid",7,33, s1 < s2 );
    ok = ok && check("uid",7,34, s1.substr( 19,  4 ) != s2.substr( 19,  4 ) ); // clock seq
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_url (void)
{
    bool         ok = true;
    std::string  s;
    //
    ok = ok && check( "url", 1, 1, url_encode( "AéB C=D%E" ) == "A%C3%A9B%20C%3DD%25E" );
    ok = ok && check( "url", 1, 2, url_encode( "a.b_c-d~e" ) == "a.b_c-d~e"            );
    ok = ok && check( "url", 1, 3, url_encode( "=a" )        == "%3Da"                 );
    ok = ok && check( "url", 1, 4, url_encode( "a=" )        == "a%3D"                 );
    ok = ok && check( "url", 1, 5, url_encode( "abcd" )      == "abcd"                 );
    //
    ok = ok && check( "url", 2, 1,   url_decode( "A%C3%A9B%20C%3DD%25E", s ) && ( s == "AéB C=D%E" ) );
    ok = ok && check( "url", 2, 2,   url_decode( "a.b_c-d~e"           , s ) && ( s == "a.b_c-d~e" ) );
    ok = ok && check( "url", 2, 3,   url_decode( "%3Da"                , s ) && ( s == "=a"        ) );
    ok = ok && check( "url", 2, 4,   url_decode( "a%3D"                , s ) && ( s == "a="        ) );
    ok = ok && check( "url", 2, 5,   url_decode( "abcd"                , s ) && ( s == "abcd"      ) );
    ok = ok && check( "url", 2, 6, ! url_decode( "a%zz"                , s ) );
    ok = ok && check( "url", 2, 7, ! url_decode( "a%0"                 , s ) );
    ok = ok && check( "url", 2, 8, ! url_decode( "a%0z"                , s ) );
    ok = ok && check( "url", 2, 9, ! url_decode( "a%z0"                , s ) );
    ok = ok && check( "url", 2,10, ! url_decode( "a%"                  , s ) );
    ok = ok && check( "url", 2,11, ! url_decode( "a%%aa"               , s ) );
    ok = ok && check( "url", 2,12, ! url_decode( "a%z0x"               , s ) );
    //
    std::map< std::string, std::string > A, B;
    //
    A = {};                              ok = ok && check( "url", 3, 1, url_encode_parameters( A ) == ""            );
    A = {{"a","1"}};                     ok = ok && check( "url", 3, 2, url_encode_parameters( A ) == "a=1"         );
    A = {{"a","1"},{"b","2"}};           ok = ok && check( "url", 3, 3, url_encode_parameters( A ) == "a=1&b=2"     );
    A = {{"a","1"},{"b","2"},{"c","3"}}; ok = ok && check( "url", 3, 4, url_encode_parameters( A ) == "a=1&b=2&c=3" );
    //
    A = {};                              ok = ok && check( "url", 4, 1, url_decode_parameters( "",            B ) && A == B );
    A = {{"a","1"}};                     ok = ok && check( "url", 4, 2, url_decode_parameters( "a=1",         B ) && A == B );
    A = {{"a","1"},{"b","2"}};           ok = ok && check( "url", 4, 3, url_decode_parameters( "a=1&b=2",     B ) && A == B );
    A = {{"a","1"},{"b","2"},{"c","3"}}; ok = ok && check( "url", 4, 4, url_decode_parameters( "a=1&b=2&c=3", B ) && A == B );
    //
    ok = ok && check( "url", 5, 1, ! url_decode_parameters( "a=1&&B=2", A ) );
    ok = ok && check( "url", 5, 2, ! url_decode_parameters( "a=%xx", A ) );
    ok = ok && check( "url", 5, 3, ! url_decode_parameters( "a=%8", A ) );
    //
    A = {{"a","1"}};           ok = ok && check( "url", 6, 1, url_add_parameters( "http://server/doc",     A       ) ==
                                                                                  "http://server/doc?a=1" );
    A = {{"a","1"},{"b","2"}}; ok = ok && check( "url", 6, 2, url_add_parameters( "http://server/doc",     A       ) ==
                                                                                  "http://server/doc?a=1&b=2" );
    A = {{"a","1"}};           ok = ok && check( "url", 6, 3, url_add_parameters( "http://server/doc?z=1", A       ) ==
                                                                                  "http://server/doc?z=1&a=1" );
    A = {{"a","1"},{"b","2"}}; ok = ok && check( "url", 6, 4, url_add_parameters( "http://server/doc?z=1", A       ) ==
                                                                                  "http://server/doc?z=1&a=1&b=2" );
    A = {{"q","a=1"}};         ok = ok && check( "url", 6, 5, url_add_parameters( "http://server/doc",     A       ) ==
                                                                                  "http://server/doc?q=a%3D1" );
    A = {{"a","1"}};           ok = ok && check( "url", 6, 6, url_add_parameters( "http://server/doc",     A, true ) ==
                                                                                  "http://server/doc#a=1" );
    A = {{"a","1"}};           ok = ok && check( "url", 6, 7, url_add_parameters( "http://server/doc?z=1", A, true ) ==
                                                                                  "http://server/doc?z=1#a=1" );
    //
    A = {{"a","1"}}; ok = ok && check( "url", 7, 1, url_add_parameters( "http://server/doc?a=2",     A, false, true  ) == "http://server/doc?a=1"         );
    A = {{"a","1"}}; ok = ok && check( "url", 7, 2, url_add_parameters( "http://server/doc?a=2#z=1", A, false, true  ) == "http://server/doc?a=1#z=1"     );
    A = {{"z","1"}}; ok = ok && check( "url", 7, 3, url_add_parameters( "http://server/doc#z=2",     A, true,  true  ) == "http://server/doc#z=1"         );
    A = {{"z","1"}}; ok = ok && check( "url", 7, 4, url_add_parameters( "http://server/doc?a=1#z=2", A, true,  true  ) == "http://server/doc?a=1#z=1"     );
    A = {{"z","1"}}; ok = ok && check( "url", 7, 5, url_add_parameters( "http://server/doc?a=1#z=2", A, true,  false ) == "http://server/doc?a=1#z=2&z=1" );
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_stats ( void )
{
    bool        ok = true;
    StatsValue  S;
    double      f;
    //
    std::tuple< double, double, double, double, double, uint64_t > d1 { 0,0,0,0,0,0 };
    std::tuple< double, double, double >                           d2 { 0,0,0       };
    //
    ok = ok && check("stats",0,1, S.details()        == d1 );
    ok = ok && check("stats",0,2, S.window_details() == d2 );
    //
    S.resize(10);
    for (int i = 0; i < 5; i++) S.add_value( 0 );
    for (int i = 0; i < 5; i++) S.add_value( 1 );
    ok = ok && check("stats",1,1, fabs(0.500'000-S.mean())     < 0.000'001);
    ok = ok && check("stats",1,2, fabs(0.277'777-S.variance()) < 0.000'001);
    ok = ok && check("stats",1,3, fabs(0.527'046-S.sd())       < 0.000'001);
    ok = ok && check("stats",1,4, fabs(1-S.max())              < 0.000'001);
    ok = ok && check("stats",1,5, fabs(0-S.min())              < 0.000'001);
    ok = ok && check("stats",1,6, S.count()                   == 10);
    //
    auto [ count, max, mean, min, sd, variance ] = S.details();
    ok = ok && check("stats",1,7, fabs(0.277'777-variance) < 0.000'001 &&
                                  fabs(0.500'000-mean)     < 0.000'001 &&
                                  fabs(0.000'000-min)      < 0.000'001 &&
                                  fabs(1.000'000-max)      < 0.000'001 &&
                                  fabs(0.527'046-sd)       < 0.000'001 &&
                                  count                   == 10);
    //
    S.clear();
    S.resize(3);
    for (int i = 1; i <= 4; i++) S.add_value( i );   // 1 [2 3 4]
    ok = ok && check("stats",2,1, fabs(3.000'000-S.window_mean()) < 0.000'001);
    S.resize(4);
    for (int i = 5; i <= 7; i++) S.add_value( i );   // 1 2 3 [4 5 6 7]
    ok = ok && check("stats",2,2, fabs(5.500'000-S.window_mean()) < 0.000'001);
    S.resize(3);                                     // 1 2 3 4 [5 6 7]
    ok = ok && check("stats",2,3, fabs(6.000'000-S.window_mean()) < 0.000'001);
    for (int i = 8; i <= 9; i++) S.add_value( i );   // 1 2 3 4 5 6 [7 8 9]
    ok = ok && check("stats",2,4, fabs(8.000'000-S.window_mean()) < 0.000'001);
    //
    ok = ok && check("stats",2,5, fabs(8.000'000-S.window_mean()) < 0.000'001);
    ok = ok && check("stats",2,6, fabs(7.000'000-S.window_min())  < 0.000'001);
    ok = ok && check("stats",2,7, fabs(9.000'000-S.window_max())  < 0.000'001);
    //
    auto [ wmax, wmean, wmin ] = S.window_details();
    ok = ok && check("stats",2,8, fabs(8.000'000-wmean) < 0.000'001 &&
                                  fabs(7.000'000-wmin)  < 0.000'001 &&
                                  fabs(9.000'000-wmax)  < 0.000'001);
    //
    f = 0;
    stats_update_average( f, 1, 3 );
    ok = ok && check("stats",3,1, fabs(0.333'333-f)         < 0.000'001);
    stats_update_average( f, 1, 3 );
    ok = ok && check("stats",3,2, fabs(0.555'555-f)         < 0.000'001);
    stats_update_average( f, 1, 3 );
    ok = ok && check("stats",3,3, fabs(0.703'703-f)         < 0.000'001);
    stats_update_average( f, 1, 3 );
    ok = ok && check("stats",3,3, fabs(0.802'469-f)         < 0.000'001);
    //
    return ok;
}

//-------------------------------------------------------------------------
bool validate_error_control ( void )
{
    std::string         s;
    bool                ok = true;
    error_control_type  ect;
    //
    // Luhn
    ok = ok && check("err.ctl",1,11,   error_control_check( ""              , error_control_type::luhn ) );
    ok = ok && check("err.ctl",1,12,   error_control_check( "2183300199"    , error_control_type::luhn ) );
    ok = ok && check("err.ctl",1,13,   error_control_check( "79927398713"   , error_control_type::luhn ) );
    ok = ok && check("err.ctl",1,14,   error_control_check( "35600000000048", error_control_type::luhn ) );
    ok = ok && check("err.ctl",1,15, ! error_control_check( "35600000000084", error_control_type::luhn ) );
    ok = ok && check("err.ctl",1,16, ! error_control_check( "1z"            , error_control_type::luhn ) );
    //
    ok = ok && check("err.ctl",1,21, (s="",              error_control_sign( s, error_control_type::luhn ) && s == "0"              ) );
    ok = ok && check("err.ctl",1,22, (s="218330019",     error_control_sign( s, error_control_type::luhn ) && s == "2183300199"     ) );
    ok = ok && check("err.ctl",1,23, (s="7992739871"   , error_control_sign( s, error_control_type::luhn ) && s == "79927398713"    ) );
    ok = ok && check("err.ctl",1,24, (s="3560000000004", error_control_sign( s, error_control_type::luhn ) && s == "35600000000048" ) );
    ok = ok && check("err.ctl",1,25, (s="123z",          ! error_control_sign( s, error_control_type::luhn ) ) );
    //
    // Mod-97
    ok = ok && check("err.ctl",2,11,   error_control_check( "01"                          , error_control_type::mod97 ) );
    ok = ok && check("err.ctl",2,12,   error_control_check( "3214282912345698765432161182", error_control_type::mod97 ) );
    ok = ok && check("err.ctl",2,13,   error_control_check( "060000123456758"             , error_control_type::mod97 ) );
    ok = ok && check("err.ctl",2,14,   error_control_check( "1110271220658244971655161187", error_control_type::mod97 ) );
    ok = ok && check("err.ctl",2,15,   error_control_check( "068999999501111443"          , error_control_type::mod97 ) );
    ok = ok && check("err.ctl",2,16, ! error_control_check( "068999999501111434"          , error_control_type::mod97 ) );
    ok = ok && check("err.ctl",2,17, ! error_control_check( "1z"                          , error_control_type::mod97 ) );
    ok = ok && check("err.ctl",2,18,   error_control_check( "000201231234567890154"       , error_control_type::mod97 ) ); // Portugal
    ok = ok && check("err.ctl",2,19,   error_control_check( "263300012039086"             , error_control_type::mod97 ) ); // Slovenia
    //
    ok = ok && check("err.ctl",2,21, (s="",                           error_control_sign( s, error_control_type::mod97 ) && s == "98" ) );
    ok = ok && check("err.ctl",2,22, (s="32142829123456987654321611", error_control_sign( s, error_control_type::mod97 ) && s == "3214282912345698765432161182" ) );
    ok = ok && check("err.ctl",2,23, (s="0600001234567",              error_control_sign( s, error_control_type::mod97 ) && s == "060000123456758" ) );
    ok = ok && check("err.ctl",2,24, (s="1z",                       ! error_control_sign( s, error_control_type::mod97 ) ) );
    ok = ok && check("err.ctl",2,25, (s="5050000123456789",           error_control_sign( s, error_control_type::mod97 ) && s == "505000012345678951" ) ); // Montenegro
    //
    // IBAN
    ok = ok && check("err.ctl",3,11,  error_control_check( "GB82WEST12345698765432"      , error_control_type::iban ) );
    ok = ok && check("err.ctl",3,12,  error_control_check( "GB87BARC20658244971655"      , error_control_type::iban ) );
    ok = ok && check("err.ctl",3,13,  error_control_check( "FR1420041010050500013M02606" , error_control_type::iban ) );
    ok = ok && check("err.ctl",3,14,  error_control_check( "FR7630004015870002601171220" , error_control_type::iban ) );
    ok = ok && check("err.ctl",3,15,  error_control_check( "ES0200190114344930034774"    , error_control_type::iban ) );
    ok = ok && check("err.ctl",3,16,  error_control_check( "BE02001000161340"            , error_control_type::iban ) );
    ok = ok && check("err.ctl",3,17,  error_control_check( "LU020019125510784000"        , error_control_type::iban ) );
    ok = ok && check("err.ctl",3,18,  error_control_check( "NL02ABNA0410354368"          , error_control_type::iban ) );
    ok = ok && check("err.ctl",3,19, !error_control_check( "DE02100500100044416512"      , error_control_type::iban ) );
    ok = ok && check("err.ctl",3,20,  error_control_check( "PT50123443211234567890172"   , error_control_type::iban ) );
    ok = ok && check("err.ctl",3,21, !error_control_check( ""                            , error_control_type::iban ) );
    //
    ok = ok && check("err.ctl",3,31, (s="GB00WEST12345698765432"     ,  error_control_sign( s, error_control_type::iban ) && s == "GB82WEST12345698765432" ) );
    ok = ok && check("err.ctl",3,32, (s="FR0030004015870002601171220",  error_control_sign( s, error_control_type::iban ) && s == "FR7630004015870002601171220"     ) );
    ok = ok && check("err.ctl",3,33, (s="IT00A0301926102000000490887",  error_control_sign( s, error_control_type::iban ) && s == "IT02A0301926102000000490887"     ) );
    ok = ok && check("err.ctl",3,34, (s=""                           , !error_control_sign( s, error_control_type::iban ) ) );
    //
    // RIB
    ok = ok && check("err.ctl",4,11,   error_control_check( "12345123450123456789A03"     , error_control_type::rib ) );
    ok = ok && check("err.ctl",4,12,   error_control_check( "12003110041506736001508"     , error_control_type::rib ) );
    ok = ok && check("err.ctl",4,13,   error_control_check( "42001411000099999933"        , error_control_type::rib ) );
    ok = ok && check("err.ctl",4,14,   error_control_check( "28521966142334508845009"     , error_control_type::rib ) );
    ok = ok && check("err.ctl",4,15,  !error_control_check( "28521963142334508845009"     , error_control_type::rib ) );
    ok = ok && check("err.ctl",4,16,  !error_control_check( ""                            , error_control_type::rib ) );
    //
    ok = ok && check("err.ctl",4,21, (s="12345123450123456789A",  error_control_sign( s, error_control_type::rib ) && s == "12345123450123456789A03" ) );
    ok = ok && check("err.ctl",4,22, (s="120031100415067360015",  error_control_sign( s, error_control_type::rib ) && s == "12003110041506736001508" ) );
    ok = ok && check("err.ctl",4,23, (s="420014110000999999"   ,  error_control_sign( s, error_control_type::rib ) && s == "42001411000099999933" ) );
    ok = ok && check("err.ctl",4,24, (s="285219661423345088450",  error_control_sign( s, error_control_type::rib ) && s == "28521966142334508845009" ) );
    ok = ok && check("err.ctl",4,25, (s=""                     , !error_control_sign( s, error_control_type::rib ) ) );
    //
    // Damm
    ok = ok && check("err.ctl",5,11,   error_control_check( "5724",         error_control_type::damm ) );
    ok = ok && check("err.ctl",5,12,   error_control_check( "84761648341",  error_control_type::damm ) );
    ok = ok && check("err.ctl",5,13,   error_control_check( "30005496",     error_control_type::damm ) );
    ok = ok && check("err.ctl",5,14, ! error_control_check( "5742",         error_control_type::damm ) );
    ok = ok && check("err.ctl",5,15, ! error_control_check( "1z",           error_control_type::damm ) );
    //
    ok = ok && check("err.ctl",5,21, (s="572",        error_control_sign( s, error_control_type::damm ) && s == "5724" ) );
    ok = ok && check("err.ctl",5,22, (s="8476164834", error_control_sign( s, error_control_type::damm ) && s == "84761648341" ) );
    ok = ok && check("err.ctl",5,23, (s="3000549",    error_control_sign( s, error_control_type::damm ) && s == "30005496" ) );
    ok = ok && check("err.ctl",5,24, (s="1z",         ! error_control_sign( s, error_control_type::damm ) ) );
    //
    // Misc
    ok = ok && check("err.ctl",7,1, error_control_type_cvt( error_control_type_cvt( error_control_type::luhn) , ect ) &&
                               ect == error_control_type::luhn );
    ok = ok && check("err.ctl",7,2, error_control_type_cvt( error_control_type_cvt( error_control_type::mod97) , ect ) &&
                               ect == error_control_type::mod97 );
    ok = ok && check("err.ctl",7,3, error_control_type_cvt( error_control_type_cvt( error_control_type::damm) , ect ) &&
                               ect == error_control_type::damm );
    ok = ok && check("err.ctl",7,4, error_control_type_cvt( error_control_type_cvt( error_control_type::iban) , ect ) &&
                               ect == error_control_type::iban );
    ok = ok && check("err.ctl",7,5, error_control_type_cvt( error_control_type_cvt( error_control_type::rib) , ect ) &&
                               ect == error_control_type::rib );
    ok = ok && check("err.ctl",7,6, ! error_control_type_cvt( "xxx" , ect ) );
    //
    ok = ok && check("err.ctl",8,1, error_control_signature_length( error_control_type::luhn  ) == 1 );
    ok = ok && check("err.ctl",8,2, error_control_signature_length( error_control_type::mod97 ) == 2 );
    ok = ok && check("err.ctl",8,3, error_control_signature_length( error_control_type::damm  ) == 1 );
    ok = ok && check("err.ctl",8,3, error_control_signature_length( error_control_type::iban  ) == 2 );
    ok = ok && check("err.ctl",8,3, error_control_signature_length( error_control_type::rib   ) == 2 );
    //
    return ok;
}

//-------------------------------------------------------------------------
int main ( int /* argc*/, char * /* argv */ [] )
{
    bool ok = true;
    //
    crashhandler_setup();
    //
    start ("common  ");
    ok = ok && stop ("common",   validate_common() );
    //
    start ("crypto  ");
    ok = ok && stop ("crypto",   validate_crypto() );
    //
    start ("encoding");
    ok = ok && stop ("encoding", validate_encoding() );
    //
    start ("string  ");
    ok = ok && stop ("string",   validate_string() );
    //
    start ("time    ");
    ok = ok && stop ("time",     validate_time() );
    //
    start ("queue   ");
    ok = ok && stop ("queue",    validate_thread_queue() );
    //
    start ("thread  ");
    ok = ok && stop ("thread",   validate_thread_pool() );
    //
    start ("file    ");
    ok = ok && stop ("file",     validate_file() );
    //
    start ("money   ");
    ok = ok && stop ("money",    validate_money() );
    //
    start ("uid     ");
    ok = ok && stop ("uid",      validate_uid() );
    //
    start ("url     ");
    ok = ok && stop ("url",      validate_url() );
    //
    start ("stats   ");
    ok = ok && stop ("stats",    validate_stats() );
    //
    start ("err.ctl ");
    ok = ok && stop ("err.ctl",  validate_error_control() );
    //
    printf ("-------\n");
    printf ("validation: %s\n", ok ? "ok" : "error");
    //
    exit  (ok ? 0 : 1);
    return ok ? 0 : 1;
}
