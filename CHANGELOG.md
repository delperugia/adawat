# Release history

## **1.19-1** 2025-01-24
  - adding UUID v1, v3, v5 and v6 support

## **1.18-1** 2024-08-06
  - adding MGA, MRU and MWK currencies

## **1.17-3** 2024-07-04
  - code review: time
  - extra validation tests
  - fix time_parse_iso8601_duration

## **1.17-2** 2024-07-02
  - making ThreadQueue compatible with object without copy operator
  - adding details functions to StatsValue
  - adding UUID v7 support

## **1.16-3** 2024-04-26
  - upgrading CI to Ubuntu 24.04

## **1.16-2** 2024-01-31
  - fix validation test
  - adding uid_alphabet upper_hexadecimal and lower_hexadecimal
  - adding uid_uuid
  - adding lower case mode to encoding_base16_encode

## **1.15-1** 2023-05-16
  - new crypto_digest and crypto_hmac template functions

## **1.14-5** 2023-01-28
  - fix dependencies in adawat_url.h
  - str_split and str_split_view code review
  - adding crypto_last_errors

## **1.14-4** 2022-09-13
  - fix Amount round up

## **1.14-3** 2022-04-21
  - adding auto tests to CMake
  - upgrading CI to Ubuntu 22.04

## **1.14-3** 2022-02-02
  - sporadic error when calling crypto_hmac

## **1.14-2** 2022-01-21
  - code cleaning
  - OpenSSL 3 compatibility
  - increasing stats window size

## **1.14-1** 2022-01-05
  - extending std::string_view usage

## **1.13-2** 2021-12-29
  - better seeding of the fail safe random generator
  - adding error_control_signature_length
  - adding IBAN and RIB error controls

## **1.12-2** 2021-09-21
  - fix recursion bug in str_replace

## **1.12-1** 2021-09-09
  - new XAF and XOF currency symbols
  - new method global_define_currency in Currency

## **1.11-3** 2021-08-24
  - force threading queue memory reduction on clear

## **1.11-2** 2021-08-20
  - fix in string trim functions

## **1.11-1** 2021-07-28
  - new browse functions in ThreadQueue and ThreadPool
  - execute signature in ThreadPool changed to rvalue
  - pop signature in ThreadPool changed to return an std::optional

## **1.10-1** 2021-06-02
  - new public key signature functions

## **1.9-1** 2021-05-20
  - new function time_parse_iso8601_duration
  - new function url_add_parameters
  - CLang/G++ detection review

## **1.8-3** 2021-05-05
  - CLang++ support
  - time_parse_duration description

## **1.8-2** 2021-01-30
  - making str method in Data const
  - error_control/file header conflict
  - static data in Damm
  - protecting fallback MT in crypto_random
  - using monotonic clock in uid_daily
  - adding uid_ts secure mode
  - new methods min and max in Amount
  - new methods start and idle in ThreadPool
  - file_append and file_read code review
  - delayed items in ThreadQueue; removing maximal size
  - new currencies
  - adding latin (iso 8859-15) and local (utf-8) currency symbols

## **1.7-5** 2020-11-11
  - speed optimization in uid_no_ts
  - adding afghani and Pakistani rupee
  - project structure reviewed

## **1.7-4** 2020-09-13
  - fix in str_split

## **1.7-3** 2020-05-29
  - HOTP and TOTP functions

## **1.7-2** 2020-03-16
  - statistics window min and max
  - adding currency numeric code
  - fix in Amount round up
  - binary file operations
  - creating path in File write functions

## **1.7-1** 2020-01-27
  - calculation error when using StatsValue::add_value
  - amount::format error with value lower than 1
  - wrong error control check type name conversion
  - new Amount::subunit information on currency

## **1.7-0** 2019-10-26
  - operator<< to set/get Data
  - fail safe mode for crypto_random

## **1.6-0** 2019-05-26
  - code review
  - new Damm error control functions
  - error control API review

## **1.5-4** 2019-05-11
  - StatsValue's count method

## **1.5-3** 2019-03-21
  - crypto_equal

## **1.5-2** 2019-03-01
  - str_join to support std::set
  - Amount's error to_text function
  - Amount to ignore non significant trailing digits
  - Amount positive, zero and negative comparison functions
  - new MOD-97 error control functions

## **1.5-1** 2019-02-01
  - UId API review
  - new LUHN error control functions

## **1.4-5** 2019-01-18
  - new str_csv_has_value function

## **1.3-4** 2018-11-30
  - UId alphabet and length changed to alphanumerical only
  - fix str_split for empty strings
  - URL functions

## **1.2-3** 2018-11-12
  - ability to set UId encoding

## **1.1-2** 2018-11-07
  - adding functions:
    - crypto_random
    - set_data
    - get_data

## **1.0-1** 2018-10-06
 - initial release
